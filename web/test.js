app= { UseConsoleLog: true };

function GetSelectedSpecificationFor_id(specs, id)
{
	var id_parts = id.split('.');
	var namespace = id_parts[0];
	var name = id_parts[1];
	return specs[namespace][name];
}

function GetSelectedFormSpecificationFor_id_Form(form_id)
{
	return GetSelectedSpecificationFor_id(CPW_forms, form_id);
}

function GetSelectedFormSpecification()
{
	var form_selector = $('#cpw-form-select');
	var form_id = form_selector.val();
	return GetSelectedFormSpecificationFor_id_Form(form_id);
}

function CreateNewFormWithSpecFunc(form_spec_func)
{
	var form_spec = form_spec_func();
	app.current_form_spec = form_spec;
	StartEditor();
	form_spec.UseProfile(GetProfile());
	form_spec.CreateNew('#report-editor');
	var html_ForProfiledData = form_spec.BuildHtmlViewForProfiledData();
	$('#config-html-attrs').html(html_ForProfiledData);
}

function ReloadConfig()
{
	form_spec= app.current_form_spec;
	form_spec.UseProfile(GetProfile());
	var html_ForProfiledData = form_spec.BuildHtmlViewForProfiledData();
	$('#config-html-attrs').html(html_ForProfiledData);
}

function CreateNewForm()
{
	var form_spec_func = GetSelectedFormSpecification();
	CreateNewFormWithSpecFunc(form_spec_func);
}

function EditFormWithSpecFunc(form_spec_func)
{
	var form_spec = form_spec_func();
	var form_content = $('#form-content-text-area').val();
	try
	{
		var content_checking_text = form_spec.SetFormContent(form_content);
	}
	catch (err)
	{
		if (!err.parseException)
		{
			throw err;
		}
		else
		{
			if (!confirm("Ошибка чтения документа (неверный формат):\r\n" + err.readablemsg + "\r\n\r\nОткрыть всё равно?"))
				return;
		}
	}
	if (null != content_checking_text)
	{
		alert("содержимое НЕ является валидным документом:\r\n\r\n" + content_checking_text);
	}
	else
	{
		form_spec.Edit('#report-editor');
		var html_ForProfiledData = form_spec.BuildHtmlViewForProfiledData();
		$('#config-html-attrs').html(html_ForProfiledData);
		app.current_form_spec = form_spec;
		StartEditor();
	}
}

function EditForm()
{
	var form_spec_func = GetSelectedFormSpecification();
	EditFormWithSpecFunc(form_spec_func);
}

function ViewFormWithSpecFunc(form_spec_func)
{
	var form_spec = form_spec_func();
	var form_content = $('#form-content-text-area').val();

	var content_checking_text = form_spec.SetFormContent(form_content);
	if (null != content_checking_text)
	{
		alert("содержимое НЕ является валидным документом:\r\n\r\n" + content_checking_text);
	}
	else
	{
		/*form_spec.View('report-editor');
		app.current_form_spec = form_spec;
		StartView();*/

		var xml_view_text = form_spec.BuildXamlView();
		var xml_Blob = new Blob([xml_view_text], { type: "text/xaml;charset=utf-8;" });
		saveAs(xml_Blob, "form.xaml");
	}
}

function PrepareXamlFor(id_form,id_content)
{
	var form_content = GetSelectedSpecificationFor_id(CPW_contents, id_content);
	var form_spec_func = GetSelectedFormSpecificationFor_id_Form(id_form);
	var form_spec = form_spec_func();

	form_spec.SetFormContent(form_content);
	return form_spec.BuildXamlView();
}

function ViewForm()
{
	var form_spec_func = GetSelectedFormSpecification();
	ViewFormWithSpecFunc(form_spec_func);
}

function StartEditor()
{
	$('#form-management').hide();
	$('#btn_test_forms_SaveForm').show();
	$('#form-editor-footer-buttons').show();
}

function StartView()
{
	$('#form-management').hide();
	$('#btn_test_forms_SaveForm').hide();
	$('#form-editor-footer-buttons').show();
}

function StopEditor()
{
	$('#form-management').show();
	$('#form-editor-footer-buttons').hide();
	$('#report-editor').empty();
	$('#config-html-attrs').empty();
	app.current_form_spec = null;
}

function CancelEditWithoutSave()
{
	StopEditor();
}

function TrySignAndSave(form_spec)
{
	try
	{
		form_spec.Sign();
		return form_spec.GetFormContent();
	}
	catch (err)
	{
		console.log(err);
		if (!err.parseException)
		{
			throw err;
		}
		else
		{
			var msgtxt = '';
			if (err.readablemsg)
				msgtxt = err.readablemsg;
			if (err.innerException && err.innerException.readablemsg)
				msgtxt = err.innerException.readablemsg;
			if (!confirm("Не получается соблюсти формат документа:\r\n" + msgtxt + "\r\n\r\n попытаться сохранить без контроля схемы?"))
			{
				return null;
			}
			else
			{
				form_spec.UsedCodecInfo.Codec.GetScheme = null;
				if (form_spec.UsedCodecInfo.previous && form_spec.UsedCodecInfo.previous.CodecInfo && form_spec.UsedCodecInfo.previous.CodecInfo.Codec)
					form_spec.UsedCodecInfo.previous.CodecInfo.Codec.GetScheme = null;
				return form_spec.GetFormContent();
			}
		}
	}
}

function MessageBox(html, title)
{
	$("#dialog").html(html);
	$("#dialog").dialog({
		width: 800
		, title: title ? title : 'Быстрое текстовое сообщение'
		, buttons: { "OK": function () { $(this).dialog("close"); } }
	});
}

function IfOkWithValidateResult(validation_result, on_validate_ok_func)
{
	var title_reject = 'Отказ сохранять содержимое формы из-за нарушенных органичений';
	if (null == validation_result || '' == validation_result)
	{
		on_validate_ok_func();
	}
	else if ('string' == typeof validation_result)
	{
		MessageBox('<pre>' + validation_result + '</pre>', title_reject);
	}
	else if ('[object Array]' === Object.prototype.toString.call(validation_result))
	{
		var html = '';
		var invalid = false;
		var descriptions = '';
		for (var i = 0; i < validation_result.length; i++)
		{
			var validate_constraint = validation_result[i];
			if (false == validate_constraint.check_constraint_result)
				invalid = true;
			descriptions += '<li>' + validate_constraint.description + '</li>';
		}
		if (invalid)
		{
			MessageBox('<ul>' + descriptions + '</ul>', title_reject);
		}
		else
		{
			$("#dialog").html('<ul>' + descriptions + '</ul><center>Сохранить содержимое формы?</center>');
			$("#dialog").dialog({
				width: 800
				, title: 'Предупреждение перед сохранением содержимого формы'
				, buttons:
					{
						"Да, сохранить": function () { $(this).dialog("close"); on_validate_ok_func(); }
						, "Нет, вернуться к редактированию": function () { $(this).dialog("close"); }
					}
			});
		}
	}
	else
	{
		MessageBox('Валидация завершена каким то непредсказуемым образом', title_reject);
	}
}

function SaveForm()
{
	var form_spec = app.current_form_spec;
	IfOkWithValidateResult(form_spec.Validate(),
	function()
	{
		var form_content = TrySignAndSave(form_spec);
		if (null != form_content)
		{
			if ("string" != typeof form_content)
				form_content = JSON.stringify(form_content, null, "\t");
			$('#form-content-text-area').val(form_content);
			StopEditor();
		}
	});
}

function CPW_Singleton()
{
	var wax =
	{
		SignXml: function (content)
		{
			/*
			return '<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">\r\n' +
			'    <ds:SignedInfo>\r\n' +
			'        <ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></ds:CanonicalizationMethod>\r\n' +
			'        <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411"></ds:SignatureMethod>\r\n' +
			'        <ds:Reference URI="">\r\n' +
			'            <ds:Transforms>\r\n' +
			'                <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></ds:Transform>\r\n' +
			'            </ds:Transforms>\r\n' +
			'            <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr3411"></ds:DigestMethod>\r\n' +
			'            <ds:DigestValue></ds:DigestValue>\r\n' +
			'        </ds:Reference>\r\n' +
			'    </ds:SignedInfo>\r\n' +
			'    <ds:SignatureValue></ds:SignatureValue>\r\n' +
			'    <ds:KeyInfo>' +
			'        <ds:X509Data>' +
			'            <ds:X509IssuerSerial>' +
			'                <ds:X509IssuerName>2.5.4.3=InfoTrust, 2.5.4.11=Удостоверяющий центр, 2.5.4.10=ООО НПП Ижинформпроект, 2.5.4.7=Ижевск, 2.5.4.8=Удмуртская Республика, 2.5.4.6=RU, 1.2.840.113549.1.9.1=pki@infotrust.ru</ds:X509IssuerName>' +
			'                <ds:X509SerialNumber>494820925618083806086438</ds:X509SerialNumber>' +
			'            </ds:X509IssuerSerial>' +
			'       </ds:X509Data>' +
			'    </ds:KeyInfo>\r\n' + 
			'</ds:Signature>';
			*/
			return "";
		},
		LoadScans: function (filter, goalFormatSpec)
		{
			var jsonScans = '[' +
				'{"path":"scan1.png",' +
				'"size":2383,' +
				'"content":"iVBORw0KGgoAAAANSUhEUgAAAL0AAADJCAIAAAD5BqSwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAA' +
						   'JcEhZcwAADsMAAA7DAcdvqGQAAAjkSURBVHhe7dZBktu4DgDQ3D5XmHNkmdtkNzfIMt81/VhFfUuxTEqWQOLtGo' +
						   'BIgkS5+tuflN6Xc5Na5NykFjk3qUXOTWqRc5Na5NykFjk3qUXOTWqRc5Na5NykFjk3qUXOTWqRc5Na5NykFjk3qU' +
						   'XOTWqRc5Na5NykFjk3qUXOTWqRc5Na5NykFjk3qUXOTWqRc5Na5NykFlPPzbdX1KUnE12NWehjrekNfhFe+xz2mN' +
						   'KwzXvb89lvMqO17TGv4ARzGKdbr/cOX25T9w5fjm6QPj3aK6pbWeUV1UML36S32qbuaFbfpm5QsdvzRBsUnclOGx' +
						   'SNKGpvXmaNis+y9xoVYwnZlQdZo+IKTrBGxUDiteQpnkhfzWmeSI8iWD8e4Yn0PTjTE+khRGrG9S/J3Y/zLcnFF6' +
						   'YTF78kd1dOuSQXXIw2XPmS3L0565JcZFFvXyICJ65IRBagB5ddkYjDuSsSYd29AddckYjG6SsSMeXcfIjTVyRiuv' +
						   'XpXXBFIiY9VCQCijQ3opHppBAN6L5Hd7UVich0UpGIJszciMann0I0mpybT9NPRSKUmx7ajVYkhqClQjSUGHMjOg' +
						   'pdFaKh5NxcQFeFaCh3PLTrrEgMRGOFaBwB5kZ0LHorROPIubmG3grROHJurqG3QjSOiebm9+/fv379+vfff/19Kb' +
						   '0VonHMMjc/f/603Ldv//zzz2OAJHY7duwcpRCNY4q5qYfmy/fv3+X26R+7/2OtQjSO8efm8TthoSXpHfrH7pmFCt' +
						   'E4xp+bx2+DhZakX+kfu1VWKUTjCDA3DxJNHv+RWGVJ+pXOsdtilUI0jpue2HUWoq0e/5FYqCL3SufYrbJEIRrKFH' +
						   'Pz+M14/Edirf/8+PFDboeesVtliUI0lBhz8yBxhc6xe2aVQjSUnJtP009FIpT7HtqlFqLx6acQjSbM3DxIRKaTik' +
						   'Q0tz63qy1EI9NJIRpQpLl5kIhJDxWJgO5+dBdckYjG6SsSMU03N4cs0uBr35pETAFO75orEu/zfUXilbeKn319Xp' +
						   'MIK0YDLrsi8T7fL8k9ka5IvMOXFYnIos7Ng9z7fN/EErv5bEkusjA9uPIluVZW2cEHb/LxklxwJ7bhno67Kcstyf' +
						   'Wx1hPpJpZYkovvrE7cUyHazXJPpO/BmZ5ID+FDc/NFro+1nkhfzWmeSI/io3PzIN3HWmtUXMEJ1qgYyIktubM1Kj' +
						   'pYaI2Kz7L3GhVjObcrN7dGRQcLbVN3JjttU3cc6149jhdfrooOFtqm7mhW36buONatSFzhQ3trdI2KDhZ6RXUfa7' +
						   '2i+jjWXZK7wkf31u4aFR0stJvPXlG9m8+OY901Kq7w6b11vEZFH2tdwQmOY90Nii5yzfZaX6Oim+XOZ7+jWX2Nik' +
						   'tddgh3sEbFEax4DnsczeobFF3t4nO4jDUqjmPdPtY6hz02KLqH60/jVtaoOIc9XlF9MpttUHQndzmTG1qj4gQ22K' +
						   'buZDbboOhmbnQs97RGxXGsu49vTmCDDYpu6XaHc2dPpLtZ7n2+P4hFNyi6sTse0eWtUdHKKq2s0s1yGxTd201P6Q' +
						   'rXqHif79eoKETXqGhllQ2KIrj1WV3nGhW7+eyJ9BoVT6Tf5OMNiuK4+4nd6xoVO/hgSe4V1Utyu/lsjYpoYpzbHa' +
						   '9R8VdKKxL7+KYisYMPNigKKMzR3fQaFRsUVSTe4cuKxDZ1GxSFFawBt75GxRPpisQ7fFmR2KBojYrg4rXh+teoqE' +
						   'hUJN7n+4rEktwGRfFF7cQ7rFFx6NB8sUpF4j9CGxSNInA/HmTNVsFXvJlVKlvx2lfNYMJ35XF28EEfa+3ms+GM0J' +
						   'gn+iulR7DiK6oHNU57nmuDoiNYcZu6oQ3VpHdbo+IIVtygaHSj9en1luSOY90luTkM2K1nrEgcx7oViWmMPzeiR7' +
						   'N6ITqNnJtGVi9Ep5Fz08jqheg0RmvYMxai57BHITqHnJt29ihE55Bz084ehegccm7a2aMiMYGhWvV6heiZ7FSITi' +
						   'DnpoudCtEJ5Nx0sVMhOoGcmy52KkQnkHPTxU6F6ARybrrYqSIxunH69G6F6PnsV4iOLueml/0K0dHl3PSyXyE6up' +
						   'ybXvYrREeXc9PLfoXo6HJuetmvEB1dzk0v+xWio8u56WW/QnR0OTe97FeIji7nppf9CtHR5dz0sl8hOrqcm172K0' +
						   'RHl3PTy36F6OhybnrZrxAdXc5NL/sVoqPLuellv0J0dMPOzYPEyWxWiI5uqD49XSF6JjsVohPIuelip0J0Ajk3Xe' +
						   'xUiE4g56aLnQrRCeTcdLFTITqBnJsudipEJzBaqx6wED2HPSoSE8i5aWePQnQOOTft7FGIziHnpp09CtE5DNitZy' +
						   'xEj2b1QnQaOTeNrF6ITiPnppHVC9FpjD83DxLHsW5FYhpjNuwxC9HjWLcQnckUc/MgcQQrViRmMmzPnrQQPYIVC9' +
						   'HJ5Ny8zYqF6GRmmZsHiT7WqkhMZuS2PWxFopVVKhLzybl5g1UqEvMZtnMP+0T6fb6vSEwp52Yv31ckpjRm8x52g6' +
						   'J3+LIiMasZ5+ZB3T6+eSI9pQGb96qvqH5F9RoVUxqteU9a2Yp/+cquUvFXSuczy9w8+HuNikJ0Bx/MZ6jOPWZFoh' +
						   'BttbrC18qzGXluRJfk3uf7fbsMb5y2PWNFYo2KfXxTiFYkZjLs3Ij+ldJt6p5IF6IzGaRnD1iR2MEHS3IbFFUkpj' +
						   'FCw56uInEmOxWi08i5aWSnisQcwnfr0SoS57NfITqH0eZG9CNsWZGYQOxWPVdF4lPsWohOYKi5Ef0gG1ckRhe4Tw' +
						   '9Vkfgsexeio4vap1eqSHyc7SsSQ8u5OYATFKJDC9mk96lIXMQhKhLjGmFuRC/lKIXouOJ16GUqEpdylIrEoMLPje' +
						   'gNOFAhOqhg7XmTisQNOFBFYkSR7l20InEbjlWIjuguvbnpN/n4NhxrH9/EFHhufHknTrabzwKKOjc+uxmH281nAd' +
						   '3o6O5yH9/cjMPt5rOAbn10t/tE+pYccQcfxBT79OkqOTepRc5NapFzk1rk3KQWOTfpfX/+/A+85of7Tz43nQAAAABJRU5ErkJggg=="' +
				'}]';
			return JSON.parse(jsonScans);
		}
	};
	return wax;
}

function ChangeContentVariant()
{
	var content_selector = $('#cpw-content-select');
	var content_id = content_selector.val();
	var content = GetSelectedSpecificationFor_id(CPW_contents, content_id);

	var content_area = $('#form-content-text-area');
	var old_content = content_area.val();
	if (!old_content || '' == old_content || confirm('Вы действительно хотите заменить содержимое новым?'))
		$('#form-content-text-area').val(content);

	content_selector.val('');
}

function Print()
{
	alert(1);
	var content = app.current_form_spec.BuildHtmlContentToPrint();

	$('#page').addClass('non-printable');
	$('body')
	.addClass('body-printable')
	.append('<div id="printable" class="printable">' + content + '</div>');

	window.print();

	$('#printable').detach();
	$('body').removeClass('body-printable');
	$('#page').removeClass('non-printable');
}

function GetProfile()
{
	if (app.profile)
	{
		return app.profile;
	}
	else
	{
		var sprofile = $.cookie("cpw-forms-profile");
		if ('' == sprofile)
		{
			profile = {}
		}
		else
		{
			profile = JSON.parse(sprofile);
		}
		return profile;
	}
}

function LoadProfile()
{
	var sprofile = $('#form-content-text-area').val();
	app.profile = null == sprofile || '' == sprofile ? null : JSON.parse(sprofile);
	$.cookie("cpw-forms-profile", null == app.profile ? '' : JSON.stringify(app.profile), { expires: 365 });
}

function EditProfile()
{
	var profile = GetProfile();
	$('#form-content-text-area').val(JSON.stringify(profile));
	var form_spec_func = GetSelectedSpecificationFor_id(CPW_forms, 'profile.tabs');
	EditFormWithSpecFunc(form_spec_func);
}

function wbt_controller_GetFormContentTextArea()
{
	return $('#form-content-text-area').val().replace(new RegExp('\n', 'g'), '\r\n');
}

$(function ()
{
	$("#btn_test_forms_CreateNew").click(CreateNewForm);
	$("#btn_test_forms_CancelEdit").click(CancelEditWithoutSave);
	$('#btn_test_forms_SaveForm').click(SaveForm);
	$('#btn_test_forms_Print').click(Print);
	$('#btn_test_forms_Edit').click(EditForm);
	$('#btn_test_forms_View').click(ViewForm);
	$('#btn_test_forms_Reload').click(ReloadConfig);
	$('#btn_test_load_profile').click(LoadProfile);
	$('#btn_test_edit_profile').click(EditProfile);
	$('#cpw-content-select').change(ChangeContentVariant);
});
