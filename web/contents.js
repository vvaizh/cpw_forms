require.config
({
  enforceDefine: true,
  urlArgs: "bust" + (new Date()).getTime(),
  baseUrl: '.',
  map:
  {
    '*':
    {
      txt:  'js/libs/text'
    }
  }
});

require([
	  'contents/collector'
	, 'contents/test/index'
], function (collect)
{
	CPW_contents = collect(
	[
		  'test'
	], Array.prototype.slice.call(arguments, 1));

	var content_selector = $('#cpw-content-select');
	content_selector.append($('<option>', {
		value: '',
		text: ''
	}));
	for (var content_namespace in CPW_contents)
	{
		for (var content_name in CPW_contents[content_namespace])
		{
			var id_content = content_namespace + '.' + content_name;
			content_selector.append($('<option>', {
				value: id_content,
				text: id_content
			}));
		}
	}

	ProcessContentArgument= function(prefix, do_for_form_spec)
	{
		var url = location.href;
		var iprefix = url.indexOf(prefix);
		if (-1 != iprefix)
		{
			var arg = url.substring(iprefix + prefix.length);
			if (arg.indexOf('#') != -1)
				arg = arg.substring(0, arg.length - 1);

			var arg_parts = arg.split('.');
			var id_content = arg_parts[0] + '.' + arg_parts[2];
			var id_form = arg_parts[0] + '.' + arg_parts[1];

			var content = GetSelectedSpecificationFor_id(CPW_contents, id_content);
			if (!content)
			{
				alert('can not find content for id \"' + id_content + '\"');
			}
			else
			{
				var content_area = $('#form-content-text-area');
				$('#form-content-text-area').val(content);
			}

			$("#cpw-form-select [value='" + id_form + "']").attr("selected", "selected");
			var form_spec_func = GetSelectedFormSpecificationFor_id_Form(id_form);
			if (!form_spec_func)
			{
				alert('can not find specification for form \"' + id_form + '\"');
			}
			else
			{
				do_for_form_spec(form_spec_func);
			}
		}
	}

	ProcessContentArguments = function ()
	{
		ProcessContentArgument("?edit=", EditFormWithSpecFunc);
		ProcessContentArgument("?view=", ViewFormWithSpecFunc);
	}

	if (typeof (CPW_forms) != "undefined")
		ProcessContentArguments();

	$('#contents_loaded_span').show();
});
