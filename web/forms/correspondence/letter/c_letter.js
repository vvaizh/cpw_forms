define([
	  'forms/base/c_binded'
	, 'tpl!forms/correspondence/letter/e_letter.html'
],
function (binded_controller, tpl)
{
	return function ()
	{
		return binded_controller(tpl);
	}
});
