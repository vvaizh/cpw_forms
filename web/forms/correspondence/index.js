define
(
	[
		  'forms/correspondence/letter/c_letter'
	],
	function (c_letter)
	{
		var controls =
		{
			letter: c_letter
		};
		return controls;
	}
);