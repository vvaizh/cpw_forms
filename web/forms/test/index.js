define
(
	[
		  'forms/collector'
		, 'forms/test/xml/controller'
		, 'forms/test/tpl/controller'
		, 'forms/test/min/controller'
		, 'forms/test/base/controller'
	],
	function (collect)
	{
		return collect([
		  'xml'
		, 'tpl'
		, 'min'
		, 'base'
		], Array.prototype.slice.call(arguments,1));
	}
);