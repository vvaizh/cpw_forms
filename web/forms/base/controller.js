define(
['forms/base/log']
, function (GetLogger)
{
	return function ()
	{
		var log = GetLogger('base/controller');
		var controller =
		{
			CreateNew: function (sel) { alert('unimplemented CreateNew of form specifier for ' + sel); }
			, Edit: function (sel) { alert('unimplemented Edit of form specifier for ' + sel); }
			, GetFormContent: function () { alert('unimplemented GetFormContent'); return null; }
			, SetFormContent: function (content) { alert('unimplemented SetFormContent'); return 'unimplemented SetFormContent'; }
			, CopyFormContent: function (content) { alert('unimplemented CopyFormContent'); return 'unimplemented CopyFormContent'; }
			, BeforeEditValidation: function () { alert('unimplemented Validate'); return 'unimplemented Validate'; }
			, Validate: function () { alert('unimplemented Validate'); return 'unimplemented Validate'; }
			, BuildXamlView: function () { alert('unimplemented BuildXamlView of form specifier'); }
			, BuildHtmlViewForProfiledData: function () { return ""; }
			, Destroy: function () { }

			, Sign: function () { }

			, UseCodec: function (codec)
			{
				log.Debug('UseCodec() {');

				var old_GetFormContent = this.GetFormContent;
				var old_SetFormContent = this.SetFormContent;

				this.GetFormContent = function ()
				{
					log.Debug('Wrapper around GetFormContent() {');
					var res = this.SignedContent ? this.SignedContent :
						codec.Encode(old_GetFormContent.call(controller));
					if (codec.GetScheme)
					{
						try
						{
							log.Debug("Wrapper around GetFormContent codec have GetScheme, let's check Decode.. ");
							log.Debug("Wrapper around GetFormContent call codec.Decode {");
							codec.Decode(res);
							log.Debug("Wrapper around GetFormContent call codec.Decode }");
						}
						catch (ex)
						{
							throw {parseException:true,readablemsg:"Введенные данные не соответствуют требуемому формату.",innerException:ex};
						}
					}
					log.Debug('Wrapper around GetFormContent() }');
					return res;
				};

				this.SetFormContent = function (form_content)
				{
					log.Debug('Wrapper around SetFormContent {');
					var content = codec.Decode(form_content);
					var res = old_SetFormContent.call(controller, content);
					log.Debug('Wrapper around SetFormContent }');
					return res;
				};

				var old_UsedCodecInfo = this.UsedCodecInfo;
				this.UsedCodecInfo =
				{
					Codec: codec,
					previous:
					{
						CodecInfo: old_UsedCodecInfo,
						GetFormContent: old_GetFormContent,
						SetFormContent: old_SetFormContent
					}
				};

				log.Debug('UseCodec() }');
			}

			, GetDraftFormContent: function ()
			{
				var GetFormContent = this.GetFormContent;
				var CodecInfo = this.UsedCodecInfo;
				while (CodecInfo)
				{
					GetFormContent = CodecInfo.previous.GetFormContent;
					CodecInfo = CodecInfo.previous.CodecInfo;
				}
				return GetFormContent.call(this);
			}

			, StartUsingForm: function (id_form)
			{
				var form_spec_func = CpwFormsExtensionsConstructors[id_form];
				var form_spec = form_spec_func();
				window.current_form_spec = form_spec;
				for (var ie = 0; ie < CpwFormsExtensionsInfo.length; ie++)
				{
					var extension = CpwFormsExtensionsInfo[ie];
					$('#cpw-form-container div.ExtensionTitle').text(extension.Title);
				}
				$('#cpw-start-page').hide();
				var profile = GetProfile();
				form_spec.UseProfile(profile);
				$('#cpw-form-container').show();
				return form_spec;
			}

			, UseProfile: function (profile) { }
		};
		return controller;
	}

});