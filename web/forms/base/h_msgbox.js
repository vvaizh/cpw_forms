define(function ()
{
	var helper_res =
	{
		Create_OnOpen_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				options.controller.Edit('#cpw-msgbox');
			}
		}

		, Create_OnClose_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				options.controller.Destroy();
				options.controller = null;
				$('#cpw-msgbox').dialog('destroy');
			}
		}

		, PrepareDiv: function ()
		{
			var box_sel = '#cpw-msgbox';
			var box = $(box_sel);
			if (0 == box.length)
			{
				$('body').prepend('<div id="cpw-msgbox" class="cpw-msgbox" style="display:none">Здесь будет всплывающая форма</div>');
				box = $(box_sel);
			}
			return box;
		}

		, ShowModal: function (options)
		{
			var box = this.PrepareDiv();

			if (options.html)
			{
				box.html(options.html);
				delete options.html;
			}
			else if (options.controller)
			{
				options.open = this.Create_OnOpen_ForMsgBoxWithController(options);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
			}

			if (!options.modal)
				options.modal = true;
			if (!options.modal)
				options.resizable = false;

			var buttons = (!options.buttons) ? ['OK'] : options.buttons;

			var on_close = options.onclose ? options.onclose : function (bname) { };
			delete options.onclose;

			options.buttons = {};
			for (var i = 0; i < buttons.length; i++)
			{
				var button_name = buttons[i];
				options.buttons[button_name] =
					(function (bname, onclose) { return function () { onclose(bname); $(this).dialog("close"); } })
				(button_name, on_close);
			}

			box.dialog(options);
		}
	};
	return helper_res;
});
