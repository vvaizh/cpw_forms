﻿define([
	'forms/base/h_times'
],
function (helper_Times)
{
	var helper =
	{
		AddressErrorClass: 'address-errors'
		, ErrorClass: 'error'
		, AddressErrorText: 'Не заполнен обязательный реквизит'
		, ProfileErrorText: 'Не заполнены обязательные поля профиля'
		, PeriodErrorMsg: 'Дата выезда из места пребывания не может быть меньше даты въезда'
		, PeriodBirtdayErrorMsg: 'Дата выдачи документа не должна быть меньше даты рождения'
		, DocPeriodErrorMsg: 'Дата окончания действия документа не может быть меньше даты выдачи'
		, DocPeriodRegistrationErrorMsg: 'Дата окончания регистрации не может быть меньше даты начала регистрации'
		, DateBeforeErrorMsg: 'Значение должно быть не позднее текущей даты'
		, ValidateErrorMsg: 'Неверно заполнены поля, подсвеченные красным цветом'
		, ValidateErrorMsgConstruct: 'Неверно заполнены поля, подсвеченные красным цветом'
		, EmptyErrorMsg: 'Не может быть пустым'
		, FormatDateErrorMsg: 'Неверный формат даты'
		, FormatInnErrorMsg: 'Невалидный ИНН'
		, FormatNameErrorMsg: 'Может содержать только буквы (кириллица), цифры, символы: «.», «,», «\'», «-», «I», «V», «X», «L», «C», «D», «M», пробел'
		, AddressErrorTemplate: function ()
		{
			return '<span class="' + helper.AddressErrorClass + '"><span class="' + helper.ErrorClass + '">' + helper.AddressErrorText + '</span></span>';
		}

		, RequiredValues: function (value)
		{
			var res = value && '' != value && null != value && undefined != value;
			if (!res)
				helper.ValidateErrorMsgConstruct = helper.ProfileErrorText + '! ' + helper.ValidateErrorMsg;
			return res;
		}

		, ValidateSymbolsINN: function (e)
		{
			var key = e.charCode || e.keyCode || 0;
			return key == 8
				|| key == 9
				|| key == 35
				|| key == 36
				|| key == 45
				|| key == 46
				|| (key == 65 && e.ctrlKey)
				|| (key == 67 && e.ctrlKey)
				|| (key == 86 && e.ctrlKey)
				|| (key == 88 && e.ctrlKey)
				|| (key == 90 && e.ctrlKey)
				|| (key >= 37 && key <= 40)
				|| (key >= 48 && key <= 57 && !e.shiftKey)
				|| (key >= 96 && key <= 105);
		}

		, IsValidINN: function (inn)
		{
			if (!inn || inn == "")
			{
				return false;
			} else
			{
				if (inn.length == 10)
				{
					return inn[9] == String(((
							2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
							3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
							4 * inn[6] + 6 * inn[7] + 8 * inn[8]
						) % 11) % 10);
				} else if (inn.length == 12)
				{
					return inn[10] == String(((
							7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
							10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
							9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
							8 * inn[9]
						) % 11) % 10) && inn[11] == String(((
							3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
							4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
							5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
							6 * inn[9] + 8 * inn[10]
						) % 11) % 10);
				} else if (inn.length == 14)
				{
					return true;
				}
				return false;
			}
		}

		, ValidateINNVal: function (elem)
		{
			if (elem.val() == '')
				return true;
			var inn = elem.val();
			var res = helper.IsValidINN(inn);
			if (res)
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, helper.FormatInnErrorMsg);
			return res;
		}

		, ValidateINN: function (elem)
		{
			helper.ValidateBindAction('keyup', 'ValidateINNVal', elem);
			return helper.ValidateINNVal(elem);
		}

		, OnValidateINN: function (e)
		{
			if (e.keyCode != 9)
			{
				helper.ValidateINN($(e.target));
			}
		}

		, OnChangeDateInput_FixFormat: function (e)
		{
			var ditem = $(e.target);
			var old_txtdt = ditem.val();
			var can_be_0 = ditem.hasClass('can_be_0');
			txtdt = helper.ConvertTxtToDateFormat(old_txtdt, can_be_0);
			if (old_txtdt != txtdt)
				$(e.target).val(txtdt);
		}

		, OnConvertToDateFormat: function (e)
		{
			var ditem = $(e.target);
			if (ditem.val() != "")
			{
				helper.ConvertToDateFormat(ditem);
				helper.IsValidDateVal(ditem);
			}
		}

		, ConvertTxtToDateFormat: function (txtDate, can_be_0)
		{
			if ("" != txtDate)
			{
				var year, month, day;
				if (txtDate.indexOf('.') != -1)
				{
					var parts = txtDate.split('.');
					year = parts[2];
					month = parts[1];
					day = parts[0];
				}
				else
				{
					var parts = txtDate.split('-');
					year = parts[0];
					month = parts[1];
					day = parts[2];
				}
				var month_is_0 = (0 == month || '0' == month || '00' == month);
				var day_is_0 = (0 == day || '0' == day || '00' == day);
				if (year != undefined && month != undefined && day != undefined)
				{
					var res = $.datepicker.formatDate("dd.mm.yy", new Date(year, month_is_0 ? 1 : month - 1, day_is_0 ? 1 : day));
					if (can_be_0)
					{
						var parts = res.split('.');
						if (month_is_0)
							parts[1] = '00';
						if (day_is_0)
							parts[0] = '00';
						return parts.join('.');
					}
					return res;
				}
			}
			return txtDate;
		}

		, ConvertToDateFormat: function (elem)
		{
			var year, month, day;
			var txtDate = elem.val();
			if (txtDate != "")
			{
				var can_be_0 = elem.hasClass('can_be_0');
				var newDate = helper.ConvertTxtToDateFormat(txtDate, can_be_0);
				if (newDate != txtDate)
					elem.val(newDate);
			}
		}

		, IsValidDate: function (elem)
		{
			if (elem.val() == '') return true;
			helper.ValidateBindAction('change', 'IsValidDateVal', elem);
			return helper.IsValidDateVal(elem);
		}

		, IsValidDateVal: function (elem)
		{
			var res = true;
			if (elem.val() && elem.val() != "")
			{
				var parts = elem.val().split('.');
				var year = parts[2];
				var month = parts[1];
				var day = parts[0];
				res = year != undefined && year.length == 4
				   && month != undefined && month.length == 2
				   && day != undefined && day.length == 2
				if (res)
					helper.RemoveErrorClass(elem);
				else
					helper.AddErrorClass(elem, helper.FormatDateErrorMsg);
			}
			return res;
		}

		, IsValidPeriodDateParams: function (attrs)
		{
			helper.ValidateBindAction('change', 'IsValidPeriodDateParamsVal', attrs);
			return helper.IsValidPeriodDateParamsVal(attrs);
		}

		, IsValidPeriodDateParamsVal: function (attrs)
		{
			var res = true;
			var elemStartDate = attrs[0];
			var elemEndDate = attrs[1];
			if ((elemStartDate.val() == '' && elemEndDate.val() == '') ||
				(elemStartDate.val() != '' && (!elemEndDate || elemEndDate.val() == ''))) return true;
			var startDate = elemStartDate ? elemStartDate.val() : helper_Times.nowDateUTC('r');
			var endDate = elemEndDate ? elemEndDate.val() : helper_Times.nowDateUTC('r');
			var start_date = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var end_date = new Date(endDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var error = attrs[2];
			var title = error ? error : '';
			res = start_date <= end_date;
			if (!res && elemStartDate)
				helper.AddErrorClass(elemStartDate, title);
			else if (res && elemStartDate)
				helper.RemoveErrorClass(elemStartDate);
			if (!res && elemEndDate)
				helper.AddErrorClass(elemEndDate, title);
			else if (res && elemEndDate)
				helper.RemoveErrorClass(elemEndDate);
			return res;
		}

		, OnValidateDateIsNotEmpty: function (e)
		{
			var elem = $(e.target);
			helper.ValidateIsNotEmptyVal(elem);
			helper.IsValidDateVal(elem);
		}

		, OnValidateDateBeforeNow: function (elem)
		{
			helper.IsValidDateBeforeNowVal(elem);
		}

		, IsValidDateBeforeNow: function (elem)
		{
			helper.ValidateBindAction('change', 'IsValidDateBeforeNowVal', elem);
			return helper.IsValidDateBeforeNowVal(elem);
		}

		, IsValidDateBeforeNowVal: function (elem)
		{
			return elem.val() == "" ||
				   (helper.IsValidDateVal(elem) &&
				   helper.IsValidPeriodDateParamsVal([elem, null, helper.DateBeforeErrorMsg]));
		}

		, OnValidateIsNotEmpty: function (e)
		{
			if (e.keyCode != 9)
			{
				var el = $(e.target);
				helper.ValidateIsNotEmptyVal(el);
			}
		}

		, ValidateIsNotEmpty: function (elem)
		{
			helper.ValidateBindAction('focusout', 'ValidateIsNotEmptyVal', elem);
			return helper.ValidateIsNotEmptyVal(elem);
		}

		, ValidateIsNotEmptyVal: function (elem)
		{
			var res = true;
			if (res = ($.trim(elem.val()).length != 0))
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, helper.EmptyErrorMsg);
			return res;
		}

		, OnValidateSelect2IsNotEmpty: function (e)
		{
			if (e.keyCode != 9)
			{
				var el = $(e.target);
				helper.ValidateIsNotEmptyVal(el);
			}
		}

		, ValidateSelect2IsNotEmpty: function (elem)
		{
			helper.ValidateBindAction('select2-close', 'ValidateSelect2IsNotEmptyVal', elem);
			helper.ValidateBindAction('select2-removed', 'ValidateSelect2IsNotEmptyVal', elem);
			return helper.ValidateSelect2IsNotEmptyVal(elem);
		}

		, ValidateSelect2IsNotEmptyVal: function (elem)
		{
			var res = true;
			if (res = ($.trim(elem.val()).length != 0))
				helper.RemoveErrorClass(elem.siblings('.select2-container'));
			else
				helper.AddErrorClass(elem.siblings('.select2-container'), helper.EmptyErrorMsg);
			return res;
		}

		, OnValidateAddressIsNotEmpty: function (attrs)
		{
			$('#' + attrs[1]).unbind('focusout.ValidateAddressIsNotEmpty');
			$('#' + attrs[1]).unbind('changeAddressText');
			$('#' + attrs[1]).on('changeAddressText', function ()
			{
				helper.ValidateAddressIsNotEmpty(attrs);
			});
			$('#' + attrs[1]).on('focusout.ValidateAddressIsNotEmpty', function ()
			{
				helper.ValidateAddressIsNotEmpty(attrs);
			});
			return helper.ValidateAddressIsNotEmpty(attrs);
		}

		, ValidateAddressIsNotEmpty: function (attrs)
		{
			var address = attrs[0] ? attrs[0] : null;
			var idControl = attrs[1] ? attrs[1] : null;
			if (!address || !idControl)
				return false;

			var res = address != 'undefined' && (null != address.Street || address.text && '' != address.text && null != address.text);
			if (res)
			{
				$('#' + idControl).siblings('.' + helper.AddressErrorClass).remove();
			}
			else
			{
				if ($('#' + idControl).siblings('.' + helper.AddressErrorClass).size() == 0)
					$('#' + idControl).after(helper.AddressErrorTemplate());
			}
			return res;
		}

		, IsValidControlsOnTabPage: function (resultValidation, tabPage)
		{
			if (resultValidation)
				helper.RemoveErrorClass(tabPage);
			else
				helper.AddErrorClass(tabPage);

		}

		, IsValidName: function (elem)
		{
			var regexp = /[^А-Я-Ёа-я-ё0-9IVXLCDM—\s\.,\'\-]/
			var value = elem.val();
			var res = !regexp.test(value);
			if (res)
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, helper.FormatNameErrorMsg);
			return res;
		}

		, AddErrorClass: function (elem, title)
		{
			elem.addClass(helper.ErrorClass);
			elem.prop('title', title);
		}

		, RemoveErrorClass: function (elem)
		{
			elem.removeClass(helper.ErrorClass);
			elem.prop('title', '');
		}

		, ValidateProfileCommonData: function (content)
		{
			var messageError = '';
			if (!content.supplierInfo || content.supplierInfo == "")
				messageError += '\r\n   "' + 'Уникальный идентификатор системы, передающей данные, получаемый в ППО "Территория"' + '"';
			if (!content.subdivision || content.subdivision.id == -1)
				messageError += '\r\n   "' + 'Территориальный орган / структурное подразделение ФМС' + '"';
			if (!content.employee || content.employee.name == "")
				messageError += '\r\n   "' + 'Строка, идентифицирующая пользователя (ФИО / логин)' + '"';
			if (!content.employee || content.employee.ummsId == "")
				messageError += '\r\n   "' + 'Идентификатор пользователя в ППО "Территория"' + '"';
			return messageError;
		}
		, ValidateBindAction: function (action, title, elem)
		{
			if (elem instanceof window.Array)
			{
				for (var i = 0; i < elem.length; i++)
				{
					helper.AddBind(action, title, elem[i], elem);
				};
			} else
			{
				helper.AddBind(action, title, elem);
			}
		}
		, AddBind: function (action, title, elem, attrs)
		{
			var a = action + '.' + title;
			var e = typeof (elem) === 'object' ? elem : $(elem);
			var p = attrs ? attrs : elem;
			e.unbind(a);
			e.on(a, function ()
			{
				helper[title](p);
			});
		}
	}
	return helper;
});