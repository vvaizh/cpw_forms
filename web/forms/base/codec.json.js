define([
	  'forms/base/codec'
],
function (BaseCodec)
{
	return function()
	{
		var res = BaseCodec();

		res.Encode = function (data)
		{
			return JSON.stringify(data);
		};

		res.Decode = function (json_string)
		{
			return JSON.parse(json_string);
		};

		return res;
	}
});
