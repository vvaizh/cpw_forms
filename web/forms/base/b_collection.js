define(function (BaseController)
{
	return function ()
	{
		var binding =
		{
			empty_attrs: function ()
			{
				var res = (!this.model || 0 == this.model.length) ? '' : ' style="display:none" ';
				res += 'class="empty-list"';
				return res;
			},

			not_empty_attrs: function ()
			{
				var res = (this.model && 0 != this.model.length) ? '' : ' style="display:none" ';
				res += 'class="not-empty-list"';
				return res;
			},

			iterate: function ()
			{
				if (!this.CurrentCollectionIndex && 0 != this.CurrentCollectionIndex)
				{
					this.CurrentCollectionIndex = -1;
					return true;
				}
				else
				{
					this.CurrentCollectionIndex++;
					var model_len = !this.model ? 0 : this.model.length;
					return this.CurrentCollectionIndex < model_len;
				}
			},

			is_seed: function ()
			{
				return -1 == this.CurrentCollectionIndex;
			},

			item_attrs: function ()
			{
				if (-1 == this.CurrentCollectionIndex)
				{
					return ' style="display:none" class="seed" ';
				}
				else
				{
					return ' class="collection-item" collection-index="' + + this.CurrentCollectionIndex + '"';
				}
			},

			text_func_attrs: function (prepare_text_by_item)
			{
				if (!this.collection_item_text_funcs)
					this.collection_item_text_funcs = [];
				this.collection_item_text_funcs.push(prepare_text_by_item);
				return ' class="collection-item-text-' + (this.collection_item_text_funcs.length - 1) + '"';
			},

			text_field_attrs: function (item_field_name)
			{
				return this.text_func_attrs
				(
					function (item)
					{
						return item[item_field_name];
					}
				);
			},

			field: function (item_field_name)
			{
				return this.is_seed() ? '' : this.model[this.CurrentCollectionIndex][item_field_name];
			},

			text_field: function (item_field_name)
			{
				var res = '<span ' + this.text_field_attrs(item_field_name) + ' >';
				if (!this.is_seed())
					res += this.model[this.CurrentCollectionIndex][item_field_name];
				return res;
			},

			text_field_end: function ()
			{
				return '</span>';
			},

			item_position1: function()
			{
				var res = '<span ' + this.text_func_attrs(function(item,i_item){return i_item+1;}) + ' >';
				if (!this.is_seed())
					res += '' + (this.CurrentCollectionIndex + 1);
				return res;
			},

			item_position1_end: function ()
			{
				return '</span>';
			},

			safe_prepare_dom_item: function (i_item)
			{
				var dom_item = $(this.form_div_selector + ' *[collection-index="' + i_item + '"]');
				if (dom_item.length)
				{
					return dom_item;
				}
				else
				{
					var seed = $(this.form_div_selector + ' .not-empty-list .seed');
					var clone = seed.clone();
					clone.attr('collection-index', i_item);
					clone.removeClass('seed');
					clone.addClass('collection-item');
					clone.insertAfter(0 == i_item ? seed : $(this.form_div_selector + ' *[collection-index="' + (i_item-1) + '"]'));
					clone.show();
					return clone;
				}
			},

			find_binding_for_item: function(dom_item)
			{
				var id_prefix = this.id_form_div + '-item-';
				var i_item;
				do
				{
					dom_item = dom_item.parent();
					i_item = dom_item.attr('collection-index');
				}
				while (dom_item && (!i_item || null==i_item));
				var res = { dom_item: dom_item };
				if (i_item && null!=i_item)
					res.i_item = i_item;
				return res;
			},

			fill_dom_item: function (i_item, model_item, dom_item)
			{
				dom_item.removeClass('deprecated');
				for (var i = 0; i < this.collection_item_text_funcs.length; i++)
				{
					var prepare_text_by_item = this.collection_item_text_funcs[i];
					var txt = prepare_text_by_item(model_item,i_item);
					var id_dom_item = dom_item.attr('id');
					$(this.form_div_selector + ' *[collection-index="' + i_item + '"]' + ' .collection-item-text-' + i).text(txt);
				}
			},

			update_dom: function ()
			{
				var selector_not_empty = this.form_div_selector + ' .not-empty-list';
				var dom_empty = $(this.form_div_selector + ' .empty-list');
				var dom_not_empty = $(selector_not_empty);
				$(this.form_div_selector + ' .collection-item').addClass('deprecated');
				if (!this.model || 0 == this.model.length)
				{
					dom_empty.show();
					dom_not_empty.hide();
				}
				else
				{
					dom_empty.hide();
					dom_not_empty.show();
					var seed = $(selector_not_empty + ' .seed');
					for (var i = 0; i < this.model.length; i++)
					{
						var dom_item = this.safe_prepare_dom_item(i);
						var model_item = this.model[i];
						this.fill_dom_item(i, model_item, dom_item);
					}
				}
				$(this.form_div_selector + ' .deprecated').remove();
				if (this.on_after_update_dom)
					this.on_after_update_dom();
			}
		};
		return binding;
	}
});
