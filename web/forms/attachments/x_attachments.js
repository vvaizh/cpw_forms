﻿define([
	'forms/base/log'
],
function (GetLogger)
{
	var log = GetLogger('x_attachments');
	return function(BaseCodec)
	{
		var res = 
		{
			Encode: function (content)
			{
				log.Debug('Encode()');
				return content;
			},

			Decode: function (content)
			{
				log.Debug('Decode()');
				return content;
			}
		}
		return res;
	}
});