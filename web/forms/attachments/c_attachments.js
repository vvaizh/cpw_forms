﻿define([
	  'forms/attachments/x_attachments'
	, 'tpl!forms/attachments/attachments.html'
	, 'tpl!forms/attachments/attachment.html'
	, 'tpl!forms/attachments/note.html'
	, 'forms/base/log'
	, 'forms/base/h_file'
	, 'forms/base/h_times'
	, 'forms/base/h_select2'
	, 'forms/attachments/h_DocumentType'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_dictionary'
],
function (codec
	, attachments_tpl
	, attachment_tpl
	, note_tpl
	, GetLogger
	, helper_File
	, helper_times
	, helper_Select2
	, helper_DocumentType
	, helper_msgbox
	, helper_dictionary
)
{
	return function (BaseFormController, formSpec)
	{
		var log = GetLogger('c_attachments');
		var controller = BaseFormController();

		var form = formSpec;

		controller.ShowAttachmentsPanel = false;

		controller.result_Data =
		{
			subject: '',
			body: '',
			attachments: [],
			note: ''
		}

		var baseform_BuildHtmlViewForProfiledData = controller.BuildHtmlViewForProfiledData;
		controller.BuildHtmlViewForProfiledData = function ()
		{
			return baseform_BuildHtmlViewForProfiledData.call(controller);
		};

		var baseform_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var attachments = [];
			var content = baseform_GetFormContent();
			var name = helper_File.FMSPrepareFileName(form.FilePrefix, helper_times.safeDateTime());
			attachments.push
			({
				Name: name,
				FileName: name + '.' + form.FileExtension,
				FilePath: name + '.' + form.FileExtension,
				FileSize: helper_File.GetSizeOfUtf8(content),
				Description: '',
				Format: '',
				Content: content,
				Note: form.Description,
				AttachmentType: 'f'
			});
			var scans = this.CollectAttachments();
			this.result_Data.attachments = attachments.concat(scans);
			this.result_Data.body = this.GetBody();
			if (scans.length > 0)
				this.result_Data.body += '\r\nК документу приложены скан-копии документов в количестве ' + scans.length + ' шт.';
			if ($('.scan-note').val() && $('.scan-note').val() != '')
			{
				this.result_Data.body += '\r\n' + $('.scan-note').val();
				this.result_Data.note += '\r\n' + $('.scan-note').val();
			}
			return this.result_Data;
		};

		var baseform_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			var res = baseform_SetFormContent.call(controller, !content.attachments || content.attachments.length < 1 ? '' : content.attachments[0].Content);
			this.SetAttachments(content.attachments.slice(1), content.note);
			return res;
		};

		var baseform_CopyFormContent = controller.CopyFormContent;
		controller.CopyFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			baseform_CopyFormContent.call(controller, !content.attachments || content.attachments.length < 1 ? '' : content.attachments[0].Content);
			this.SetAttachments(content.attachments.slice(1), '');
		}

		var baseform_CreateNew = controller.CreateNew;
		controller.CreateNew = function (id_form_div)
		{
			baseform_CreateNew.call(controller, id_form_div);
			this.RenderFormAttachments();
		};

		var base_Edit = controller.Edit;
		controller.Edit = function (form_div_selector)
		{
			base_Edit.call(this, form_div_selector);
			this.RenderFormAttachments();
		};

		var base_Sign = controller.Sign;
		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content && null != content.attachments.length > 0)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var form_content = content.attachments[0].Content;
					var signature = wax.SignXml(form_content);
					var pos = !this.model && this.content ? this.content.PositionForSignature : this.model.PositionForSignature;
					var signedContent = form_content.substring(0, pos)
						+ signature
						+ form_content.substring(pos, content.length);
					content.attachments[0].Content = signedContent;
					this.SignedContent = content;
				}
			}
		};

		var RenderSelect2 = function (select2_element, h_values)
		{
			select2_element.select2({
				placeholder: '',
				allowClear: true,
				query: function (query) {
					query.callback({ results: helper_Select2.FindForSelect(!controller.GetDocumentTypeValues ? h_values.GetValues() : controller.GetDocumentTypeValues(false, false), query.term) });
				}
			});
		}

		controller.RenderFormAttachments = function ()
		{
			if (this.ShowAttachmentsPanel && (!$('#scan-list') || $('#scan-list').length == 0))
			{
				var countChild = $('.cpw-forms-attachments').children().length;
				if ($('.cpw-forms-attachments').children()[countChild - 1].localName != "hr")
					$('.cpw-forms-attachments').append('<hr/>');
				$('.cpw-forms-attachments').append(attachments_tpl());
				$('#add-scans').click(function (e) { OnAddScans(e); });
				LoadAttachments(this.attachments, this.note);
			}
		}

		controller.UpdateVisibilityDocumentTypes = function (newIdDocumentType, oldIdDocumentType) { }

		var RenderSelectDocumentType = function(scanType)
		{
			var indexScanDocumentType = $('.scan_DocumentType').length;
			if (indexScanDocumentType > 0) {
				RenderSelect2($('.scan_DocumentType').last(), helper_DocumentType);
				$('.scan_DocumentType').last().select2('data', scanType);
			}
		}

		var LoadAttachments = function (attachments, note)
		{
			if (attachments)
			{
				var availableTypes = controller.GetDocumentTypeValues(true, false);
				for (var i = 0; i < attachments.length; i++)
				{
					var attachment = attachments[i];
					if ('object' == (typeof attachment.Description))
					{
						LoadAttachment(attachment, attachment.Description);
					}
					else
					{
						if (null != availableTypes && availableTypes.length > 0)
						{
							var descriptionIndex = FindAttachmentType(availableTypes, attachment.Description);
							LoadAttachment(attachment, descriptionIndex == -1 ? null : availableTypes[descriptionIndex]);
							availableTypes.splice(descriptionIndex, 1);
						}
					}
				}
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				$('#scan-list .scan_DocumentType').on('change', function (e) { DocumentType_Change(e); });
				if (attachments.length > 0)
					AddNoteFormScans(note);
			}
		}

		var LoadAttachment = function (attachment, description)
		{
			var htmlScans = PrepAttachmentHtml(attachment.Name, attachment.FileName, attachment.FileSize, attachment.Content);
			$('#scans').append(htmlScans);
			RenderSelectDocumentType(description);
			controller.UpdateVisibilityDocumentTypes(!description || null == description ? '' : description.id, '');
		}

		var FindAttachmentType = function (availableTypes, description)
		{
			for (var j = 0; j < availableTypes.length; j++)
			{
				if (description == availableTypes[j].text)
					return j;
			}
			return -1;
		}

		var OnAddScans = function (e)
		{
			e.preventDefault();
			var wax = CPW_Singleton();
			if (wax)
				var scans = wax.LoadScans('Файлы изображений|*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff', '{Formats: "jpeg", MaxSizeBytes:2000000, ResolutionDPI:{min:200,max:300}}');
			if (scans)
			{
				for (var i = 0; i < scans.length; i++)
				{
					var scan = scans[i];
					var shortName = scan.path.split('\\');
					shortName = shortName[shortName.length - 1];
					var htmlScans = PrepAttachmentHtml(shortName, scan.path, scan.size, scan.content);
					$('#scans').append(htmlScans);
					RenderSelectDocumentType(controller.GetDefaultDocumentType ? controller.GetDefaultDocumentType() : { id: 0, text: "Иной документ" });
				}
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				$('#scan-list .scan_DocumentType').on('change', function (e) { DocumentType_Change(e); });
				if (scans.length > 0)
					AddNoteFormScans('');
			}
			controller.DoValidateAttachments(null);
		}

		var AddNoteFormScans = function(note)
		{
			note = !note ? '' : note;
			if ($('#scans-notes').html() == "")
			{
				$('#scans-notes').html(note_tpl({ note: note }));
			}
		}

		var PrepAttachmentHtml = function (name, filepath, size, content)
		{
			return attachment_tpl({ name: name, filepath: filepath, size: size, sizetext: helper_File.GetNumberingBytes(size), content: content });
		}

		var OnDeleteAttachmentClick = function (e)
		{
			e.preventDefault();
			var docWrap = $(e.target).closest('.doc-wrap');
			var typeDoc = docWrap.find('.scan_DocumentType').select2('data');
			controller.UpdateVisibilityDocumentTypes('', !typeDoc && null != typeDoc ? '' : typeDoc.id);
			docWrap.remove();
			if ($('#scans-notes').html() != "" && $('#scans .doc-wrap').length == 0)
			{
				$('#scans-notes').html('');
			}
			controller.DoValidateAttachments(null);
		}

		var OnShowScreenshotClick = function (e)
		{
			e.preventDefault();
			var docWrap = $(e.target).closest('.doc-wrap');
			if (docWrap)
			{
				var content = docWrap.find('.content').html();
				$('#modal-dialog-scan').dialog({
					width: 600,
					height: 600,
					resizable: true,
					modal: true,
					title: docWrap.find('.file-name').html(),
					buttons: {
						'Закрыть': function () {
							$(this).dialog('close');
						}
					}
				});
				$('#modal-dialog-scan-content').html('<div class="attach-block"><img style="max-width:500px;" src="data:image/png;base64,' + content + '"/></div>');
			}
		}

		controller.CollectAttachments = function()
		{
			var attachments= [];
			$('.doc-wrap').each(function (counter)
			{
				if ($(this).data('virtual') !== 1)
				{
					var size= $(this).find('.size').html().toString();
					var filename = $(this).find('.file').html();
					var name= $(this).find('.file-name').html();
					var content= $(this).find('.content').html();
					var description = $(this).find('.scan_DocumentType').select2('data');
					attachments.push
					({
						Name: name,
						FileName: name,
						FilePath: name,
						FileSize: size,
						Description: description,
						Format: 'text/binary',
						Content: content,
						Note: '',
						AttachmentType: 's'
					});
				}
			});
			return attachments;
		}

		controller.SetAttachments = function (attachments, note)
		{
			this.attachments = attachments;
			this.note = note;
			return null;
		}

		var base_Validate = controller.Validate;
		controller.Validate = function ()
		{
			var res_txt = base_Validate.call(this);
			return controller.DoValidateAttachments(res_txt);
		}

		controller.DoValidateAttachments = function(res_txt)
		{
			if (controller.ShowAttachmentsPanel)
			{
				res_txt = ValidateAttachments(res_txt);
			}
			return res_txt;
		}

		var ValidateAttachments = function(res_txt)
		{
			var types = controller.GetDocumentTypeValues(true, true);
			var attachments = controller.CollectAttachments();
			var check_constraint_result = true;
			for(var i = 0; i < types.length; i++)
			{
				var findAttach = false;
				for(var j = 0; j < attachments.length; j++)
				{
					if (attachments[j].Description && null != types[i] && types[i].id == attachments[j].Description.id && !attachments[j].find)
					{
						attachments[j].find = true;
						findAttach = true;
						break;
					}
				}
				if (!findAttach)
				{
					check_constraint_result &= findAttach;
					res_txt = BuildValidationText(res_txt, types[i]);
				}
			}
			AfterValidation(check_constraint_result);
			return res_txt;
		}

		var DocumentType_Change = function (e)
		{
			e.preventDefault();
			controller.UpdateVisibilityDocumentTypes(e.val, !e.removed ? '' : e.removed.id);
			ValidateAttachments(null);
		}

		var BuildValidationText = function(res_txt, type)
		{
			if (null == res_txt)
			{
				res_txt = [];
			}
			if ('string' == typeof res_txt)
			{
				res_txt = res_txt == null ? '' : res_txt + '\r\n';
				res_txt += 'Не прикреплена скан-копия документа "' + type.text + '"';
				res_txt += (!type.series || type.series == '') && (!type.number || type.number == '') ? '' : ',';
				res_txt += !type.series || type.series == '' ? '' : ' серия ' + type.series;
				res_txt += !type.number || type.number == '' ? '' : ' №' + type.number;
			}
			else if ('[object Array]' == Object.prototype.toString.call(res_txt))
			{
				var message = 'Не прикреплена скан-копия документа "' + type.text + '"';
				message += (!type.series || type.series == '') && (!type.number || type.number == '') ? '' : ',';
				message += !type.series || type.series == '' ? '' : ' серия ' + type.series;
				message += !type.number || type.number == '' ? '' : ' №' + type.number;
				res_txt.push({ check_constraint_result: false, description: message });
			}
			return res_txt;
		}
		var AfterValidation = function(check_constraint_result)
		{
			if (!check_constraint_result && !$('#add-scans').parent().parent().hasClass('invalid'))
			{
				$('#add-scans').parent().parent().addClass('invalid');
				$('#add-scans').parent().parent().find('.field-hint').click(ShowMessageForField);
			}
			else if (check_constraint_result && $('#add-scans').parent().parent().hasClass('invalid'))
			{
				$('#add-scans').parent().parent().removeClass('invalid');
			}
		}

		var ShowMessageForField = function ()
		{
			var validation_result = controller.DoValidateAttachments(null);
			if (validation_result && validation_result.length > 0)
			{
				var descriptions = [];
				for(var i = 0; i < validation_result.length; i++)
				{
					descriptions.push(validation_result[i].description);
				}
				helper_msgbox.ShowModal({
					title: 'Пояснения для поля'
					, html: '<ul><li>' + descriptions.join('</li><li>') + '</li></ul>'
					, width: 600
				});
			}
		}

		controller.UseCodec(codec());

		return controller;
	}
});