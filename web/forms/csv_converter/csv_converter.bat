@set csv_path=%~dp0..\fms\guides

@pushd %~dp0
@call CScript //nologo "csv_converter.js" convert_to_json %csv_path%
@popd

@pause