if (WScript.FullName.indexOf("cscript.exe") == -1) {
  WScript.Echo("This script may be runned only with CScript..")
  WScript.Quit(1)
}

FSO = new ActiveXObject("Scripting.FileSystemObject");
WshShell = new ActiveXObject("WScript.Shell");

varg = WScript.Arguments;

function ConvertToJson(csv_path) {
  if (!FSO.FolderExists(csv_path)) {
    WScript.Echo('Path: "' + csv_path + '" not found.');
  }
  Folder = FSO.GetFolder(csv_path);
  FC = new Enumerator(Folder.Files);
  for (; !FC.atEnd(); FC.moveNext()) {
    goToJsonConvert(FC.item());
  }
}

function goToJsonConvert(File) {
  WshShell.Run('"D:\\DevTools\\iconv\\iconv.exe" -f utf-8 -t cp1251 ' + File.Path + ' > ' + File.Path, 0);
  FileName = File.Name.split('.');
  FileTitle = FileName[0];
  FileType = FileName[1];

  if (FileType != 'csv')
    return false;

  SavePath = File.ParentFolder + '\\' + FileTitle + '.txt';

  JsonFile = FSO.CreateTextFile(SavePath);

  TextStream = File.OpenAsTextStream(1);

  ls = '[';
  for (; !TextStream.AtEndOfStream; TextStream.moveNext) {
    ls += '[' + TextStream.ReadLine() + '],';
  }
  ls = ls.slice(0, -1);
  ls += ']';

  JsonFile.Write(ls);
  TextStream.Close();
  JsonFile.Close();
  WshShell.Run('"D:\\DevTools\\iconv\\iconv.exe" -f cp1251 -t utf-8 ' + File.Path + ' > ' + File.Path, 0);
}

if (varg.Count() > 0) {
  cmd = varg.Item(0);
  switch (cmd) {
    case 'convert_to_json':
      ConvertToJson(varg.Item(1));
      break;
    default:
      WScript.Echo("unknown cmd=" + cmd);
  }
}