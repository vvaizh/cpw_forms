﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/statements/certificate_i/e_certificate_i.html'
	, 'tpl!forms/statements/certificate_i/v_certificate_i.xaml'
	, 'tpl!forms/statements/certificate_i/v_certificate_v2_i.xaml'
	, 'forms/statements/certificate_i/m_certificate_i'
	, 'forms/statements/certificate_i/x_certificate_i'
	, 'forms/statements/certificate_i/r_certificate_i'
	, 'forms/base/h_select2'
	, 'forms/base/h_names'
	, 'forms/base/h_times'
	, 'forms/base/h_validate_n'
	, 'forms/statements/base/h_CSP'
	, 'forms/statements/base/h_Extensions'
	, 'forms/statements/base/h_KeyGenerateBy'
	, 'forms/statements/base/h_RosregisterI'
	, 'forms/base/h_dictionary'
	, 'forms/guides/dict_officialorgan_fms.csv'
	, 'forms/base/log'
],
function (
	BaseFormController
	, layout_tpl
	, print_tpl
	, print_tpl_v2
	, model_certificate_i
	, codec
	, r_certificate_i
	, helper_Select2
	, helper_Names
	, helper_Times
	, helper_Validate
	, helper_CSP
	, helper_Extensions
	, helper_KeyGenerateBy
	, helper_Rosregister
	, h_dictionary
	, h_fms
	, GetLogger) {
	return function () {
		var log = GetLogger('c_certificate_i');

		var Select2Options = function (h_values) {
			return function (m) {
				return {
					placeholder: '',
					allowClear: true,
					query: function (q) {
						q.callback({ results: helper_Select2.FindForSelect(h_values(), q.term) });
					}
				};
			}
		}

		var formatResult = function (item) {
			return '<div class="cpw-fms-official-organ"><div class="code">код:' +
				item.row.CODE + '</div><div class="name">' +
				item.row.NAME + '</div></div>';
		}

		var formatSelection = function (item) {
			if (!item.row) {
				return item.text;
			}
			else {
				return '<div class="cpw-fms-official-organ" title="' +
					item.row.NAME + '"><div class="name">' +
					item.row.NAME + '</div><div class="code">код:<br/>' +
					item.row.CODE + '</div></div>';
			}
		}

		var controller = BaseFormController(layout_tpl,
		{
			constraints: r_certificate_i()
			, field_spec:
			{
				CSP: Select2Options(helper_CSP.GetValues)
				, KeyGenerateBy: Select2Options(helper_KeyGenerateBy.GetValues)
				, RosregisterNumber: Select2Options(helper_Rosregister.GetValues)
			}
		});

		controller.ShowProfiledData = false;

		controller.SafeCreateNewContent = function () {
			if (!this.model)
				this.model = model_certificate_i();
		}

		controller.UseProfile = function (profile) {
			this.SafeCreateNewContent();
			if (profile && profile.Extensions && profile.Extensions.statements) {
				var pextn = profile.Extensions.statements;
				if (pextn && pextn.User) {
					if (pextn.User.FirstName) {
						this.model.FirstName = pextn.User.FirstName;
					}
					if (pextn.User.LastName) {
						this.model.LastName = pextn.User.LastName;
					}
					if (pextn.User.MiddleName) {
						this.model.MiddleName = pextn.User.MiddleName;
					}
					if (pextn.User.CurrentCert) {
						this.model.CurrentCert = pextn.User.CurrentCert;
					}
				}
				if (pextn.Forma) {
					this.ShowAttachmentsPanel = pextn.Forma.ShowAttachmentsPanel;
				}
			}
		}

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
			base_Render.call(this, id_form_div);

			$('#cpw_form_Phone').inputmask("+7(999)999-99-99");

			$('#cpw_form_LastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$("#cpw_form_DocumentSeries").keydown(helper_Validate.ValidateSymbolsINN);
			$("#cpw_form_DocumentNumber").keydown(helper_Validate.ValidateSymbolsINN);

			$('#cpw_form_DocumentGivenDate').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_Date').datepicker(helper_Times.DatePickerOptions);

			$('#cpw_form_CSP').select2('enable', false);

			$('#cpw_form_DocumentGivenBy').select2({
				placeholder: ''
				, allowClear: true
				, dropdownCssClass: "bigdrop"
				, query: function (q) { q.callback({ results: h_dictionary.FindByNameOrCode(h_fms, q.term) }); }
				, escapeMarkup: function (m) { return m; }
				, formatResult: formatResult
				, formatSelection: formatSelection
			});

			$('#cpw_form_DocumentGivenBy_check').change(function (e) {
				var checked = $(this).attr('checked') == 'undefined' ? this.checked : $(this).attr('checked');
				if ('checked' == checked) {
					$('#s2id_cpw_form_DocumentGivenBy').show();
					$('#cpw_form_DocumentGivenBy_text').hide();
				}
				else {
					$('#s2id_cpw_form_DocumentGivenBy').hide();
					$('#cpw_form_DocumentGivenBy_text').show();
				}
			});

			$('#cpw_form_Rosregister_check').on("change", function (e) {
				if ($('#cpw_form_Rosregister_check').attr('checked')) {
					$('#cpw_form_RosregisterNumber').select2('enable', true);
				}
				else {
					$('#cpw_form_RosregisterNumber').select2('enable', false);
					$('#cpw_form_RosregisterNumber').select2('data', null);
				}
			});

			$('.cpw_certificate_i_form .datepicker').on("change", function (e) { helper_Validate.OnChangeDateInput_FixFormat(e); });

			$('.short').click(function (e) {
				e.preventDefault();
				if ($('.row_description').hasClass('hidden'))
					$('.row_description').removeClass('hidden');
				else
					$('.row_description').addClass('hidden');
			});
			DataLoad(this.model);
		}

		var DataLoad = function (content) {
			$('#cpw_form_RosregisterNumber').select2('data', null);
			$('#cpw_form_Rosregister_check').change();
			if (content.Extensions) {
				var txt_extension = content.Extensions.join(';') + ';';
				$('#cpw_form_ETPGPB_check').attr('checked', -1 != txt_extension.indexOf(helper_Extensions.ValuesI.ETPGPB) ? 'checked' : null);
				$('#cpw_form_Fabricant_check').attr('checked', -1 != txt_extension.indexOf(helper_Extensions.ValuesJ.Fabricant) ? 'checked' : null);
				$('#cpw_form_FTSRussian_check').attr('checked', -1 != txt_extension.indexOf(helper_Extensions.ValuesI.FTSRussian) ? 'checked' : null);
				$('#cpw_form_LicenseCSP_check').attr('checked', -1 != txt_extension.indexOf(helper_Extensions.ValuesJ.LicenseCSP) ? 'checked' : null);
				var rValues = helper_Rosregister.GetValues();
				for (var i = 0; i < rValues.length; i++) {
					var rosregister = rValues[i];
					if (-1 != txt_extension.indexOf(rosregister.id + ';')) {
						$('#cpw_form_Rosregister_check').attr('checked', 'checked');
						$('#cpw_form_RosregisterNumber').select2('data', rosregister);
						break;
					}
				}
			}

			var visible = $('#cpw_form_DocumentGivenBy').is(':visible');
			$('#cpw_form_DocumentGivenBy').select2('data', content.DocumentGivenBy);
			if (!visible)
				$('#s2id_cpw_form_DocumentGivenBy').hide();
			$('#cpw_form_DocumentGivenBy_check').attr('checked', content.DocumentGivenBy_from_dict ? 'checked' : null).change();
			$('#cpw_form_DocumentGivenBy_text').val(content.DocumentGivenBy_text);
		}

		var DataSave = function (content) {
			content.Extensions = [];
			if ($('#cpw_form_ETPGPB_check').attr('checked'))
				content.Extensions.push(helper_Extensions.ValuesI.ETPGPB);
			if ($('#cpw_form_Fabricant_check').attr('checked'))
				content.Extensions.push(helper_Extensions.ValuesJ.Fabricant);
			if ($('#cpw_form_FTSRussian_check').attr('checked'))
				content.Extensions.push(helper_Extensions.ValuesI.FTSRussian);
			if ($('#cpw_form_LicenseCSP_check').attr('checked'))
				content.Extensions.push(helper_Extensions.ValuesJ.LicenseCSP);
			if ($('#cpw_form_Rosregister_check').attr('checked')) {
				var rosregister = $('#cpw_form_RosregisterNumber').select2('data');
				if (rosregister && null != rosregister)
					content.Extensions.push(rosregister.id);
			}

			content.Phone = !content.Phone || null == content.Phone ? null : content.Phone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/_/g, '');

			content.DocumentGivenBy = $('#cpw_form_DocumentGivenBy').select2('data');
			content.DocumentGivenBy_from_dict = ('checked' == $('#cpw_form_DocumentGivenBy_check').attr('checked'));
			content.DocumentGivenBy_text = $('#cpw_form_DocumentGivenBy_text').val();
		}

		controller.SetFormContent = function (content) {
			this.model = content;
			if (content.DocumentGivenBy && !content.DocumentGivenBy.row && content.DocumentGivenBy.id) {
				var item = h_dictionary.FindById(h_fms, content.DocumentGivenBy.id)
				if (item)
					content.DocumentGivenBy.row = item.row;
			}
			return null;
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			this.model = base_GetFormContent.call(this);
			DataSave(this.model);
			return this.model;
		};

		var RemoveHadFocus = function () {
			$('#cpw_form_Nationality').parent().removeClass('had_focus');
			$('#cpw_form_DocumentGivenBy_check').parent().parent().removeClass('had_focus');
			$('input').parent().parent().removeClass('had_focus');
		}

		controller.CreateNew = function (id_form_div) {
			this.SafeCreateNewContent();
			if (!this.model.Date)
				this.model.Date = helper_Times.nowDateUTC('r');
			this.Render(id_form_div);
			if (!this.model.DocumentGivenBy_text || $('#cpw_form_DocumentGivenBy_text').val() == '')
				$('#cpw_form_DocumentGivenBy_check').attr('checked', 'checked').change();
			RemoveHadFocus();
		}

		controller.Edit = function (id_form_div) {
			this.Render(id_form_div);
			$('#cpw_form_Rosregister_check').change();
			RemoveHadFocus();
		}

		controller.BuildHtmlViewForProfiledData = function () { return ""; };

		controller.PrepareDocumentGivenByTextToPrint = function () {
			if (!this.model.DocumentGivenBy_from_dict) {
				return this.model.DocumentGivenBy_text;
			}
			else {
				return !this.model.DocumentGivenBy || !this.model.DocumentGivenBy.text ? '' : this.model.DocumentGivenBy.text;
			}
		}

		controller.PrepareDocumentGivenByCodeToPrint = function () {
			if (!this.model.DocumentGivenBy_from_dict || !this.model.DocumentGivenBy) {
				return '';
			}
			else {
				return !this.model.DocumentGivenBy.row || !this.model.DocumentGivenBy.row.CODE ? '' : this.model.DocumentGivenBy.row.CODE;
			}
		}

		controller.PrepareAttachments = function () {
			var AttachmentsLines = [];
			if (this.model.Attachments && 0 < this.model.Attachments.length) {
				var countPassport = 0;
				var countOverDoc = 0;
				for (var i = 0; i < this.model.Attachments.length; i++) {
					if ('passport' == this.model.Attachments[i].Type)
						countPassport++;
					else if ('other' == this.model.Attachments[i].Type)
						countOverDoc++;
				}
				if (1 < countPassport)
					AttachmentsLines.push('Паспорт Пользователя УЦ (страница с ФИО/фото), скан-копия (' + countPassport + 'шт.)');
				else if (0 < countPassport)
					AttachmentsLines.push('Паспорт Пользователя УЦ (страница с ФИО/фото), скан-копия');
				if (1 < countOverDoc)
					AttachmentsLines.push('Иной документ, скан-копия (' + countOverDoc + 'шт.)');
				else if (0 < countOverDoc)
					AttachmentsLines.push('Иной документ, скан-копия');
			}
			return AttachmentsLines;
		}

		controller.BuildXamlView = function () {
			if (this.model.Phone && null != this.model.Phone) {
				var phone = this.model.Phone.replace('+7', '');
				this.model.PhoneCode = phone.substring(0, 3);
				this.model.PhoneNumber = phone.substring(3, this.model.Phone.length);
			}
			var params = {
				form: this.model
				, helper_Times: helper_Times
				, helper_Extensions: helper_Extensions
				, helper_Rosregister: helper_Rosregister
				, DocumentGivenByText: this.PrepareDocumentGivenByTextToPrint()
				, DocumentGivenByCode: this.PrepareDocumentGivenByCodeToPrint()
				, AttachmentsLines: this.PrepareAttachments()
			};
			var content = !this.model.version || 2 != this.model.version ?
				print_tpl(params) :
				print_tpl_v2(params);
			return content;
		}

		controller.Validate = function () {
			if (this.binding.CurrentCollectionIndex >= 0) {
				return null;
			}
			else {
				return this.binding.Validate(this.binding.form_div_selector);
			}
		};

		controller.UseCodec(codec());

		return controller;
	}
});