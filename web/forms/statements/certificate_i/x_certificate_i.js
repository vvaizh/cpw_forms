define([
	  'forms/statements/base/x_statements'
	, 'forms/statements/certificate_i/m_certificate_i'
	, 'forms/statements/base/h_Extensions'
	, 'forms/base/log'
],
function (BaseCodec, model, helper_Extensions, GetLogger) {
	var log = GetLogger('x_certificate_i');
	return function () {
		log.Debug('Create {');
		var res = BaseCodec();
		res.versionXml = 2;

		res.Encode = function (m_certificate) {
			log.Debug('Encode() {');
			var xml_string = '<?xml version="1.0" encoding="utf-8"?>' + this.FormatEOL();
			xml_string += '<certificate_i>' + this.FormatEOL();

			xml_string += this.StringifyField(1, "version", this.versionXml);

			xml_string += this.StringifyField(1, "date", m_certificate.Date);

			xml_string += this.FormatTabs(1) + '<user>' + this.FormatEOL();
			xml_string += this.FormatTabs(2) + '<person>' + this.FormatEOL();
			xml_string += this.StringifyField(3, "lastName", m_certificate.LastName);
			xml_string += this.StringifyField(3, "firstName", m_certificate.FirstName);
			xml_string += this.SafeStringifyField(3, "middleName", m_certificate.MiddleName);
			xml_string += this.FormatTabs(2) + '</person>' + this.FormatEOL();

			xml_string += this.FormatTabs(2) + '<document>' + this.FormatEOL();
			xml_string += this.SafeStringifyField(3, "series", m_certificate.DocumentSeries);
			xml_string += this.StringifyField(3, "number", m_certificate.DocumentNumber);
			if (!m_certificate.DocumentGivenBy_from_dict) {
				xml_string += this.StringifyField(3, "givenBy", m_certificate.DocumentGivenBy_text);
			}
			else if (null != m_certificate.DocumentGivenBy) {
				var ditem = {
				id: m_certificate.DocumentGivenBy.id
					, text: (m_certificate.DocumentGivenBy.row && m_certificate.DocumentGivenBy.row.NAME) ? m_certificate.DocumentGivenBy.row.NAME : m_certificate.DocumentGivenBy.text
				};
				xml_string += this.EncodeNamedDictionaryItem(3, 'givenByOrgan', ditem);
			}
			xml_string += this.SafeStringifyField(3, "issued", m_certificate.DocumentGivenDate);
			xml_string += this.FormatTabs(2) + '</document>' + this.FormatEOL();

			xml_string += this.StringifyField(2, "phone", m_certificate.Phone);
			xml_string += this.FormatTabs(1) + '</user>' + this.FormatEOL();

			xml_string += this.FormatTabs(1) + '<certificate>' + this.FormatEOL();
			xml_string += this.EncodeNamedDictionaryItem(2, "profile", m_certificate.Profile);
			xml_string += this.StringifyField(2, "extensions", m_certificate.Extensions.join(';'));
			xml_string += this.EncodeNamedDictionaryItem(2, "csp", m_certificate.CSP);
			xml_string += this.EncodeNamedDictionaryItem(2, "keyGenerateBy", m_certificate.KeyGenerateBy);
			xml_string += this.FormatTabs(1) + '</certificate>' + this.FormatEOL();

			xml_string += this.StringifyField(1, "currentCert", m_certificate.CurrentCert);

			if (m_certificate.Attachments && null != m_certificate.Attachments) {
				xml_string += this.FormatTabs(1) + '<attachments>' + this.FormatEOL();
				for (var i = 0; i < m_certificate.Attachments.length; i++) {
					var attachment = m_certificate.Attachments[i];
					xml_string += this.FormatTabs(2) + '<attachment>' + this.FormatEOL();
					xml_string += this.SafeStringifyField(3, "type", attachment.Description && attachment.Description.id == '103008' ? 'passport' : 'other');
					xml_string += this.SafeStringifyField(3, "description", (attachment.Description ? attachment.Description.text : 'Иной документ') + ', скан-копия');
					xml_string += this.SafeStringifyField(3, "filename", attachment.FileName);
					xml_string += this.FormatTabs(2) + '</attachment>' + this.FormatEOL();
				}
				xml_string += this.FormatTabs(1) + '</attachments>' + this.FormatEOL();
			}

			xml_string += '</certificate_i>';
			log.Debug('Encode() }');
			return xml_string;
		};

		res.DecodeCertificateInfo = function (node, m_certificate) {
			log.Debug('DecodeCertificateInfo {');
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++) {
				var child = childs[i];
				switch (child.tagName) {
					case "profile": m_certificate.Profile = res.DecodeDictionaryItem(child); break;
					case "extensions":
						var text = this.GetTextValue(child);
						m_certificate.Extensions = (!text || "" == text) ? [] : text.split(';');
						break;
					case "csp": m_certificate.CSP = res.DecodeDictionaryItem(child); break;
					case "keyGenerateBy": m_certificate.KeyGenerateBy = res.DecodeDictionaryItem(child); break;
				}
			}
			log.Debug('DecodeCertificateInfo }');
		}

		res.DecodeXmlDocument = function (doc) {
			var m_certificate = model();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++) {
				var child = childs[i];
				switch (this.GetTagNameWithoutNamespace(child)) {
					case "version": m_certificate.version = this.GetTextValue(child); break;
					case "date": m_certificate.Date = this.GetTextValue(child); break;
					case "user": res.DecodePersonDataDocument(child, m_certificate); break;
					case "certificate": this.DecodeCertificateInfo(child, m_certificate); break;
					case "currentCert": m_certificate.CurrentCert = this.GetTextValue(child); break;
					case "attachments":
						res.DecodeAttachments(child, m_certificate);
						break;
				}
			}
			return m_certificate;
		};

		log.Debug('Create }');
		return res;
	}
});
