﻿define([
	  'forms/attachments/c_attachments'
	, 'forms/statements/certificate_i/c_certificate_i'
	, 'forms/statements/certificate_i/ff_certificate_i'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, GetLogger) {
	return function () {
		var log = GetLogger('c_a_certificate_i');

		var controller = BaseController(BaseFormController, formSpec);

		controller.GetBody = function () {
			var body = 'Направляем документ "Заявление об изготовлении сертификата Пользователя Удостоверяющего центра InfoTrust".';
			return body;
		}
		controller.Sign = function () { };

		var IsNotExistDocumentTypePasport = true;
		controller.GetDocumentTypeValues = function (withDuplicate, onlyRequired) {
			types = [];
			if (IsNotExistDocumentTypePasport)
				types.push({ id: '103008', text: 'Паспорт Пользователя УЦ (страница с ФИО/фото)', series: $('#cpw_form_DocumentSeries').val(), number: $('#cpw_form_DocumentNumber').val() });
			if (!onlyRequired) {
				types.push({ id: '0', text: 'Иной документ' });
			}
			return types;
		}
		controller.GetDefaultDocumentType = function () {
			if (IsNotExistDocumentTypePasport) {
				IsNotExistDocumentTypePasport = false;
				return { id: 103008, text: "Паспорт Пользователя УЦ (страница с ФИО/фото)" };
			}
			else {
				return { id: 0, text: "Иной документ" };
			}
		}

		var ContainsTypeById = function (id, types) {
			for (var i = 0; i < types.length; i++) {
				if (types[i].id == id)
					return true;
			}
			return false;
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			this.model.Attachments = this.CollectAttachments();
			this.model = base_GetFormContent.call(this);
			return this.model;
		};

		var base_UpdateVisibilityDocumentTypes = controller.UpdateVisibilityDocumentTypes;
		controller.UpdateVisibilityDocumentTypes = function (newIdDocumentType, oldIdDocumentType) {
			if ('103008' == newIdDocumentType) {
				IsNotExistDocumentTypePasport = false;
			}
			else if ('103008' == oldIdDocumentType && ('0' == newIdDocumentType || '' == newIdDocumentType)) {
				IsNotExistDocumentTypePasport = true;
			}
		}

		return controller;
	}
});