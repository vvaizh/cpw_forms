﻿define([
	'forms/base/h_constraints'
],
function (h_constraints) {
	return function () {
		var constraints = [
			h_constraints.for_Field('LastName', h_constraints.NotEmpty())
			, h_constraints.for_Field('LastName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('FirstName', h_constraints.NotEmpty())
			, h_constraints.for_Field('FirstName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('MiddleName', h_constraints.recommend_NotEmpty('Не указано отчество гражданина'))
			, h_constraints.for_Field('MiddleName', h_constraints.recommend_Ciryllic())
			, { fields: ['DocumentSeries']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Поле "Серия паспорта" не может быть пустым'; }
			}
			, h_constraints.for_Fields(['DocumentSeries'], h_constraints.recommend_SeriesPassport())
			, { fields: ['DocumentNumber']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Поле "Номер паспорта" не может быть пустым'; }
			}
			, h_constraints.for_Fields(['DocumentNumber'], h_constraints.recommend_NumberPassport())
			, h_constraints.for_Fields(['DocumentGivenDate'], h_constraints.BeforeDateNow())
			, { fields: ['DocumentGivenBy', 'DocumentGivenBy_text', 'DocumentGivenBy_check']
				, check: function (organ, txt, check) {
					return !((check && h_constraints.check_Empty(organ)) || ((!check || '' == check) && h_constraints.check_Empty(txt)));
				}
				, description: function (element, field) {
					return 'Поле "Орган, выдавший документ, удостоверяющего личность" не может быть пустым';
				}
			}
			, h_constraints.for_Field('DocumentGivenDate', h_constraints.Date())
			, h_constraints.for_Field('DocumentGivenDate', h_constraints.recommend_NotEmpty('Не указана дата выдачи документа, удостоверяющего личность'))
			, h_constraints.for_Field('Phone', h_constraints.NotEmpty())
			, h_constraints.for_Field('Phone', h_constraints.checkPhone())
			, { fields: ['Rosregister_check', 'RosregisterNumber']
				, check: function (check, number) {
					if (!check || '' == check) {
						return true;
					}
					else {
						return null != number && '' != number;
					}
				}
				, description: function (element, check, number) {
					return 'Номер Россреестра не может быть пустым';
				}
			}
			, h_constraints.for_Field('CSP', h_constraints.NotEmpty())
			, h_constraints.for_Field('KeyGenerateBy', h_constraints.NotEmpty())
		];
		return constraints;
	}
});