define([
	  'forms/statements/certificate_i/c_a_certificate_i'
	, 'forms/statements/certificate_i/ff_certificate_i'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_certificate_i'
		, Title: 'Заявление об изготовлении сертификата для ИП'
		, FileFormat: FileFormat
	};
	return form_spec;
});
