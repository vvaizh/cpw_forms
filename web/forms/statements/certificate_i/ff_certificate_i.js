﻿define([],
function () {
	var file_format =
	{
		  FilePrefix: 'StatementForCertificate-QI'
		, FileExtension: 'xml'
		, Description: 'Заявление об изготовлении сертификата Пользователя УЦ InfoTrust для ИП'
	};
	return file_format;
});
