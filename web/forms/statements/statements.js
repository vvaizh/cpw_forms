require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/text'
		}
	}
}),

require
(
	[
		  'forms/statements/certificate_i/f_a_certificate_i'
		, 'forms/statements/certificate_j/f_a_certificate_j'
	],
	function () {
		var extension =
		{
			Title: 'Заявления'
			, key: 'statements'
			, forms: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++) {
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension) {
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);