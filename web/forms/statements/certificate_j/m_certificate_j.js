define([],function (TownAddress)
{
	return function ()
	{
		var res =
		{
			FirstName : '',
			LastName : '',
			MiddleName : '',

			DocumentSeries: '',
			DocumentNumber: '',
			DocumentGivenDate: null, // 'ДД.ММ.ГГГГ'
			DocumentGivenBy: null,
			DocumentGivenByText: '',
			DocumentGivenBy_from_dict: true,

			Phone: '',
			PhoneCode: '',
			PhoneNumber: '',

			Profile: { id: '1.2.643.3.34.4.1', text: 'Квалифицированный-ЮЛ' },
			Extensions: [],
			CSP: { id: '1.2.643.3.34.0.1.3, 1.2.643.100.113.1, 1.2.643.100.113.2', text: 'КриптоПро CSP 4.0 КС2' },
			KeyGenerateBy: { id: 'o', text: 'Формируется Пользователем УЦ' },

			Date: null, // 'ДД.ММ.ГГГГ'
			CurrentCert: '',
			JobTitle: '',
			ClientName: ''
		};
		return res;
	};
});