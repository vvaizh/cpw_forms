define([
	  'forms/statements/certificate_j/c_a_certificate_j'
	, 'forms/statements/certificate_j/ff_certificate_j'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_certificate_j'
		, Title: 'Заявление об изготовлении сертификата для ЮЛ'
		, FileFormat: FileFormat
	};
	return form_spec;
});
