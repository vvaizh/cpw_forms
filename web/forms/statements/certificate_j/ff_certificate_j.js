﻿define([],
function () {
	var file_format =
	{
		  FilePrefix: 'StatementForCertificate-QJ'
		, FileExtension: 'xml'
		, Description: 'Заявление об изготовлении сертификата Пользователя УЦ InfoTrust для ЮЛ'
	};
	return file_format;
});
