define
(
	[
		  'forms/collector'
		, 'forms/statements/certificate_i/c_a_certificate_i'
		, 'forms/statements/certificate_j/c_a_certificate_j'
	],
	function (collect) {
		return collect([
		  'a_certificate_i'
		  , 'a_certificate_j'
		], Array.prototype.slice.call(arguments, 1));
	}
);