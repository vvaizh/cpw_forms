﻿define([]
, function () {
	var res = {};

	var valuesI = [
		{ id: '1.2.643.3.34.4.2', text: 'Квалифицированный-ИП' },
		{ id: '1.2.643.3.34.4.3', text: 'Квалифицированный-ФЛ' },
	];

	var valuesJ = [
		{ id: '1.2.643.3.34.4.1', text: 'Квалифицированный-ЮЛ' },
		{ id: '1.2.643.3.34.4.4', text: 'Квалифицированный-ИС' }
	];

	res.GetValuesI = function () {
		return valuesI;
	}

	res.GetValuesJ = function () {
		return valuesJ;
	}

	return res;
});