﻿define([
	  'forms/base/codec.xml'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger) {
	var log = GetLogger('x_statements');
	return function () {
		var res = BaseCodec();
		res.tabs = ['', '  ', '    ', '      ', '        ', '          ', '            ', '              ', '                ', '                  '];

		res.GetTextValue = function (node) {
			return !node.textContent ? node.text : node.textContent;
		}

		res.DecodeDictionaryItem = function (node) {
			try {
				log.Debug('DecodeDictionaryItem {');
				var dictionary_item = {};
				var childs = node.childNodes;
				var childs_len = childs.length;
				for (var i = 0; i < childs_len; i++) {
					var child = childs[i];
					switch (child.tagName) {
						case "element": dictionary_item.id = this.GetTextValue(child); break;
						case "value": dictionary_item.text = this.GetTextValue(child); break;
					}
				}
				return dictionary_item;
			}
			catch (e) {
				log.Error('can not decode dictionary item');
				throw e;
			}
			finally {
				log.Debug('DecodeDictionaryItem }');
			}
		}

		res.DecodePersonDataDocument = function (node, model) {
			log.Debug('DecodePersonDataDocument {');
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++) {
				var child = childs[i];
				switch (child.tagName) {
					case "person": this.DecodePersonDataDocument_Person(child, model); break;
					case "document": this.DecodePersonDataDocument_Document(child, model); break;
					case "phone": model.Phone = this.GetTextValue(child); break;
				}
			}
			log.Debug('DecodePersonDataDocument }');
		}

		res.DecodePersonDataDocument_Document = function (node, model) {
			log.Debug('DecodePersonDataDocument_Document {');
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "series": model.DocumentSeries = self.GetTextValue(child); break;
					case "number": model.DocumentNumber = self.GetTextValue(child); break;
					case "issued": model.DocumentGivenDate = self.GetTextValue(child); break;
					case "validTo": model.DocumentFinishDate = self.GetTextValue(child); break;
					case "givenByOrgan":
						var ditem = self.DecodeDictionaryItem(child);
						model.DocumentGivenBy = { id: ditem.id, text: ditem.text };
						model.DocumentGivenBy_from_dict = true;
						break;
					case 'givenBy':
						model.DocumentGivenBy_text = self.GetTextValue(child);
						model.DocumentGivenBy_from_dict = false;
						break;
				}
			});
			log.Debug('DecodePersonDataDocument_Document }');
		}

		res.DecodePersonDataDocument_Person = function (node, model) {
			log.Debug('DecodePersonDataDocument_Person {');
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "lastName": model.LastName = self.GetTextValue(child); break;
					case "firstName": model.FirstName = self.GetTextValue(child); break;
					case "middleName": model.MiddleName = self.GetTextValue(child); break;
				}
			});
			log.Debug('DecodePersonDataDocument_Person }');
		}

		res.DecodeAttachments = function (node, model) {
			log.Debug('DecodeAttachments {');
			var self = this;
			model.Attachments = [];
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "attachment": model.Attachments.push(self.DecodeAttachment(child)); break;
				}
			});
			log.Debug('DecodeAttachments }');
		}

		res.DecodeAttachment = function (node) {
			log.Debug('DecodeAttachment {');
			var self = this;
			var attachment = {};
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "type": attachment.Type = self.GetTextValue(child); break;
					case "description": attachment.Description = self.GetTextValue(child); break;
					case "filename": attachment.FileName = self.GetTextValue(child); break;
				}
			});
			log.Debug('DecodeAttachment }');
			return attachment;
		}

		res.EncodeDictionaryItem = function (num_tabs, dictionary_item) {
			log.Debug('EncodeDictionaryItem {');
			var xml_string = '';
			xml_string += this.StringifyField(num_tabs, "element", dictionary_item.id);
			xml_string += this.SafeStringifyField(num_tabs, "value", dictionary_item.text);
			log.Debug('EncodeDictionaryItem }');
			return xml_string;
		}

		res.EncodeNamedDictionaryItem = function (num_tabs, name, dictionary_item) {
			log.Debug('EncodeNamedDictionaryItem {');
			try {
				var xml_string = '';
				xml_string += this.FormatTabs(num_tabs) + '<' + name + '>' + this.FormatEOL();
				xml_string += (null == dictionary_item) ? '' : this.EncodeDictionaryItem(num_tabs + 1, dictionary_item);
				xml_string += this.FormatTabs(num_tabs) + '</' + name + '>' + this.FormatEOL();
				return xml_string;
			}
			catch (e) {
				log.Error('can not store dictionary item "' + name + '"');
				throw e;
			}
			finally {
				log.Debug('EncodeNamedDictionaryItem }');
			}
		}

		return res;
	}
});