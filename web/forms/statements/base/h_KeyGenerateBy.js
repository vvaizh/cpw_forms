﻿define([]
, function () {
	var res = {};

	var values = [
		{ id: 'o', text: 'Формируется Пользователем УЦ' },
		{ id: 'c', text: 'Формируется Удостоверяющим центром' }
	];

	res.GetValues = function () {
		return values;
	}

	return res;
});