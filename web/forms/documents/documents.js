﻿require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/text'
		}
	}
}),

require
(
	[
		'forms/documents/notification/f_notification'
		, 'forms/documents/protocol/f_protocol'
	],
	function ()
	{
		var extension =
		{
			Title: 'Документы'
			, key: 'documents'
			, forms: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);