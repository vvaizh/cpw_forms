﻿define
(
	[
		  'forms/collector'
		, 'forms/documents/notification/c_notification'
		, 'forms/documents/protocol/c_protocol'
	],
	function (collect) {
		return collect([
		  'notification'
		, 'protocol'
		], Array.prototype.slice.call(arguments, 1));
	}
);