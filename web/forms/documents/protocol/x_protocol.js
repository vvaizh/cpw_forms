define([
	  'forms/base/codec.json'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger) {
	var log = GetLogger('x_protocol');
	return function () {
		log.Debug('Create {');
		var codec = BaseCodec();

		codec.Encode = function (data) {
			return JSON.stringify(data, null, '\t').replace(/\n/g, '\r\n');
		};

		codec.Decode = function (json_string) {
			log.Debug('Decode {');
			var res = json_string;
			if ('string' == typeof json_string) {
				try {
					res = JSON.parse(json_string);
				}
				catch (ex) {
					log.Error('can not parse json :');
					log.Error(json_string);
					throw ex;
				}
			}
			log.Debug('Decode }');
			return res;
		};

		log.Debug('Create }');
		return codec;
	}
});