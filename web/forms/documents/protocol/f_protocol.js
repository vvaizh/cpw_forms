﻿define(['forms/documents/protocol/c_protocol'],
function (CreateController) {
	var form_spec =
	{
		CreateController: CreateController
		, key: 'protocol'
		, Title: 'Протокол входного контроля'
		, FileFormat: {
			FileExtension: ''
			, Description: 'Протокол входного контроля'
		}
	};
	return form_spec;
});
