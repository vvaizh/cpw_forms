﻿define([
	  'forms/base/controller'
	, 'tpl!forms/documents/protocol/v_protocol.xaml'
	, 'forms/base/log'
	, 'forms/base/codec.tpl.xaml'
],
function (base_controller, tpl, GetLogger, codec_tpl_xaml) {
	return function () {
		var log = GetLogger('c_protocol');
		var controller = base_controller();

		controller.SetFormContent = function (form_content) {
			if ('string' == typeof (form_content)) {
				form_content = JSON.parse(form_content);
			}
			this.content = form_content;
			return null;
		};

		var InitializeHtmlForm = function (id_form_div) {
			var form_div = $(id_form_div);
			form_div.html('<br/><div>Форма имеет только печатное представление</div><br/>');
		}

		controller.CreateNew = function (id_form_div) {
			InitializeHtmlForm(id_form_div);
		}

		controller.Edit = function (id_form_div) {
			InitializeHtmlForm(id_form_div);
		}

		controller.BuildXamlView = function () {
			this.content.Body = !this.content.Body || null == this.content.Body ? '' : this.content.Body.replace(/\r\n/g, '\n').replace(/\n/g, '&#x0a;');
			var content = tpl({ form: this.content });
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		return controller;
	}
});