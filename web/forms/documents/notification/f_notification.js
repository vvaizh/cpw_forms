﻿define(['forms/documents/notification/c_notification'],
function (CreateController) {
	var form_spec =
	{
		CreateController: CreateController
		, key: 'notification'
		, Title: 'Служебный документ'
		, FileFormat: {
			FileExtension: ''
			, Description: 'Служебный документ'
		}
	};
	return form_spec;
});
