require.config
({
  enforceDefine: true,
  urlArgs: "bust" + (new Date()).getTime(),
  baseUrl: '.',
  paths:
  {
    styles: 'css',
    images: 'images'
  },
  map:
  {
    '*':
    {
      tpl:   'js/libs/tpl',
      css:   'js/libs/css',
      image: 'js/libs/image',
      txt: 'js/libs/text'
    }
  }
});

require([
	  'forms/collector'
	, 'forms/base/log'
	, 'forms/test/index'
	, 'forms/documents/index'
	, 'forms/statements/index'
	/*, 'forms/correspondence/index'*/
], function (collect, GetLogger)
{
	var log = GetLogger('forms');
	log.Debug('{');
	aCPW_forms = collect(
	[
		  'test'
		, 'documents'
		, 'statements'
		/*, 'correspondence'*/
	], Array.prototype.slice.call(arguments, 2));
	CPW_forms = aCPW_forms;

	log.Debug('fill select');
	var form_selector = $('#cpw-form-select');
	for (var form_namespace in CPW_forms)
	{
		for (var form_name in CPW_forms[form_namespace])
		{
			var id_form = form_namespace + '.' + form_name;
			form_selector.append($('<option>', {
				value: id_form,
				text: id_form
			}));
		}
	}

	var url = location.href;
	if (-1 != url.indexOf("&clear_profile") || -1 != url.indexOf("?clear_profile"))
	{
		app.profile = {};
	}
	if (-1 != url.indexOf("&use_test_timer") || -1 != url.indexOf("?use_test_timer"))
	{
		app.cpw_Now = function () { return new Date(2015, 6, 26, 17, 36, 0, 0); };
	}
	var prefix = "?createnew=";
	var iprefix = url.indexOf(prefix);
	if (-1 != iprefix)
	{
		log.Debug('process createnew option');
		var id_form = url.substring(iprefix + prefix.length);
		log.Debug('id_form=' + id_form);
		if (id_form.indexOf('#') != -1)
			id_form = id_form.substring(0, id_form.length - 1);
		$("#cpw-form-select [value='" + id_form + "']").attr("selected", "selected");
		var form_spec = GetSelectedFormSpecificationFor_id_Form(id_form);
		if (!form_spec)
		{
			alert('can not find specification for form \"' + id_form + '\"');
		}
		else
		{
			log.Debug('call CreateNewFormWithSpecFunc {');
			CreateNewFormWithSpecFunc(form_spec);
			log.Debug('call CreateNewFormWithSpecFunc }');
		}
	}

	if (typeof (ProcessContentArguments) != "undefined")
		ProcessContentArguments();

	$('#forms_loaded_span').show();
	log.Debug('}');
});
