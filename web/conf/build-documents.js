({
    baseUrl: "..",
    include: ['forms/documents/documents'],
    name: "optimizers/almond",
    out: "..\\built\\documents.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/text'
      }
    }
})