({
    baseUrl: "..",
    include: ['forms/statements/statements'],
    name: "optimizers/almond",
    out: "..\\built\\statements.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/text'
      }
    }
})