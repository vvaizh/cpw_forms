﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Web.Script.Serialization;

using log4net;

using cpw.Forms;

namespace cpw.Forms
{
    public partial class ProfileBrowser : Form
    {
        static ILog log = LogManager.GetLogger(typeof(FormBrowser));

        string path_to_index_html= Path.Combine(PathHelper.WebRootPath,"profile.html");
        string ExtensionsPath= PathHelper.ExtensionsPath;
        string ProfileEditorPath= Path.Combine(PathHelper.WebRootPath,@"js\profile.js");

        const string tplTitle = "КриптоВеб Формы. Профиль по умолчанию";
        const string tplTitleForDocument = "КриптоВеб Формы. Профиль \"{0}\"";

        internal FileDocument m_Document;
        internal StatesMachine States= new StatesMachine();

        static ProfileBrowser m_Instance;
        public static ProfileBrowser Instance
        {
            get
            {
                lock (log)
                {
                    if (null == m_Instance)
                        m_Instance= new ProfileBrowser();
                    return m_Instance;
                }
            }
        }

        public static void SafeCloseInstance()
        {
            lock (log)
            {
                if (null != m_Instance)
                    m_Instance.Close();
            }
        }

        public ProfileBrowser()
        {
            InitializeComponent();
            IconHelper.FixFormIcon(this);

            ExtensionCollector ecollector= new ExtensionCollector();
            JavascriptListener jslistener= new JavascriptListener(browser);
            jslistener.InitStdFunctions();

            States.Start(
                new States.LoadHtml(browser,path_to_index_html,
                    new States.LoadExtensions(browser, ProfileEditorPath, ExtensionsPath, jslistener, ecollector,
                        new States.StartProfile(this)
                    )
                )
            );
            States.Update+=OnStateUpdate;
        }

        void OnStateUpdate(object sender, EventArgs e)
        {
            if (null == m_Document)
            {
                this.Text = tplTitle;
            }
            else
            {
                this.Text = string.Format(tplTitleForDocument, m_Document.FilePath);
            }
        }

        private void ProfileBrowser_FormClosing(object sender, FormClosingEventArgs e)
        {
            lock (log)
            {
                Form parent = Owner;
                if (null != parent)
                    parent.Activate();
                m_Instance= null;
                m_Document= null;
            }
        }
    }
}
