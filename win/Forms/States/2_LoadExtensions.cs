﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class LoadExtensions : State
    {
        WebBrowser browser;
        State state_on_load_extensions;
        HtmlElementEventHandler hdivine_help_link_OnClick;
        string ExtensionsPath;
        string ProfileEditorPath;
        JavascriptListener jsListener;
        ExtensionCollector ecollector;

        internal override bool RealState { get { return false; } }
        internal override string Name { get { return "Загрузка расширений"; } }

        public LoadExtensions(WebBrowser browser, string ExtensionsPath, JavascriptListener listener, ExtensionCollector ecollector, State state_on_load_extensions)
        {
            this.ExtensionsPath= ExtensionsPath;
            this.browser= browser;
            this.state_on_load_extensions= state_on_load_extensions;
            this.jsListener= listener;
            this.ecollector= ecollector;
            if (null != listener)
                hdivine_help_link_OnClick= new HtmlElementEventHandler(listener.OnLinkClick);
        }

        public LoadExtensions(WebBrowser browser, string ProfileEditorPath, string ExtensionsPath, 
            JavascriptListener listener, ExtensionCollector ecollector, State state_on_load_extensions)
            : this(browser,ExtensionsPath,listener,ecollector,state_on_load_extensions)
        {
            this.ProfileEditorPath= ProfileEditorPath;
        }

        internal override void OnStart()
        {
            HtmlDocument document= browser.Document;
            HtmlElement divine_help_link= document.GetElementById("divine_help");
            if (null != divine_help_link)
            {
                divine_help_link.Click -= hdivine_help_link_OnClick;
                divine_help_link.Click += hdivine_help_link_OnClick;
            }
            JacascriptLoader.Load(browser,ExtensionsPath,jsListener,ecollector,OnExtensionLoaded);
        }

        void OnExtensionLoaded()
        {
            if (string.IsNullOrEmpty(ProfileEditorPath))
            {
                Start(state_on_load_extensions);
            }
            else
            {
                JacascriptLoader.LoadProfileEditor(browser,ProfileEditorPath,jsListener,OnProfileEditorLoaded);
            }
        }

        void OnProfileEditorLoaded()
        {
            Start(state_on_load_extensions);
        }
    }
}
