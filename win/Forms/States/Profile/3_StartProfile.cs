﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class StartProfile : State
    {
        WebBrowser browser;
        ProfileBrowser mainfrm;

        public StartProfile(ProfileBrowser mainfrm)
        {
            this.mainfrm= mainfrm;
            this.browser= mainfrm.browser;
        }

        internal override void OnStart()
        {
            Progress.HideP();
            IProfile profile= cpw.Forms.Profile.CreateFromPath(PathHelper.DefaultProfilePath);
            mainfrm.m_Document= new FileDocument();
            mainfrm.m_Document.FilePath= PathHelper.DefaultProfilePath;
            mainfrm.m_Document.FileContent=
            mainfrm.m_Document.LocalContent= profile.ToJsonStringForFormController();
            HtmlDocument html_doc= browser.Document;
            html_doc.InvokeScript("cpw_from_desktop_EditProfile", new object[] { mainfrm.m_Document.FileContent });
            on(mainfrm.miSave,OnSave);
        }

        void OnSave()
        {
            HtmlDocument html_doc= browser.Document;
            object oprofile= html_doc.InvokeScript("cpw_from_desktop_GetEditedProfile");
            if (oprofile.ToString() != string.Empty)
            {
                mainfrm.m_Document.LocalContent = oprofile.ToString();
                cpw.Forms.Profile.Save(PathHelper.DefaultProfilePath, mainfrm.m_Document.FileContent, mainfrm.m_Document.LocalContent);
            }
        }

    }
}
