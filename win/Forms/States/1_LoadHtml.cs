﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class LoadHtml : State
    {
        string path_to_index_html;
        State state_on_load_html;
        WebBrowser browser;

        internal override bool RealState { get { return false; } }
        internal override string Name { get { return "Загрузка html"; } }

        internal LoadHtml(WebBrowser browser, string path_to_index_html, State state_on_load_html)
        {
            this.browser= browser;
            this.path_to_index_html= path_to_index_html;
            this.state_on_load_html= state_on_load_html;
        }

        internal override void OnStart()
        {
            Progress.Start(browser, "Загрузка компонентов", "Начинаем загрузку дополнительных компонентов");
            browser.ScriptErrorsSuppressed= false;
            string pathExecutable = Path.GetDirectoryName(Application.ExecutablePath);
            string pathIndexHtml  = Path.Combine(pathExecutable, path_to_index_html);
            Progress.ShowMessage(string.Format("Загружаем html страницу \"{0}\" из папки\r\n\"{1}\"",
                Path.GetFileName(pathIndexHtml),Path.GetDirectoryName(pathIndexHtml)));
            browser.Navigate(new Uri(pathIndexHtml, System.UriKind.Absolute));

            if (null==hbrowser_DocumentCompleted)
                hbrowser_DocumentCompleted= new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);
            browser.DocumentCompleted -= hbrowser_DocumentCompleted;
            browser.DocumentCompleted += hbrowser_DocumentCompleted;
        }

        WebBrowserDocumentCompletedEventHandler hbrowser_DocumentCompleted;

        void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                Progress.ShowMessage("html страница загружена ");
                browser.DocumentCompleted -= hbrowser_DocumentCompleted;
                Start(state_on_load_html);
            }
            catch (Exception ex)
            {
                Progress.ShowError(browser,"Произошла ошибка при дальнейших действиях с загруженной страницей",ex);
                throw ex;
            }
        }
    }
}
