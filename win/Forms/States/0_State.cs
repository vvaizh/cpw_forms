﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;

namespace cpw.Forms
{
    internal class StatesMachine
    {
        internal State Current;

        internal StatesMachine()
        {
        }

        internal void Start(State state)
        {
            state.states= this;
            state.InternalStart();
        }

        internal void StopOnOkOnReject(Action on_ok_to_close, Action on_reject_closing)
        {
            if (null == Current)
            {
                on_ok_to_close();
            }
            else
            {
                State lCurrent = Current;
                Current.Stop(() =>
                {
                    if (Current == lCurrent)
                    {
                        on_reject_closing();
                    }
                    else
                    {
                        StopOnOkOnReject(on_ok_to_close, on_reject_closing);
                    }
                });
            }
        }

        internal event EventHandler Update;
        internal void DoUpdate()
        {
            if (null!=Update)
                Update(null,null);
        }
    }

    internal class State
    {
        internal State          Parent {get;private set;}

        internal StatesMachine  states;

        internal virtual bool   RealState   { get { return true; } }
        internal virtual string Name        { get { return GetType().FullName; } }

        internal virtual void   OnStart     ()              {Update();}
        internal virtual void   OnStop      (Action action) {action();}
        internal virtual void   Update      ()              {BaseStateUpdate();}

        protected void BaseStateUpdate()
        {
            if (null != MenuItemActions)
            {
                foreach (MenuItemAction miaction in MenuItemActions.Values)
                {
                    if (!string.IsNullOrEmpty(miaction.Title))
                        miaction.mitem.Text= miaction.Title;
                    miaction.mitem.Visible= true;
                    miaction.mitem.Enabled= true;
                }
                for (State p = Parent; null != p; p = p.Parent)
                {
                    Dictionary<ToolStripMenuItem,MenuItemAction> items= p.MenuItemActions;
                    if (null != items)
                    {
                        foreach (MenuItemAction miaction in items.Values)
                            miaction.mitem.Enabled = true;
                    }
                }
            }
        }

        internal void InternalStart()
        {
#if DEBUG
            if (null!= Parent && states.Current==this)
                throw new Exception("Похоже запутались состояния, начинаем второй раз..");
#endif
            Parent= states.Current;
            states.Current= this;
            OnStart();
            DoUpdate();
        }

        internal void Start(State state)
        {
            states.Start(state);
        }

        internal void Stop(Action action)
        {
            off();
            OnStop(() => { 
                states.Current= Parent;
                DoUpdate();
                action();
            });
        }

        internal void BackToMe(Action action)
        {
            if (this == states.Current)
            {
                action();
            }
            else
            {
                states.Current.Stop(() => { BackToMe(action); });
            }
        }

        internal void DoUpdate()
        {
            states.DoUpdate();
            if (null != states.Current)
                states.Current.Update();
        }

        internal class MenuItemAction
        {
            internal ToolStripMenuItem   mitem;
            internal Action              action;
            internal string              Title;
            EventHandler                 haction;

            internal void on()
            {
                if (null==haction)
                    haction= new EventHandler(mitem_Click);
                mitem.Click+= haction;
            }

            internal void off()
            {
                if (null!=haction)
                    mitem.Click-= haction;
            }

            void  mitem_Click(object sender, EventArgs e)
            {
                action();
            }
        }

        Dictionary<ToolStripMenuItem,MenuItemAction> MenuItemActions;

        protected void on(ToolStripMenuItem mi, Action a)
        {
            on(mi,a,null);
        }

        protected void on_if_current(ToolStripMenuItem mi, Action a, string title)
        {
            on(mi, () => { if (this==states.Current) a(); }, title);
        }

        protected void on(ToolStripMenuItem mi, Action a, string title)
        {
            if (null == MenuItemActions)
                MenuItemActions= new Dictionary<ToolStripMenuItem,MenuItemAction>();

            MenuItemAction miaction;
            if (!MenuItemActions.TryGetValue(mi, out miaction))
            {
                miaction = new MenuItemAction { mitem = mi, action = a, Title= title };
                MenuItemActions.Add(mi,miaction);
            }
            else
            {
                miaction.off();
                miaction.action = a;
            }
            miaction.on();
        }

        protected void off()
        {
            if (null != MenuItemActions)
            {
                foreach (MenuItemAction miaction in MenuItemActions.Values)
                    miaction.off();
            }
        }
    }

    namespace States
    {
        internal class Empty : State
        {
        }
    }
}
