﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class FormsStartPage : BaseFormsState
    {
        State state_form;

        internal override string Name { get { return "Главная страница"; } }

        public FormsStartPage(FormBrowser mainform)
            : base(mainform)
        {
            state_form= new States.Form(mainform);
        }

        internal override void OnStart()
        {
            Progress.HideP();
            on(mainfrm.miOpen,OnOpen);
            on(mainfrm.smiOpenDocument,OnOpen);
            on(mainfrm.miCreate,OnCreate);
            on(mainfrm.smiCreateDocument,OnCreate);
            jsListener.AddFunc(CreateNewForm);
        }

        void CreateNewForm(HtmlDocument html_document, string id_form)
        {
            Specs specs= mainfrm.ecollector.Specs;
            Spec spec= specs.SafeGetByKey(id_form);
            Create(spec);
        }

        Spec SelectSpec()
        {
            using (SelectorDocumentForms frm = new SelectorDocumentForms())
            {
                Specs specs= mainfrm.ecollector.Specs;
                frm.LoadDocumentForms(specs);
                return (System.Windows.Forms.DialogResult.OK != frm.ShowDialog(mainfrm))
                    ? null : frm.SelectedFrom;
            }
        }

        void Create(Spec spec)
        {
            if (null != spec)
            {
                mainfrm.m_Document= new Document();
                mainfrm.m_Document.Spec= spec;
                states.Start(state_form);
            }
        }

        void OnCreate()
        {
            BackToMe(() => {
                Create(SelectSpec());
            });
        }

        void OnOpen()
        {
            Progress.Start(mainfrm,"Открытие формы","Вызвана команда на открытие формы");
            Progress.ShowMessage("Откатываемся в состояние стартовой страницы..");
            BackToMe(() => {
                Progress.ShowMessage("Откатыились в состояние стартовой страницы.");
                Specs specs= mainfrm.ecollector.Specs;
                Spec spec;
                Progress.ShowMessage("Запрашиваем файл который необходимо открыть");
                string FilePath= FileDialogHelper.ChooseFileToLoad(mainfrm.browser,specs,out spec);
                if (null == FilePath)
                {
                    Progress.ShowMessage("Файл не выбран.");
                    Progress.HideP();
                }
                else
                {
                    Progress.ShowMessage(string.Format("Выбран файл\r\n\"{0}\"",FilePath));
                    if (null == spec)
                    {
                        if (DraftHelper.IsDraftFilePath(FilePath))
                        {
                            Progress.ShowMessage("Загружаем черновик...");
                            mainfrm.m_Document= Document.LoadDraft(FilePath, specs);
                            Progress.ShowMessage("Открываем форму..");
                            Start(state_form);
                        }
                        else
                        {
                            Progress.ShowMessage("Загружаем форму неизвестного формата...");
                            string FileContent= File.ReadAllText(FilePath);
                            Progress.ShowMessage("Определяем формат формы...");
                            DialogResult res= ChooseFormSpecForFilePath(FileContent,FilePath, out spec);
                            if (null == spec)
                            {
                                if (DialogResult.No!=res)
                                    Progress.HideP();
                            }
                            else
                            {
                                Progress.ShowMessage("Открываем форму..");
                                mainfrm.m_Document = new Document(FilePath,spec,FileContent);
                                Start(state_form);
                            }
                        }
                    }
                }
            });
        }

        DialogResult CheckFormSpecForFilePath(Spec spec, string FilePath, string frm_doc_FilePath_Extension, string FileContent)
        {
            JsonSpec.Form form= spec.form;
            Progress.ShowMessage(string.Format("Проверяем форму \"{0}\"",form.FileFormat.Description));
            Progress.ShowMessage("{");
            Progress.ShowMessage(string.Format("  должно быть расширение \"{0}\"!",form.FileFormat.FileExtension));
            if (frm_doc_FilePath_Extension == form.FileFormat.FileExtension.ToLower())
            {
                Progress.ShowMessage("  расширение подходит, пытаемся загрузить форму..");
                object ores = html_doc.InvokeScript("cpw_from_desktop_TryToLoadFormContent",new object []{spec.key,FileContent});
                if (null != ores && System.DBNull.Value != ores)
                {
                    Progress.ShowMessage("    При попытке загрузить, получено сообщение об ошибке:");
                    Progress.ShowMessage(string.Format("    {{\r\n------------------\r\n{0}\r\n------------------",ores.ToString()));
                    Progress.ShowMessage("    }");
                }
                else
                {
                    Progress.ShowMessage("      Загрузка формы завершилась успешно..");
                    Progress.ShowMessage("        Спрашиваем согласие пользователя");

                    string msg = string.Format("Открыть файл\r\n\r\n\"{1}\"\r\n\r\nкак\r\n                 \"{0}\"  ?",
                        form.FileFormat.Description,FilePath);

                    DialogResult answer= MessageBox.Show
                        (mainfrm,msg,"Открытие файла неизвестной структуры",MessageBoxButtons.YesNoCancel);
                    string progress_msg;
                    switch (answer)
                    {
                        case DialogResult.Cancel:   progress_msg= "          пользователь ответил \"отмена\"."; break;
                        case DialogResult.No:       progress_msg= "          пользователь ответил \"нет\"."; break;
                        case DialogResult.Yes:      progress_msg= "          пользователь ответил \"да\"."; break;
                        default:                    progress_msg= "          пользователь ответил что то неожиданное."; break;
                    }
                    Progress.ShowMessage(progress_msg);
                    return answer;
                }
            }
            return DialogResult.No;
        }

        DialogResult ChooseFormSpecForFilePath(string FileContent, string FilePath, out Spec out_spec)
        {
            HtmlDocument html_doc= mainfrm.browser.Document;
            string frm_doc_FilePath_Extension= Path.GetExtension(FilePath).TrimStart('.').ToLower();

            Progress.ShowMessage(string.Format("Определяем форму файла c расширением \"{0}\":",frm_doc_FilePath_Extension));
            Progress.ShowMessage(FilePath);
            foreach (Spec spec in mainfrm.ecollector.Specs)
            {
                DialogResult res= CheckFormSpecForFilePath(spec,FilePath,frm_doc_FilePath_Extension,FileContent);
                if (DialogResult.No != res)
                {
                    out_spec= (DialogResult.Cancel == res) ? null : spec;
                    return res;
                }
            }
            Progress.ShowMessage("}");
            Progress.ShowError(mainfrm,"Не удалось подобрать подходящий формат формы для открытия!",null);
            out_spec= null;
            return DialogResult.No;
        }
    }
}
