﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Reflection;

namespace cpw.Forms.States
{
    internal class Form : BaseFormsState
    {
        Document frm_document { get { return mainfrm.m_Document; } }
        State state_on_edit;
        State state_on_profiled;

        internal override string Name { get { return "Форма"; } }

        public Form(FormBrowser mainform)
            : base(mainform)
        {
            state_on_edit= new States.Edit(this);
            state_on_profiled= new States.Profiled(mainfrm);
        }

        internal override void Update()
        {
            Progress.Start(mainfrm,"Открытие печатного представления","Начинаем открытие печатного представления");
            BaseStateUpdate();
            this.mainfrm.browser.Visible = false;
            this.mainfrm.xaml_browser.Visible = true;
            LoadXamlView();
        }

        internal override void OnStart()
        {
            on(mainfrm.miEdit,OnEdit);
            on_if_current(mainfrm.miProfiled,OnProfiled, "Корешок");
            on(mainfrm.smiSaveDocumentAs,OnSaveAs);

            if (frm_document.isDraft)
            {
                Progress.ShowMessage("Загружаем в браузер содержимое черновика..");
                html_doc.InvokeScript("cpw_from_desktop_SetActiveFormDraftContent",
                    new object[] { frm_document.Spec.key, frm_document.FileContent });
                Progress.ShowMessage("Запускаем редактор..");
                OnEdit();
            }
            else if (frm_document.isDraft || frm_document.isNew)
            {
                Progress.ShowMessage("Загружаем профиль..");
                IProfile profile = cpw.Forms.Profile.CreateFromPath(PathHelper.DefaultProfilePath);
                string sprofile= profile.ToJsonStringForFormController();
                Progress.ShowMessage("Заводим новую чистую форму по профилю..");
                html_doc.InvokeScript("cpw_from_desktop_SetActiveFormSpec", new object[] { frm_document.Spec.key, sprofile });
                Progress.ShowMessage("Запускаем редактор..");
                Edit();
            }
            else
            {
                Progress.ShowMessage("Загружаем в браузер содержимое формы..");
                object ores= html_doc.InvokeScript("cpw_from_desktop_SetActiveFormContent",
                    new object[] { frm_document.Spec.key, frm_document.FileContent });
            }
        }

        void LoadXamlView()
        {
            Progress.ShowMessage("Получаем печатный вид формы..");
            object oxaml = html_doc.InvokeScript("cpw_from_desktop_GetActiveFormXamlView");
            string xaml_text = oxaml as string;
            if (null == xaml_text)
            {
                Type t= oxaml.GetType();
                object oexception_text= t.InvokeMember("exception_text", BindingFlags.GetProperty, null, oxaml, null);
                string exception_text= oexception_text as string;
                Progress.ShowError(mainfrm,exception_text,null);
            }
            else
            {
                Progress.ShowMessage("Сохраняем полученный печатный вид..");
                string xaml_tmp_path = PathHelper.TmpXamlPath;
                File.WriteAllText(PathHelper.TmpXamlPath, xaml_text);

                Progress.ShowMessage("Запускаем отображение печатного вида..");
                if (null == hxml_browser_DocumentCompleted)
                    hxml_browser_DocumentCompleted = new WebBrowserDocumentCompletedEventHandler(xml_browser_DocumentCompleted);
                WebBrowser xaml_browser = mainfrm.xaml_browser;
                xaml_browser.DocumentCompleted -= hxml_browser_DocumentCompleted;
                xaml_browser.DocumentCompleted += hxml_browser_DocumentCompleted;
                xaml_browser.Navigate(new Uri(xaml_tmp_path, System.UriKind.Absolute));
            }
        }

        WebBrowserDocumentCompletedEventHandler hxml_browser_DocumentCompleted;
        void xml_browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            File.Delete(PathHelper.TmpXamlPath);
            Progress.ShowMessage("Отобразили печатный вид.");
            Progress.HideP();
        }

        internal override void OnStop(Action action)
        {
            mainfrm.m_Document= null;
            jsListener.AddAutoRemoveFunc("OnCurrentFormStopped",action);
            html_doc.InvokeScript("cpw_from_desktop_StopActiveForm");
        }

        void Edit()
        {
            Start(state_on_edit);
        }

        internal void OnEdit()
        {
            Progress.Start(mainfrm,"Запуск редактора формы","Вызвана команда на редактирование формы");
            Edit();
        }

        internal void OnSaveAs()
        {
            frm_document.isFileToSave= false;
            BackToMe(() => {});
        }

        internal void OnProfiled()
        {
            states.Start(state_on_profiled);
        }
    }
}
