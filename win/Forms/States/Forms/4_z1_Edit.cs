﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class Edit : BaseFormsState
    {
        Document frm_document { get { return mainfrm.m_Document; } }
        States.Form state_form;
        State state_on_profiled;
        bool doNotSave= false;

        internal override string Name { get { return "Редактирование"; } }

        public Edit(States.Form state_form)
            : base(state_form.mainfrm)
        {
            this.state_form= state_form;
            state_on_profiled= new States.EditProfiled(mainfrm);
        }

        internal override void OnStart()
        {
            on(mainfrm.miProfiled,OnProfiled,"Корешок");
            on(mainfrm.miSave,OnSave);
            if (frm_document.isDraft)
                on(mainfrm.miSaveAs,OnSaveAs);
            on_if_current(mainfrm.miBack,OnCancel,"Вернуться без сохранения");

            doNotSave= false;

            Progress.ShowMessage(frm_document.isNew ? "Открываем редактор новой формы.." : "Открываем редактор загруженной формы..");
            string on_ok_func_name= "OnOkEditForm";
            jsListener.AddAutoRemoveFunc(on_ok_func_name, () => {
                Progress.ShowMessage("Редактор успешно открыт");
                Progress.HideP();
            });
            html_doc.InvokeScript(frm_document.isNew ? "cpw_from_desktop_CreateNewActiveForm" : "cpw_from_desktop_EditActiveForm",
                new object [] { on_ok_func_name });
        }

        internal override void OnStop(Action actionOnStop)
        {
            DialogResult res= doNotSave ? DialogResult.No : 
                MessageBox.Show(mainfrm,"Сохранить форму?",
                                "Выход из режима редактирования формы",
                                MessageBoxButtons.YesNoCancel);
            if (DialogResult.Cancel != res)
            {
                if (DialogResult.Yes == res)
                    Save();
                actionOnStop();
            }
        }

        internal void OnSave()
        {
            Save();
        }

        internal void OnSaveAs()
        {
            SaveAs();
        }

        internal void OnProfiled()
        {
            Start(state_on_profiled);
        }

        internal void OnCancel()
        {
            Cancel();
        }

        /* Helper methods: { */
        void Cancel()
        {
            DialogResult res= MessageBox.Show(mainfrm,
                "Вы действительно хотите отказаться от сделанных в процессе изменений,\r\n и не сохранять их?",
                "Завершение редактирования",MessageBoxButtons.YesNoCancel);
            switch (res)
            {
                case DialogResult.No:
                    OnSave();
                    break;
                case DialogResult.Yes:
                    doNotSave=true;
                    Stop(()=>{});
                    break;
            }
        }

        void SaveDraft(string FilePath)
        {
            if (null != FilePath)
            {
                object odraft = html_doc.InvokeScript("cpw_from_desktop_GetActiveFormDraftContent");
                frm_document.SaveDraftContentFile(odraft.ToString(), FilePath);
                DoUpdate();
            }
        }

        void SafeSaveValid(string FilePath)
        {
            object ocontent = html_doc.InvokeScript("cpw_from_desktop_GetActiveFormContent");
            if (null != ocontent && System.DBNull.Value != ocontent)
            {
                frm_document.SaveValidContentFile(ocontent.ToString(), FilePath);
                DoUpdate();
            }
            else
            {
                string DraftFilePath= FileDialogHelper.ChooseFileToStoreDraft(mainfrm, mainfrm.m_Document);
                SaveDraft(DraftFilePath);
            }
        }

        void SaveAs()
        {
            string FilePath = FileDialogHelper.ChooseFileToStore(mainfrm, mainfrm.m_Document);
            if (null != FilePath)
            {
                if (DraftHelper.IsDraftFilePath(FilePath))
                {
                    SaveDraft(FilePath);
                }
                else
                {
                    SafeSaveValid(FilePath);
                }
            }
        }

        void Save()
        {
            if (frm_document.NeedChooseFileToSave)
            {
                SaveAs();
            }
            else if (frm_document.isDraft)
            {
                SaveDraft(frm_document.FilePath);
            }
            else
            {
                SafeSaveValid(frm_document.FilePath);
            }
        }
        /* Helper methods  } */
    }
}
