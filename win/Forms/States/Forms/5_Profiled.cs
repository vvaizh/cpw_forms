﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class Profiled : BaseFormsState
    {
        internal override string Name { get { return "Корешок формы"; } }

        public Profiled(FormBrowser mainform)
            : base(mainform)
        {
        }

        internal override void OnStart()
        {
            on(mainfrm.miBack,OnBack,"Вернуться к просмотру формы");
            html_doc.InvokeScript("cpw_from_desktop_ViewActiveFormProfiledData");
        }

        void OnBack()
        {
            html_doc.InvokeScript("cpw_from_desktop_StopViewActiveFormProfiledData");
            Stop(()=>{});
        }
    }
}
