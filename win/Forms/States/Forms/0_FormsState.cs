﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class BaseFormsState : State
    {
        internal FormBrowser mainfrm;

        
        protected JavascriptListener jsListener { get { return mainfrm.jslistener; } }
        protected WebBrowser browser  { get { return mainfrm.browser; } }
        protected HtmlDocument html_doc { get { return browser.Document; } }

        internal override void Update()
        {
            base.Update();
            this.mainfrm.browser.Visible= true;
            this.mainfrm.xaml_browser.Visible= false;
        }

        public BaseFormsState(FormBrowser mainform)
        {
            this.mainfrm = mainform;
        }
    }
}
