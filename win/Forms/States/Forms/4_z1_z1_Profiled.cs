﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace cpw.Forms.States
{
    internal class EditProfiled : BaseFormsState
    {
        internal override string Name { get { return "Предпросмотр корешка"; } }

        public EditProfiled(FormBrowser mainform)
            : base(mainform)
        {
        }

        internal override void OnStart()
        {
            on(mainfrm.miBack,OnBack,"Вернуться к редактированию");
            on(mainfrm.miUpdateByProfile,OnUpdateByProfile);
            html_doc.InvokeScript("cpw_from_desktop_ViewActiveFormProfiledData");
        }

        void OnBack()
        {
            html_doc.InvokeScript("cpw_from_desktop_StopViewActiveFormProfiledData");
            Stop(()=>{});
        }

        void OnUpdateByProfile()
        {
            Progress.Start(mainfrm,"Обновление формы по профилю","Начинаем обновление");
            Progress.ShowMessage("Читаем профиль из папки\r\n" + PathHelper.DefaultProfilePath);
            IProfile profile= cpw.Forms.Profile.CreateFromPath(PathHelper.DefaultProfilePath);
            Progress.ShowMessage("Передаём прочитанный профиль для обновления в форму");
            html_doc.InvokeScript("cpw_from_desktop_ActiveFormUseProfile",new object [] { profile.ToJsonStringForFormController() });
            Progress.ShowMessage("Обновление завершилось успешно");
            Progress.HideP();
        }
    }
}
