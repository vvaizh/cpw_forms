﻿namespace cpw.Forms
{
    partial class SelectorDocumentForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.lblDocumentForm = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lbDocumentForms = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(128, 235);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(94, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Создать";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblDocumentForm
            // 
            this.lblDocumentForm.AutoSize = true;
            this.lblDocumentForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDocumentForm.Location = new System.Drawing.Point(12, 9);
            this.lblDocumentForm.Name = "lblDocumentForm";
            this.lblDocumentForm.Size = new System.Drawing.Size(309, 16);
            this.lblDocumentForm.TabIndex = 2;
            this.lblDocumentForm.Text = "Выберите, какую форму вы хотите заполнить:";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(228, 234);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lbDocumentForms
            // 
            this.lbDocumentForms.BackColor = System.Drawing.SystemColors.Control;
            this.lbDocumentForms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbDocumentForms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDocumentForms.FormattingEnabled = true;
            this.lbDocumentForms.ItemHeight = 15;
            this.lbDocumentForms.Location = new System.Drawing.Point(32, 41);
            this.lbDocumentForms.Name = "lbDocumentForms";
            this.lbDocumentForms.Size = new System.Drawing.Size(289, 167);
            this.lbDocumentForms.TabIndex = 4;
            this.lbDocumentForms.SelectedIndexChanged += new System.EventHandler(this.cbDocunetForms_SelectedIndexChanged);
            this.lbDocumentForms.DoubleClick += new System.EventHandler(this.lbDocumentForms_DoubleClick);
            // 
            // SelectorDocumentForms
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(334, 269);
            this.Controls.Add(this.lbDocumentForms);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblDocumentForm);
            this.Controls.Add(this.btnOK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectorDocumentForms";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выбор формы для заполнения";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblDocumentForm;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lbDocumentForms;
    }
}