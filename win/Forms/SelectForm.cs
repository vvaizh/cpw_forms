﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace cpw.Forms
{
    public partial class SelectorDocumentForms : Form
    {
        public SelectorDocumentForms()
        {
            InitializeComponent();
            IconHelper.FixFormIcon(this);
            btnOK.Enabled = lbDocumentForms.SelectedItem != null;
        }

        public void LoadDocumentForms(Specs forms)
        {
            if (null != forms)
            {
                foreach (Spec form in forms)
                {
                    lbDocumentForms.Items.Add(form);
                }
            }
        }

        public Spec SelectedFrom
        {
            get { return lbDocumentForms.SelectedItem as Spec; }
        }

        private void cbDocunetForms_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = lbDocumentForms.SelectedItem != null;
        }

        private void lbDocumentForms_DoubleClick(object sender, EventArgs e)
        {
            if (lbDocumentForms.SelectedItem != null)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }
    }
}
