﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Packaging;
using System.Text;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace cpw.Forms
{
    public static class DraftHelper
    {
        const string meta_part_name= "meta.json.txt";
        const string content_part_name= "content.json.txt";

        public const string FileExtension= "dft";
        public const string DotedFileExtension= ".dft";
        public const string FileDialogFilterString= "Черновик формы (*.dft)|*.dft";

        public static bool IsDraftFilePath(string FilePath)
        {
            string FilePath_DotedExtension= Path.GetExtension(FilePath).ToLower();
            return DotedFileExtension==FilePath_DotedExtension;
        }

        public static void Save(string Filepath, string DraftContent, Spec spec)
        {
            using (Package package = Package.Open(Filepath, FileMode.Create))
            {
                Uri uriContent= PackUriHelper.CreatePartUri(new Uri(content_part_name, UriKind.Relative));
                PackagePart packagePartContent= package.CreatePart(uriContent, "text/json", CompressionOption.Maximum);
                using (Stream stream= packagePartContent.GetStream())
                using (TextWriter writer= new StreamWriter(stream))
                {
                    writer.Write(DraftContent);
                }

                Uri uriMeta= PackUriHelper.CreatePartUri(new Uri(meta_part_name, UriKind.Relative));
                PackagePart packagePartMeta= package.CreatePart(uriMeta, "text/json", CompressionOption.Maximum);
                using (Stream stream= packagePartMeta.GetStream())
                using (TextWriter writer= new StreamWriter(stream))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    writer.Write(serializer.Serialize(spec));
                }
            }
        }

        public static void Load(string FilePath, out string DraftContent, out Spec spec)
        {
            using (Package package = Package.Open(FilePath, FileMode.Open))
            {
                Uri uriContent= PackUriHelper.CreatePartUri(new Uri(content_part_name, UriKind.Relative));
                PackagePart packagePartContent= package.GetPart(uriContent);
                using (Stream stream= packagePartContent.GetStream())
                using (TextReader reader= new StreamReader(stream))
                {
                    DraftContent= reader.ReadToEnd();
                }

                Uri uriMeta= PackUriHelper.CreatePartUri(new Uri(meta_part_name, UriKind.Relative));
                PackagePart packagePartMeta= package.GetPart(uriMeta);
                using (Stream stream= packagePartMeta.GetStream())
                using (TextReader reader= new StreamReader(stream))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string spec_txt= reader.ReadToEnd();
                    spec= serializer.Deserialize<Spec>(spec_txt);
                }
            }
        }
    }
}
