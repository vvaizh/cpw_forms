﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Packaging;
using System.Text;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace cpw.Forms
{
    public class FileDocument
    {
        public string FilePath;

        public string FileContent;
        public string LocalContent;
    }

    public class Document : FileDocument
    {
        public Spec   Spec;

        public bool isFileToSave= true;
        public bool isDraft= false;

        public bool isNew { get { return string.IsNullOrEmpty(FilePath); } }
        public bool NeedChooseFileToSave { get { return isNew || !isFileToSave; } }

        public Document() { }
        public Document(string aFilePath, Spec aSpec, string aContent)
        {
            Spec= aSpec;
            SetValidContentPathFields(aContent,aFilePath);
        }

        public static Document LoadDraft(string aFilePath, Specs specs)
        {
            Document doc= new Document();
            doc.InternalLoadDraft(aFilePath, specs);
            return doc;
        }

        public static Document LoadValid(string aFilePath)
        {
            Document doc= new Document();
            doc.InternalLoadValid(aFilePath);
            return doc;
        }

        void SetContentPathFields(string aContent, string aFilePath)
        {
            FilePath= aFilePath;
            FileContent= aContent;
            LocalContent= aContent;
            isFileToSave= true;
        }

        void SetDraftContentPathFields(string aContent, string aFilePath)
        {
            isDraft= true;
            SetContentPathFields(aContent,aFilePath);
        }

        void SetValidContentPathFields(string aContent, string aFilePath)
        {
            isDraft= false;
            SetContentPathFields(aContent,aFilePath);
        }

        public void SaveDraftContentFile(string aContent, string aFilePath)
        {
            DraftHelper.Save(aFilePath,aContent,Spec);
            SetDraftContentPathFields(aContent,aFilePath);
        }

        public void SaveValidContentFile(string aContent, string aFilePath)
        {
            File.WriteAllText(aFilePath,aContent);
            SetValidContentPathFields(aContent,aFilePath);
        }

        void InternalLoadDraft(string aFilePath, Specs specs)
        {
            string DraftContent;
            Spec spec;
            DraftHelper.Load(aFilePath,out DraftContent,out spec);
            Spec= specs.SafeGetByKey(spec.key);
            if (null != Spec)
                SetDraftContentPathFields(DraftContent,aFilePath);
        }

        void InternalLoadValid(string aFilePath)
        {
            string aContent= File.ReadAllText(aFilePath);
            SetValidContentPathFields(aContent,aFilePath);
        }
    }
}
