﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Web.Script.Serialization;

using log4net;

namespace cpw.Forms
{
    public class Settings
    {
        static ILog log = LogManager.GetLogger(typeof(Settings));

        public bool   IsUseProxy                { get; set; }
        public bool   IsAuthentication          { get; set; }
        public string ProxyServer               { get; set; }
        public string ProxyPort                 { get; set; }
        public string ProxyUser                 { get; set; }

        [System.ComponentModel.DefaultValueAttribute(null)]
        public string ProxyPassword             { get; set; }
        public string EncryptedProxyPassword    { get; set; }

        static Settings m_Settings= null;
        public static Settings Instance
        {
            get
            {
                if (null == m_Settings)
                    m_Settings= ReadSettings();
                return m_Settings;
            }
        }

        public static Settings ReadSettings()
        {
            if (File.Exists(PathHelper.ConfigFile))
            {
                string json_content = File.ReadAllText(PathHelper.ConfigFile);
                if (!string.IsNullOrEmpty(json_content))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Settings res= serializer.Deserialize<Settings>(json_content);
                    res.ProxyPassword= Encoding.UTF8.GetString(Transform(Convert.FromBase64String(res.EncryptedProxyPassword), Decryptor));
                    return res;
                }
            }
            return new Settings();
        }

        public void SaveSettings()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                EncryptedProxyPassword= Convert.ToBase64String(Transform(Encoding.UTF8.GetBytes(ProxyPassword), Encryptor));
                string old_ProxyPassword= ProxyPassword;
                try
                {
                    ProxyPassword= null;
                    string json_settings = serializer.Serialize(this);
                    File.WriteAllText(PathHelper.ConfigFile, json_settings);
                }
                finally
                {
                    ProxyPassword= old_ProxyPassword;
                }
            }
            catch (Exception ex)
            {
                log.Error("Не удалось сохранить настройки программы", ex);
            }
        }

        /// ///////////////////////////////////////////////////// Veil
        static TripleDESCryptoServiceProvider m_TripleDESCryptoServiceProvider= new TripleDESCryptoServiceProvider();
        static ICryptoTransform Encryptor { get { return m_TripleDESCryptoServiceProvider.CreateEncryptor(m_Veil_key, m_Veil_iv); } }
        static ICryptoTransform Decryptor { get { return m_TripleDESCryptoServiceProvider.CreateDecryptor(m_Veil_key, m_Veil_iv); } }
        static byte [] Transform(byte [] input, ICryptoTransform transform) 
        {
            if (null == input)
            {
                return null;
            }
            else
            {
                using (MemoryStream stream = new MemoryStream())
                using (CryptoStream cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(input, 0, input.Length);
                    cryptoStream.FlushFinalBlock();
                    return stream.ToArray();
                }
            }
        }
        static byte[] m_Veil_key=  ASCIIEncoding.ASCII.GetBytes("SDJI6701KQAW987MZ7678F8A");
        static byte[] m_Veil_iv=   ASCIIEncoding.ASCII.GetBytes("MHSDG98S");
        /// /////////////////////////////////////////////////////
    }
}
