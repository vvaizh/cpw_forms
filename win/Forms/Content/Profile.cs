﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web.Script.Serialization;

using log4net;

namespace cpw.Forms
{
    public interface IProfile
    {
        void   ReadFromPath(string path_to_profile_dir);
        string ToJsonStringForFormController();
    }

    public class Profile : IProfile
    {
        public object                          User        { get; set; }
        public object                          Abonent     { get; set; }
        public Dictionary<string, Extension>   Extensions  { get; set; }

        const string FileName_User= "User.json.txt";
        const string FileName_Abonent= "Abonent.json.txt";
        const string FolderName_Extensions= "Extensions";

        public Profile() { }
        Profile(string path_to_profile_dir)
        {
            ReadFromPath(path_to_profile_dir);
        }

        public static void Save(string path_to_profile_dir, string file_content, string local_content)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Profile profile= serializer.Deserialize<Profile>(local_content);

            string suser= serializer.Serialize(profile.User);
            File.WriteAllText(Path.Combine(path_to_profile_dir,FileName_User),suser);
            string sabonent= serializer.Serialize(profile.Abonent);
            File.WriteAllText(Path.Combine(path_to_profile_dir,FileName_Abonent),sabonent);

            string extension_dir_path= Path.Combine(path_to_profile_dir,FolderName_Extensions);
            if (!Directory.Exists(extension_dir_path))
                Directory.CreateDirectory(extension_dir_path);
            if (null != profile.Extensions)
            {
                foreach (KeyValuePair<string, Extension> kvp in profile.Extensions)
                {
                    string epath = Path.Combine(extension_dir_path, kvp.Key);
                    Directory.CreateDirectory(epath);
                    Extension extension = kvp.Value;
                    suser= serializer.Serialize(extension.User);
                    File.WriteAllText(Path.Combine(epath, FileName_User), suser);
                    sabonent= serializer.Serialize(extension.Abonent);
                    File.WriteAllText(Path.Combine(epath,FileName_Abonent), sabonent);
                }
            }
        }

        public static IProfile CreateFromPath(string path_to_profile_dir)
        {
            return new Profile(path_to_profile_dir);
        }

        Extension SafeExtension(ref Extension extension)
        {
            if (null==extension)
                extension= new Extension();
            return extension;
        }

        public void ReadFromPath(string path_to_profile_dir)
        {
            User=    FilePortion.ReadJsonObjectFile(Path.Combine(path_to_profile_dir,FileName_User));
            Abonent= FilePortion.ReadJsonObjectFile(Path.Combine(path_to_profile_dir,FileName_Abonent));

            DirectoryInfo extensions_dir= new DirectoryInfo(Path.Combine(path_to_profile_dir,FolderName_Extensions));
            if (extensions_dir.Exists)
            {
                DirectoryInfo[] extensions_dirs = extensions_dir.GetDirectories();
                foreach (DirectoryInfo extension_dir in extensions_dirs)
                {
                    string extension_dir_path= extension_dir.FullName;
                    Extension extension= null;
                    object extension_Abonent= FilePortion.ReadJsonObjectFile(Path.Combine(extension_dir_path,FileName_Abonent));
                    if (null != extension_Abonent)
                        SafeExtension(ref extension).Abonent= extension_Abonent;
                    object extension_User= FilePortion.ReadJsonObjectFile(Path.Combine(extension_dir_path,FileName_User));
                    if (null != extension_User)
                        SafeExtension(ref extension).User= extension_User;
                    if (null != extension)
                    {
                        if (null == Extensions)
                            Extensions= new Dictionary<string,Extension>();
                        Extensions.Add(extension_dir.Name,extension);
                    }
                }
            }
        }

        public string ToJsonStringForFormController()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(this);
        }
    }

    public class Extension
    {
        public object Abonent { get; set; }
        public object User    { get; set; }
    }

    internal class FilePortion
    {
        static ILog log = LogManager.GetLogger(typeof(FilePortion));

        static T ReadJsonFile<T>(string fullpath_to_file, Func<T> Default, Func<string,T> ReadFromJsonText)
        {
            try
            {
                if (!File.Exists(fullpath_to_file))
                {
                    log.ErrorFormat("not found file {0}", fullpath_to_file);
                }
                else
                {
                    string json_text = File.ReadAllText(fullpath_to_file);
                    return ReadFromJsonText(json_text);
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("can not read \"{0}\"", fullpath_to_file), ex);
            }
            return Default();
        }

        static JavaScriptSerializer serializer = new JavaScriptSerializer();

        internal static T ReadJsonTypedFile<T>(string fullpath_to_file) where T : new()
        {
            return ReadJsonFile(fullpath_to_file,()=>new T(),(json_text)=>serializer.Deserialize<T>(json_text));
        }

        internal static object ReadJsonObjectFile(string fullpath_to_file)
        {
            return ReadJsonFile<object>(fullpath_to_file,()=>null,(json_text)=>serializer.DeserializeObject(json_text));
        }
    }
}
