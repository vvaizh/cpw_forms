﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

using log4net;

namespace cpw.Forms
{
    namespace JsonSpec
    {
        public class FileFormat
        {
            public string FileExtension;
            public string Description;
        }

        public class Module
        {
            public string key;
            public string Title;
        }

        public class Form : Module
        {
            public FileFormat FileFormat;
        }

        public class Extension : Module
        {
            public Form [] forms;
        }
    }

    public class Spec
    {
        public string               key;
        public JsonSpec.Extension   extension;
        public JsonSpec.Form        form;

        public Spec()
        {
        }

        public Spec(JsonSpec.Extension extension, JsonSpec.Form form)
        {
            this.extension= extension;
            this.form= form;
            this.key= string.Format("{0}.{1}",extension.key,form.key);
        }

        public override string ToString() // используется в списках выбора..
        {
            return form.FileFormat.Description;
        }

        public string ToFileDialogFilterString()
        {
            return string.Format("{0} (*.{1})|*.{1}", form.FileFormat.Description, form.FileFormat.FileExtension);
        }
    }

    public class Specs : List<Spec>
    {
        public Spec SafeGetByKey(string key)
        {
            return this.FirstOrDefault(spec => key==spec.key);
        }

        public Specs() { }

        public Specs(JsonSpec.Extension [] extensions)
        {
            Clear();
            if (null != extensions)
            {
                foreach (JsonSpec.Extension extension in extensions)
                    Add(extension);
            }
        }

        public void Add(JsonSpec.Extension extension)
        {
            if (null != extension)
            {
                JsonSpec.Form [] forms= extension.forms;
                if (null != forms)
                {
                    foreach (JsonSpec.Form form in forms)
                    {
                        Add(new Spec(extension,form));
                    }
                }
            }
        }
    }

    public class ExtensionCollector
    {
        public Specs Specs= new Specs();
        public Dictionary<string,JsonSpec.Extension> Extensions= new Dictionary<string,JsonSpec.Extension>();
        public void OnLoadExtension(JsonSpec.Extension extension)
        {
            Extensions.Add(extension.key,extension);
            Specs.Add(extension);
        }
    }
}
