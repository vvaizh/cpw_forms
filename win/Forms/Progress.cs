﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using log4net;

namespace cpw.Forms
{
    public partial class Progress : Form
    {
        protected static ILog log = LogManager.GetLogger(typeof(Progress));

        public Progress()
        {
            InitializeComponent();
            IconHelper.FixFormIcon(this);
        }

        public static void ShowMessage(string text)
        {
            log.Debug("ShowMessage " + text);
            if (null != instance && !instance.txtLog.IsDisposed)
            {
                if (0 != instance.txtLog.TextLength)
                    instance.txtLog.AppendText("\r\n");
                instance.txtLog.AppendText(DateTime.Now.ToLongTimeString());
                instance.txtLog.AppendText("    ");
                instance.txtLog.AppendText(text);
            }
        }

        Exception exception_to_show= null;
        public static void ShowError(IWin32Window owner, string text, Exception e)
        {
            instance.txtLog.AppendText("\r\n");
            ShowMessage(text);
            HideP();
            instance.btnClose.Visible= true;
            if (null != e)
            {
                instance.linkLabelError.Visible = true;
                instance.exception_to_show = e;
            }
            instance.ShowDialog(owner);
        }

        static Progress instance= null;
        public static void Start(IWin32Window owner, string title, string message)
        {
            CloseP();
            instance= new Progress();
            SaveFixPosition(owner);
            instance.txtLog.Clear();
            instance.Text= title;
            ShowP(owner);
            ShowMessage(message);
        }

        public static bool FormIsVisible
        {
            get
            {
                return null != instance && instance.Visible;
            }
        }

        public static void CloseP()
        {
            if (null != instance)
            {
                instance.Close();
                instance = null;
            }
        }

        public static void HideP()
        {
            instance.Hide();
        }

        static void SaveFixPosition(IWin32Window owner)
        {
            Control fowner= owner as Control;
            while (null != fowner.Parent)
                fowner= fowner.Parent;
            if (null != fowner)
            {
                instance.Location= new Point
                    (
                    fowner.Location.X + (fowner.Width - instance.Width)/2,
                    fowner.Location.Y + (fowner.Height - instance.Height)/2
                    );
            }
        }

        public static void ShowP(IWin32Window owner)
        {
            SaveFixPosition(owner);
            instance.Show(owner);
            SaveFixPosition(owner);
        }

        private void linkLabelError_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (null != exception_to_show)
            {
                instance.txtLog.AppendText("\r\n\r\nЗафиксирована исключительная ситуация (");
                instance.txtLog.AppendText(exception_to_show.GetType().FullName);
                instance.txtLog.AppendText("):\r\n");
                instance.txtLog.AppendText(exception_to_show.Message);
                instance.txtLog.AppendText("\r\n\r\nПри вызове:\r\n");
                instance.txtLog.AppendText(exception_to_show.StackTrace);
            }
        }
    }
}
