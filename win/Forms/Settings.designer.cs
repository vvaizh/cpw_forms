﻿namespace cpw.Forms
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbProxyServer = new System.Windows.Forms.GroupBox();
            this.pnlProxy = new System.Windows.Forms.Panel();
            this.chbIsAuthorization = new System.Windows.Forms.CheckBox();
            this.txtProxyPort = new System.Windows.Forms.TextBox();
            this.txtProxyServer = new System.Windows.Forms.TextBox();
            this.lblProxyServer = new System.Windows.Forms.Label();
            this.lblProxyPort = new System.Windows.Forms.Label();
            this.pnlAuthorization = new System.Windows.Forms.Panel();
            this.txtProxyUser = new System.Windows.Forms.TextBox();
            this.lblProxyUser = new System.Windows.Forms.Label();
            this.lblProxyPassword = new System.Windows.Forms.Label();
            this.txtProxyPassword = new System.Windows.Forms.TextBox();
            this.chbIsProxyUse = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.gbProxyServer.SuspendLayout();
            this.pnlProxy.SuspendLayout();
            this.pnlAuthorization.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbProxyServer
            // 
            this.gbProxyServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProxyServer.Controls.Add(this.pnlProxy);
            this.gbProxyServer.Controls.Add(this.chbIsProxyUse);
            this.gbProxyServer.Location = new System.Drawing.Point(12, 7);
            this.gbProxyServer.Name = "gbProxyServer";
            this.gbProxyServer.Size = new System.Drawing.Size(568, 141);
            this.gbProxyServer.TabIndex = 2;
            this.gbProxyServer.TabStop = false;
            this.gbProxyServer.Text = "Прокси-сервер";
            // 
            // pnlProxy
            // 
            this.pnlProxy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProxy.Controls.Add(this.chbIsAuthorization);
            this.pnlProxy.Controls.Add(this.txtProxyPort);
            this.pnlProxy.Controls.Add(this.txtProxyServer);
            this.pnlProxy.Controls.Add(this.lblProxyServer);
            this.pnlProxy.Controls.Add(this.lblProxyPort);
            this.pnlProxy.Controls.Add(this.pnlAuthorization);
            this.pnlProxy.Location = new System.Drawing.Point(4, 44);
            this.pnlProxy.Name = "pnlProxy";
            this.pnlProxy.Size = new System.Drawing.Size(558, 94);
            this.pnlProxy.TabIndex = 3;
            // 
            // chbIsAuthorization
            // 
            this.chbIsAuthorization.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chbIsAuthorization.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chbIsAuthorization.Location = new System.Drawing.Point(3, 24);
            this.chbIsAuthorization.Name = "chbIsAuthorization";
            this.chbIsAuthorization.Size = new System.Drawing.Size(88, 21);
            this.chbIsAuthorization.TabIndex = 3;
            this.chbIsAuthorization.Text = "Авторизация";
            // 
            // txtProxyPort
            // 
            this.txtProxyPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtProxyPort.Location = new System.Drawing.Point(274, 0);
            this.txtProxyPort.Name = "txtProxyPort";
            this.txtProxyPort.Size = new System.Drawing.Size(81, 21);
            this.txtProxyPort.TabIndex = 2;
            this.txtProxyPort.Tag = "Прокси-сервер";
            // 
            // txtProxyServer
            // 
            this.txtProxyServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtProxyServer.Location = new System.Drawing.Point(108, 0);
            this.txtProxyServer.Name = "txtProxyServer";
            this.txtProxyServer.Size = new System.Drawing.Size(123, 21);
            this.txtProxyServer.TabIndex = 1;
            this.txtProxyServer.Tag = "Прокси-сервер";
            // 
            // lblProxyServer
            // 
            this.lblProxyServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProxyServer.AutoSize = true;
            this.lblProxyServer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblProxyServer.Location = new System.Drawing.Point(22, 2);
            this.lblProxyServer.Name = "lblProxyServer";
            this.lblProxyServer.Size = new System.Drawing.Size(86, 13);
            this.lblProxyServer.TabIndex = 8;
            this.lblProxyServer.Text = "Прокси-сервер:";
            this.lblProxyServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProxyPort
            // 
            this.lblProxyPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProxyPort.AutoSize = true;
            this.lblProxyPort.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblProxyPort.Location = new System.Drawing.Point(238, 2);
            this.lblProxyPort.Name = "lblProxyPort";
            this.lblProxyPort.Size = new System.Drawing.Size(36, 13);
            this.lblProxyPort.TabIndex = 10;
            this.lblProxyPort.Text = "Порт:";
            this.lblProxyPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlAuthorization
            // 
            this.pnlAuthorization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAuthorization.Controls.Add(this.txtProxyUser);
            this.pnlAuthorization.Controls.Add(this.lblProxyUser);
            this.pnlAuthorization.Controls.Add(this.lblProxyPassword);
            this.pnlAuthorization.Controls.Add(this.txtProxyPassword);
            this.pnlAuthorization.Location = new System.Drawing.Point(0, 46);
            this.pnlAuthorization.Name = "pnlAuthorization";
            this.pnlAuthorization.Size = new System.Drawing.Size(558, 48);
            this.pnlAuthorization.TabIndex = 2;
            // 
            // txtProxyUser
            // 
            this.txtProxyUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProxyUser.Location = new System.Drawing.Point(108, 0);
            this.txtProxyUser.Name = "txtProxyUser";
            this.txtProxyUser.Size = new System.Drawing.Size(447, 21);
            this.txtProxyUser.TabIndex = 4;
            this.txtProxyUser.Tag = "Пользователь";
            // 
            // lblProxyUser
            // 
            this.lblProxyUser.AutoSize = true;
            this.lblProxyUser.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblProxyUser.Location = new System.Drawing.Point(22, 2);
            this.lblProxyUser.Name = "lblProxyUser";
            this.lblProxyUser.Size = new System.Drawing.Size(83, 13);
            this.lblProxyUser.TabIndex = 8;
            this.lblProxyUser.Text = "Пользователь:";
            this.lblProxyUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProxyPassword
            // 
            this.lblProxyPassword.AutoSize = true;
            this.lblProxyPassword.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblProxyPassword.Location = new System.Drawing.Point(22, 26);
            this.lblProxyPassword.Name = "lblProxyPassword";
            this.lblProxyPassword.Size = new System.Drawing.Size(48, 13);
            this.lblProxyPassword.TabIndex = 9;
            this.lblProxyPassword.Text = "Пароль:";
            this.lblProxyPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProxyPassword
            // 
            this.txtProxyPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProxyPassword.Location = new System.Drawing.Point(108, 24);
            this.txtProxyPassword.Name = "txtProxyPassword";
            this.txtProxyPassword.PasswordChar = '*';
            this.txtProxyPassword.Size = new System.Drawing.Size(447, 21);
            this.txtProxyPassword.TabIndex = 5;
            // 
            // chbIsProxyUse
            // 
            this.chbIsProxyUse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chbIsProxyUse.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chbIsProxyUse.Location = new System.Drawing.Point(8, 16);
            this.chbIsProxyUse.Name = "chbIsProxyUse";
            this.chbIsProxyUse.Size = new System.Drawing.Size(152, 24);
            this.chbIsProxyUse.TabIndex = 2;
            this.chbIsProxyUse.Text = "Использовать прокси";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(490, 153);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(391, 153);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(90, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Сохранить";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // FormSettings
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(592, 184);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gbProxyServer);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(608, 222);
            this.Name = "FormSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройка соединения";
            this.gbProxyServer.ResumeLayout(false);
            this.pnlProxy.ResumeLayout(false);
            this.pnlProxy.PerformLayout();
            this.pnlAuthorization.ResumeLayout(false);
            this.pnlAuthorization.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbProxyServer;
        private System.Windows.Forms.Panel pnlProxy;
        private System.Windows.Forms.CheckBox chbIsAuthorization;
        private System.Windows.Forms.TextBox txtProxyPort;
        private System.Windows.Forms.TextBox txtProxyServer;
        private System.Windows.Forms.Label lblProxyServer;
        private System.Windows.Forms.Label lblProxyPort;
        private System.Windows.Forms.Panel pnlAuthorization;
        private System.Windows.Forms.TextBox txtProxyUser;
        private System.Windows.Forms.Label lblProxyUser;
        private System.Windows.Forms.Label lblProxyPassword;
        private System.Windows.Forms.TextBox txtProxyPassword;
        private System.Windows.Forms.CheckBox chbIsProxyUse;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
    }
}