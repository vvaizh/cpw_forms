﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Web.Script.Serialization;

using log4net;
using cpw.Forms;

namespace cpw.Forms
{
    public partial class FormBrowser : Form
    {
        ILog log = LogManager.GetLogger(typeof(FormBrowser));

        string path_to_index_html= Path.Combine(PathHelper.WebRootPath,"forms.html");
        string ProfilePath= PathHelper.DefaultProfilePath;
        string ExtensionsPath= PathHelper.ExtensionsPath;

        const string tplTitle= "КриптоВеб Формы";
        const string tplTitleForNewDocument = "КриптоВеб Новая Форма \"{0}\"";
        const string tplTitleForDocument = "КриптоВеб Форма \"{0}\" из файла \"{1}\" из папки \"{2}\"";
        const string tplTitleForUnknownDocument = "КриптоВеб Форма из файла \"{0}\" из папки \"{1}\"";
        const string tplTitleForDraft = "КриптоВеб Форма \"{0}\" из черновика \"{1}\" из папки \"{2}\"";

        internal ExtensionCollector ecollector= new ExtensionCollector();
        internal Document m_Document;
        internal StatesMachine States= new StatesMachine();
        internal JavascriptListener jslistener;

        public FormBrowser()
        {
            InitializeComponent();
            IconHelper.FixFormIcon(this);

            jslistener= new JavascriptListener(browser);
            jslistener.InitStdFunctions();

            States.Start(
                new States.LoadHtml(browser,path_to_index_html,
                    new States.LoadExtensions(browser, ExtensionsPath, jslistener, ecollector,
                        new States.FormsStartPage(this)
                    )
                )
            );
            States.Update+=OnStateUpdate;
        }

        void OnStateUpdate(object sender, EventArgs e)
        {
            miMenu.Visible= true;
            foreach (ToolStripItem mi in msCommonMenu.Items)
            {
                if (mi!=miMenu)
                    mi.Visible= false;
            }
            smiCreateDocument.Enabled= false;
            smiOpenDocument.Enabled= false;
            smiSaveDocument.Enabled= false;
            smiSaveDocumentAs.Enabled= false;
            UpdateTitle();
            UpdateStatusLine();
        }

        internal void UpdateStatusLine()
        {
            List<State> states_track= new List<State>();
            for (State s = States.Current; null != s; s = s.Parent)
            {
                if (s.RealState)
                    states_track.Add(s);
#if DEBUG
                if (10 < states_track.Count)
                {
                    states_track.Reverse();
                    throw new Exception("Похоже запутались состояния, их слишком много..");
                }
#endif
            }
            states_track.Reverse();
            flBreadCrumbs.Controls.Clear();
            Action<string> AddLabel= (txt)=>
                {
                    Label label= new Label();
                    label.AutoSize= true;
                    label.Text= txt;
                    flBreadCrumbs.Controls.Add(label);
                };
            int count= 1;
            foreach (State s in states_track)
            {
                if (count == states_track.Count)
                {
                    AddLabel(s.Name);
                }
                else
                {
                    LinkLabel label= new LinkLabel();
                    label.AutoSize= true;
                    label.Text= s.Name;
                    flBreadCrumbs.Controls.Add(label);
                    State state= s;
                    label.Click += new EventHandler((sender, e) => {
                        state.BackToMe(() => { });
                    });
                    AddLabel("|");
                }
                count++;
            }
        }

        internal void UpdateTitle()
        {
            if (null == m_Document)
            {
                this.Text = tplTitle;
            }
            else if (string.IsNullOrEmpty(m_Document.FilePath))
            {
                this.Text = string.Format(tplTitleForNewDocument, m_Document.Spec.form.FileFormat.Description);
            }
            else if (null == m_Document.Spec)
            {
                this.Text = string.Format(
                    tplTitleForUnknownDocument,
                    Path.GetFileName(m_Document.FilePath),
                    Path.GetDirectoryName(m_Document.FilePath));
            }
            else
            {
                this.Text = string.Format(
                    m_Document.isDraft ? tplTitleForDraft : tplTitleForDocument,
                    m_Document.Spec.form.FileFormat.Description,
                    Path.GetFileName(m_Document.FilePath),
                    Path.GetDirectoryName(m_Document.FilePath));
            }

        }

        private void miClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void miEditProfile_Click(object sender, EventArgs e)
        {
            ProfileBrowser.Instance.Show(this);
        }

        private void FormBrowser_FormClosing(object sender, FormClosingEventArgs e)
        {
            ProfileBrowser.SafeCloseInstance();

            if (null != States.Current)
            {
                bool Closing_done= false;

                States.StopOnOkOnReject(() => {
                    if (Closing_done)
                        Close();
                    Closing_done= true;
                }, () => {
                    if (!Closing_done)
                        e.Cancel = true;
                    Closing_done= true;
                });

                if (!Closing_done)
                {
                    Closing_done = true;
                    e.Cancel = true;
                }
            }
        }

        private void smiSettings_Click(object sender, EventArgs e)
        {
            using (FormSettings frm = new FormSettings())
            {
                Settings settings= Settings.Instance;
                frm.DataLoad(settings);
                if (DialogResult.OK == frm.ShowDialog(this))
                {
                    frm.DataSave(settings);
                    settings.SaveSettings();
                }
            }
        }
    }
}
