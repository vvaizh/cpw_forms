﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

using log4net;

namespace cpw.Forms
{
    public partial class FormSettings : Form
    {
        ILog log = LogManager.GetLogger(typeof(FormSettings));

        public FormSettings()
        {
            IconHelper.FixFormIcon(this);
            InitializeComponent();
        }

        public void DataLoad(Settings settings)
        {
            chbIsProxyUse.Checked=      settings.IsUseProxy;
            chbIsAuthorization.Checked= settings.IsAuthentication;
            txtProxyPort.Text=          settings.ProxyPort;
            txtProxyServer.Text=        settings.ProxyServer;
            txtProxyUser.Text=          settings.ProxyUser;
            txtProxyPassword.Text=      settings.ProxyPassword;
        }

        public void DataSave(Settings settings)
        {
            settings.IsUseProxy=        chbIsProxyUse.Checked;
            settings.IsAuthentication=  chbIsAuthorization.Checked;
            settings.ProxyPort=         txtProxyPort.Text;
            settings.ProxyServer=       txtProxyServer.Text;
            settings.ProxyUser=         txtProxyUser.Text;
            settings.ProxyPassword=     txtProxyPassword.Text;
            settings.SaveSettings();
        }
    }
}
