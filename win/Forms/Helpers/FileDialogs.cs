﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Packaging;
using System.Text;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace cpw.Forms
{
    public static class FileDialogHelper
    {
        public static string ChooseFileToStore(IWin32Window owner, Document doc)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Title = "Сохранить форму как...";
                if (string.IsNullOrEmpty(doc.FilePath))
                {
                    dlg.FileName= string.Format("{0}_{1}", doc.Spec.key, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                }
                else
                {
                    dlg.InitialDirectory = Path.GetDirectoryName(doc.FilePath);
                    dlg.FileName= Path.GetFileName(doc.FilePath);
                }
                dlg.DefaultExt= doc.Spec.form.FileFormat.FileExtension;
                StringBuilder sb= new StringBuilder();
                sb.Append(doc.Spec.ToFileDialogFilterString());
                sb.Append("|");
                sb.Append(DraftHelper.FileDialogFilterString);
                dlg.FilterIndex= doc.isDraft ? 2 : 1;
                dlg.Filter= sb.ToString();
                return (DialogResult.OK != dlg.ShowDialog(owner)) ? null : dlg.FileName;
            }
        }

        public static string ChooseFileToStoreDraft(IWin32Window owner, Document doc)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Title = "Сохранить черновик как...";
                if (string.IsNullOrEmpty(doc.FilePath))
                {
                    dlg.FileName= doc.Spec.key;
                }
                else
                {
                    dlg.InitialDirectory = Path.GetDirectoryName(doc.FilePath);
                    dlg.FileName= Path.GetFileName(doc.FilePath);
                }
                dlg.DefaultExt= DraftHelper.FileExtension;
                dlg.Filter= DraftHelper.FileDialogFilterString;
                return (DialogResult.OK != dlg.ShowDialog(owner)) ? null : dlg.FileName;
            }
        }

        const string txtAutoFileDialogFilterString= "Черновики и файлы|*.xml;*.dft";
        const string txtAutoXmlFileDialogFilterString= "Авто определение xml|*.xml";

        static readonly string [] AutoFileDialogFilterStrings=
        {
            txtAutoFileDialogFilterString,
            txtAutoXmlFileDialogFilterString,
            DraftHelper.FileDialogFilterString
        };

        public static string GetFilterForFileDialog(Specs specs)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string AutoFileDialogFilterString in AutoFileDialogFilterStrings)
            {
                if (0 != sb.Length)
                    sb.Append("|");
                sb.Append(AutoFileDialogFilterString);
            }
            foreach (Spec spec in specs)
            {
                if (0 != sb.Length)
                    sb.Append("|");
                sb.Append(spec.ToFileDialogFilterString());
            }
            return sb.ToString();
        }

        public static string ChooseFileToLoad(IWin32Window owner, Specs specs, out Spec spec)
        {
            spec = null;
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Открыть файл..";
                dlg.Multiselect = false;
                dlg.Filter = GetFilterForFileDialog(specs);
                if (DialogResult.OK != dlg.ShowDialog(owner))
                {
                    return null;
                }
                else
                {
                    int iSpec= dlg.FilterIndex - AutoFileDialogFilterStrings.Length;
                    if (0 <= iSpec)
                        spec= specs[iSpec];
                    return dlg.FileName;
                }
            }
        }
    }
}
