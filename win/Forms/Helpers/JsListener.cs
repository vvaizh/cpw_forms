﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web.Script.Serialization;

using log4net;

namespace cpw.Forms
{
    public interface IJavascriptListener
    {
        void OnLinkClick(object sender, HtmlElementEventArgs e);

        void OnCallFunction(string func_name, object args, Control control, HtmlDocument document);
    }

    public class JavascriptListenerBase : IJavascriptListener
    {
        protected static ILog log= LogManager.GetLogger(typeof(JavascriptListenerBase));

        protected WebBrowser browser;
        public JavascriptListenerBase(WebBrowser browser)
        {
            this.browser = browser;
        }

        void OnLinkClickInHtmlDocument(Control control, HtmlDocument document)
        {
            object o_function_and_parameters= document.InvokeScript("cpw_from_desktop_GetDesktopFunctionAndParameters");
            if (null != o_function_and_parameters)
            {
                string saltar_content= o_function_and_parameters.ToString();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                object altar_content= serializer.DeserializeObject(saltar_content);
                Dictionary<string,object> daltar_content= (Dictionary<string,object>)altar_content;
                string func_name= (string)daltar_content["func_name"];
                object args= null;
                daltar_content.TryGetValue("arguments", out args);
                OnCallFunction(func_name,args,control,document);
            }
        }

        protected void OnLinkClickInvoke(Control control, HtmlDocument document)
        {
            control.Invoke(new MethodInvoker(delegate{OnLinkClickInHtmlDocument(control,document);}));
        }

        public void OnLinkClick(object sender, HtmlElementEventArgs e)
        {
            OnLinkClickInvoke(browser,browser.Document);
        }

        public virtual void OnCallFunction(string func_name, object args, Control control, HtmlDocument document)
        {
            MessageBox.Show(browser,"Вызов из javascript неизвестной функции ",
                string.Format("Из jaavscript вызвана неизвестная функция \"{0}\" которая должна выполняться на стороне Desktop приложения",func_name));
        }
    }

    public class JavascriptListener : JavascriptListenerBase
    {
        Dictionary<string,Action<object,Control,HtmlDocument>> m_Functions= new Dictionary<string,Action<object,Control,HtmlDocument>>();

        public JavascriptListener(WebBrowser browser) : base(browser) { }

        public override void OnCallFunction(string func_name, object args, Control control, HtmlDocument html_document)
        {
            Action<object,Control,HtmlDocument> fitem;
            if (m_Functions.TryGetValue(func_name, out fitem))
            {
                fitem(args,control,html_document);
            }
            else
            {
                base.OnCallFunction(func_name,args,control,html_document);
            }
        }

        internal bool RemoveFunc(string name)
        {
            log.DebugFormat("RemoveFunc(\"{0}\")",name);
            return m_Functions.Remove(name);
        }

        internal void AddFunc(string name, Action<object,Control,HtmlDocument> action)
        {
            log.DebugFormat("AddFunc(\"{0}\")",name);
            m_Functions.Add(name,action);
        }

        internal void RemoveFunc<T>(Action<T> action)
        {
            RemoveFunc(action.Method.Name);
        }

        internal void AddFunc<T>(Action<T> action)
        {
            AddFunc(action.Method.Name,
                (args,control,html_document)=>
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string sargs= serializer.Serialize(args);
                    action(serializer.Deserialize<T>(sargs));
                }
            );
        }

        internal void AddFunc(Action<string> action)
        {
            AddFunc(action.Method.Name,(args,control,html_document)=>{action(args.ToString());});
        }

        internal void AddFunc(string name, Action action)
        {
            AddFunc(name,(args,control,html_document)=>{action();});
        }

        internal void AddAutoRemoveFunc(string name, Action action)
        {
            AddFunc
           (
                name,
                (args,control,html_document)=>
                {
                    if (!RemoveFunc(name))
                        throw new Exception(string.Format("Can not remove javascript listener function \"{0}\"",name));
                    action();
                }
            );
        }

        internal void AddAutoRemoveFunc(Action action)
        {
            AddAutoRemoveFunc(action.Method.Name,action);
        }

        internal void AddFunc(Action action)
        {
            AddFunc(action.Method.Name,action);
        }

        internal void AddFunc(Action<IWin32Window,string> action)
        {
            AddFunc(action.Method.Name,(args,control,html_document)=>{action(control,args.ToString());});
        }

        internal void AddFunc(Action<Dictionary<string,object>> action)
        {
            AddFunc(action.Method.Name,(args,control,html_document)=>{action((Dictionary<string,object>)args);});
        }

        internal void AddFunc(Action<HtmlDocument,Dictionary<string,object>> action)
        {
            AddFunc(action.Method.Name,(args,control,html_document)=>{action(html_document,(Dictionary<string,object>)args);});
        }

        internal void AddFunc(Action<HtmlDocument,string> action)
        {
            AddFunc(action.Method.Name,(args,control,html_document)=>{action(html_document,args.ToString());});
        }

        internal void InitStdFunctions()
        {
            AddFunc(FatalError);
            AddFunc(Error);
            AddFunc(ajax);
        }

        void Error(IWin32Window owner, string txt)
        {
            if (Progress.FormIsVisible)
            {
                Progress.ShowError(owner, txt, null);
            }
            else
            {
                MessageBox.Show(owner,txt,"Ошибка");
            }
        }

        void FatalError(IWin32Window owner, string txt)
        {
            if (!Progress.FormIsVisible)
            {
                MessageBox.Show(owner, txt, "Фатальная ошибка");
                //NavigateToStart();
            }
            else
            {
                Progress.ShowMessage("javascript код инициировал фатальную ошибку");
                Progress.ShowError(owner,txt,null);
            }
        }

        void ajax(HtmlDocument html_document, Dictionary<string,object> args)
        {
            string res = AjaxHelper.Instance.ajax(args);
            html_document.InvokeScript("cpw_from_desktop_SetDesktopFunctionResult", new object[] { res });
        }
    }
}
