﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

using log4net;

namespace cpw.Forms
{
    internal interface IAjaxHelper
    {
        string ajax(Dictionary<string, object> args);
    }

    internal class AjaxHelper : IAjaxHelper
    {
        ILog log = LogManager.GetLogger(typeof(AjaxHelper));

        internal static readonly IAjaxHelper Instance= new AjaxHelper();

        WebProxy PrepareWebProxy(Settings settings)
        {
            WebProxy proxy = new WebProxy(settings.ProxyServer, int.Parse(settings.ProxyPort));
            proxy.Credentials = !settings.IsAuthentication
                ? new System.Net.NetworkCredential()
                : new System.Net.NetworkCredential(settings.ProxyUser, settings.ProxyPassword);
            return proxy;
        }

        string HttpGet(string url)
        {
            System.Net.HttpWebRequest request= (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);

            Settings settings= Settings.Instance;
            if (settings.IsUseProxy)
                request.Proxy= PrepareWebProxy(settings);

            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                return readStream.ReadToEnd();
            }
        }

        string IAjaxHelper.ajax(Dictionary<string, object> args)
        {
            string url= args["url"].ToString();
            log.DebugFormat("url=\"{0}\"",url);

            StringBuilder sb= new StringBuilder();
            sb.Append(url);
            int start_len= sb.Length;

            object odata;
            if (args.TryGetValue("data", out odata))
            {
                Dictionary<string, object> ddata = odata as Dictionary<string, object>;
                if (null != ddata)
                {
                    foreach (KeyValuePair<string, object> kvp in ddata)
                    {
                        string svalue = kvp.Value as string;
                        if (null != svalue)
                        {
                            sb.Append(sb.Length == start_len ? "?" : "&");
                            sb.Append(kvp.Key);
                            sb.Append("=");
                            sb.Append(svalue);
                        }
                    }
                }
            }

            return HttpGet(sb.ToString());
        }
    }
}
