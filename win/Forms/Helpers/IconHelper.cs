﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace cpw.Forms
{
    public class IconHelper
    {
        public static void FixFormIcon(Form frm)
        {
            Form mainfrm = Control.FromHandle(System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle) as Form;
            if (mainfrm != null)
                frm.Icon = mainfrm.Icon;
        }
    }
}
