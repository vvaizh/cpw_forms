﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Packaging;
using System.Text;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace cpw.Forms
{
    public static class JacascriptLoader
    {
        internal class ExtensionLoader
        {
            internal IWin32Window        owner;
            internal HtmlDocument        document;
            internal FileInfo[]          extensions_files;
            internal JavascriptListener  jsListener;
            internal ExtensionCollector  ecollector;
            internal Action              on_load;
            internal int                 iextension_file= 0;

            internal void Load()
            {
                if (iextension_file >= extensions_files.Length)
                {
                    Progress.ShowMessage("Успешно завершили загрузку расширений.");
                    jsListener.RemoveFunc<JsonSpec.Extension>(OnLoadExtension);
                    on_load();
                }
                else
                {
                    if (0==iextension_file)
                        jsListener.AddFunc<JsonSpec.Extension>(OnLoadExtension);
                    FileInfo extension_file = extensions_files[iextension_file++];
                    LoadJavascriptFromFile(extension_file.FullName, document);
                }
            }

            internal void OnLoadExtension(JsonSpec.Extension extension)
            {
                Progress.ShowMessage("} Загрузка завершена");
                ecollector.OnLoadExtension(extension);
                Load();
            }
        }

        public static void LoadProfileEditor(WebBrowser browser, string ProfileEditorPath, JavascriptListener jsListener, Action OnProfileEditorLoaded)
        {
            Progress.ShowMessage("Загружаем скрипт редактора профиля из файла\r\n" + ProfileEditorPath);
            LoadExtensionFromFile(ProfileEditorPath, browser.Document, jsListener, "OnLoadProfileController", OnProfileEditorLoaded);
        }

        public static void Load(WebBrowser browser, string path_to_extension_dir,
            JavascriptListener jsListener, ExtensionCollector ecollector, Action on_load)
        {
            Progress.ShowMessage("Начинаем загрузку расширений");
            Progress.ShowMessage("Ищем расширения в виде файлов *.js в папке\r\n" + path_to_extension_dir);
            DirectoryInfo extensions_dir = new DirectoryInfo(path_to_extension_dir);
            if (!extensions_dir.Exists)
            {
                throw new System.Exception("Папки с расширениями НЕ существует!");
            }
            else
            {
                FileInfo[] extensions_files = extensions_dir.GetFiles("*.js");
                if (null == extensions_files || 0 == extensions_files.Length)
                {
                    throw new System.Exception("В папки с расширениями отсутствуют *.js файлы!");
                }
                else
                {
                    ExtensionLoader loader= new ExtensionLoader()
                    {
                        owner= browser,
                        document= browser.Document,
                        extensions_files= extensions_files,
                        jsListener= jsListener,
                        ecollector= ecollector,
                        on_load= on_load
                    };
                    loader.Load();
                }
            }
        }

        static void LoadJavascriptFromFile(string extension_file_path, HtmlDocument document)
        {
            HtmlElementCollection head = document.GetElementsByTagName( "head" );
            HtmlElement script = document.CreateElement("script");
            script.SetAttribute("type", "text/javascript");
            script.SetAttribute("src",extension_file_path);

            Progress.ShowMessage("Загружаем скрипт из файла " + Path.GetFileName(extension_file_path));
            ((HtmlElement)head[0]).AppendChild(script);
            Progress.ShowMessage("{ Загрузка запущена..");
        }

        static void LoadExtensionFromFile(string extension_file_path, HtmlDocument document,
            JavascriptListener jsListener, string on_load_func_name, Action on_loaded)
        {
            HtmlElementCollection head = document.GetElementsByTagName( "head" );
            HtmlElement script = document.CreateElement("script");
            script.SetAttribute("type", "text/javascript");
            script.SetAttribute("src",extension_file_path);

            jsListener.AddAutoRemoveFunc(on_load_func_name, () =>
            {
                Progress.ShowMessage("} Загрузка завершена");
                on_loaded();
            });

            Progress.ShowMessage("Загружаем скрипт из файла " + Path.GetFileName(extension_file_path));
            ((HtmlElement)head[0]).AppendChild(script);
            Progress.ShowMessage("{ Загрузка запущена..");
        }
    }
}
