﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace cpw.Forms
{
    public class PathHelper
    {
        public static string AppDataPath
        {
            get
            {
                string AppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                return (string.IsNullOrEmpty(AppData) || !Directory.Exists(AppData))
                    ? string.Empty
                    : Path.Combine(AppData, @"InfoTrust\Forms");
            }
        }

        public static string TmpXamlPath
        {
            get
            {
                return Path.Combine(AppDataPath,"tmpview.xaml");
            }
        }

        public static string ConfigFile
        {
            get
            {
                return Path.Combine(AppDataPath,"config.json.txt");
            }
        }

#if DEBUG
        public static bool DebugLocalMode= false;
#endif

        public static string WebRootPath
        {
            get
            {
                return 
#if DEBUG
                    DebugLocalMode ? Path.Combine(ProgramPath,@"..\..\FormsHtml") :
#endif
                    Path.Combine(ProgramPath,"html");
            }
        }

        public static string ExtensionsPath
        {
            get
            {
                return 
#if DEBUG
                    DebugLocalMode ? Path.Combine(ProgramPath,@"..\..\FormsHtml\extensions") :
#endif
                    Path.Combine(ProgramPath,@"html\extensions");
            }
        }

        public static string DefaultProfilePath
        {
            get
            {
                return 
#if DEBUG
                    DebugLocalMode ? Path.Combine(ProgramPath,@"..\..\FormsProfile") :
#endif
                    Path.Combine(AppDataPath,@"users\default");
            }
        }

        static string ProgramPath
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }
    }
}
