﻿namespace cpw.Forms
{
    partial class FormBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBrowser));
            this.browser = new System.Windows.Forms.WebBrowser();
            this.msCommonMenu = new System.Windows.Forms.MenuStrip();
            this.miMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.smiCreateDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.smiOpenDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.smiSaveDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.smiSaveDocumentAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.smiEditProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.smiClose = new System.Windows.Forms.ToolStripMenuItem();
            this.miCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miBack = new System.Windows.Forms.ToolStripMenuItem();
            this.miPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.miProfiled = new System.Windows.Forms.ToolStripMenuItem();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miUpdateByProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.miSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.xaml_browser = new System.Windows.Forms.WebBrowser();
            this.flBreadCrumbs = new System.Windows.Forms.FlowLayoutPanel();
            this.smiSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.msCommonMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.browser.Location = new System.Drawing.Point(0, 27);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(1008, 523);
            this.browser.TabIndex = 0;
            // 
            // msCommonMenu
            // 
            this.msCommonMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMenu,
            this.miCreate,
            this.miOpen,
            this.miEdit,
            this.miBack,
            this.miPreview,
            this.miProfiled,
            this.miSave,
            this.miUpdateByProfile,
            this.miSaveAs});
            this.msCommonMenu.Location = new System.Drawing.Point(0, 0);
            this.msCommonMenu.Name = "msCommonMenu";
            this.msCommonMenu.Size = new System.Drawing.Size(1008, 24);
            this.msCommonMenu.TabIndex = 1;
            this.msCommonMenu.Text = "menuStrip1";
            // 
            // miMenu
            // 
            this.miMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smiCreateDocument,
            this.smiOpenDocument,
            this.smiSaveDocument,
            this.smiSaveDocumentAs,
            this.toolStripSeparator1,
            this.smiEditProfile,
            this.smiSettings,
            this.toolStripSeparator2,
            this.smiClose});
            this.miMenu.Name = "miMenu";
            this.miMenu.Size = new System.Drawing.Size(48, 20);
            this.miMenu.Text = "Файл";
            // 
            // smiCreateDocument
            // 
            this.smiCreateDocument.Name = "smiCreateDocument";
            this.smiCreateDocument.Size = new System.Drawing.Size(213, 22);
            this.smiCreateDocument.Text = "Создать...";
            // 
            // smiOpenDocument
            // 
            this.smiOpenDocument.Name = "smiOpenDocument";
            this.smiOpenDocument.Size = new System.Drawing.Size(213, 22);
            this.smiOpenDocument.Text = "Открыть...";
            // 
            // smiSaveDocument
            // 
            this.smiSaveDocument.Name = "smiSaveDocument";
            this.smiSaveDocument.Size = new System.Drawing.Size(213, 22);
            this.smiSaveDocument.Text = "Сохранить";
            // 
            // smiSaveDocumentAs
            // 
            this.smiSaveDocumentAs.Name = "smiSaveDocumentAs";
            this.smiSaveDocumentAs.Size = new System.Drawing.Size(213, 22);
            this.smiSaveDocumentAs.Text = "Сохранить как...";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(210, 6);
            // 
            // smiEditProfile
            // 
            this.smiEditProfile.Name = "smiEditProfile";
            this.smiEditProfile.Size = new System.Drawing.Size(213, 22);
            this.smiEditProfile.Text = "Профиль пользователя...";
            this.smiEditProfile.Click += new System.EventHandler(this.miEditProfile_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(210, 6);
            // 
            // smiClose
            // 
            this.smiClose.Name = "smiClose";
            this.smiClose.Size = new System.Drawing.Size(213, 22);
            this.smiClose.Text = "Выход";
            this.smiClose.Click += new System.EventHandler(this.miClose_Click);
            // 
            // miCreate
            // 
            this.miCreate.Name = "miCreate";
            this.miCreate.Size = new System.Drawing.Size(62, 20);
            this.miCreate.Text = "Создать";
            // 
            // miOpen
            // 
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(66, 20);
            this.miOpen.Text = "Открыть";
            // 
            // miEdit
            // 
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(99, 20);
            this.miEdit.Text = "Редактировать";
            // 
            // miBack
            // 
            this.miBack.Name = "miBack";
            this.miBack.Size = new System.Drawing.Size(163, 20);
            this.miBack.Text = "Вернуться без сохранения";
            this.miBack.Visible = false;
            // 
            // miPreview
            // 
            this.miPreview.Name = "miPreview";
            this.miPreview.Size = new System.Drawing.Size(170, 20);
            this.miPreview.Text = "Смотреть печатную форму";
            // 
            // miProfiled
            // 
            this.miProfiled.Name = "miProfiled";
            this.miProfiled.Size = new System.Drawing.Size(118, 20);
            this.miProfiled.Text = "Открыть корешёк";
            // 
            // miSave
            // 
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(77, 20);
            this.miSave.Text = "Сохранить";
            // 
            // miUpdateByProfile
            // 
            this.miUpdateByProfile.Name = "miUpdateByProfile";
            this.miUpdateByProfile.Size = new System.Drawing.Size(195, 20);
            this.miUpdateByProfile.Text = "Обновить данными из профиля";
            // 
            // miSaveAs
            // 
            this.miSaveAs.Name = "miSaveAs";
            this.miSaveAs.Size = new System.Drawing.Size(104, 20);
            this.miSaveAs.Text = "Сохранить как..";
            // 
            // xaml_browser
            // 
            this.xaml_browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.xaml_browser.Location = new System.Drawing.Point(0, 27);
            this.xaml_browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.xaml_browser.Name = "xaml_browser";
            this.xaml_browser.Size = new System.Drawing.Size(1008, 523);
            this.xaml_browser.TabIndex = 3;
            this.xaml_browser.Visible = false;
            // 
            // flBreadCrumbs
            // 
            this.flBreadCrumbs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flBreadCrumbs.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flBreadCrumbs.Location = new System.Drawing.Point(12, 555);
            this.flBreadCrumbs.Name = "flBreadCrumbs";
            this.flBreadCrumbs.Size = new System.Drawing.Size(984, 17);
            this.flBreadCrumbs.TabIndex = 4;
            // 
            // smiSettings
            // 
            this.smiSettings.Name = "smiSettings";
            this.smiSettings.Size = new System.Drawing.Size(213, 22);
            this.smiSettings.Text = "Настройки соединения...";
            this.smiSettings.Click += new System.EventHandler(this.smiSettings_Click);
            // 
            // FormBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 575);
            this.Controls.Add(this.flBreadCrumbs);
            this.Controls.Add(this.xaml_browser);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.msCommonMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msCommonMenu;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormBrowser";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "КриптоВеб Форма.";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBrowser_FormClosing);
            this.msCommonMenu.ResumeLayout(false);
            this.msCommonMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.WebBrowser browser;
        internal System.Windows.Forms.MenuStrip msCommonMenu;
        internal System.Windows.Forms.ToolStripMenuItem miMenu;
        internal System.Windows.Forms.ToolStripMenuItem smiCreateDocument;
        internal System.Windows.Forms.ToolStripMenuItem smiOpenDocument;
        internal System.Windows.Forms.ToolStripMenuItem smiSaveDocument;
        internal System.Windows.Forms.ToolStripMenuItem smiSaveDocumentAs;
        internal System.Windows.Forms.ToolStripMenuItem miBack;
        internal System.Windows.Forms.ToolStripMenuItem miEdit;
        internal System.Windows.Forms.ToolStripMenuItem smiClose;
        internal System.Windows.Forms.ToolStripMenuItem miOpen;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripMenuItem miCreate;
        internal System.Windows.Forms.ToolStripMenuItem miPreview;
        internal System.Windows.Forms.ToolStripMenuItem miSave;
        internal System.Windows.Forms.ToolStripMenuItem miProfiled;
        internal System.Windows.Forms.ToolStripMenuItem miUpdateByProfile;
        private System.Windows.Forms.ToolStripMenuItem smiEditProfile;
        internal System.Windows.Forms.ToolStripMenuItem miSaveAs;
        internal System.Windows.Forms.WebBrowser xaml_browser;
        private System.Windows.Forms.FlowLayoutPanel flBreadCrumbs;
        private System.Windows.Forms.ToolStripMenuItem smiSettings;
    }
}

