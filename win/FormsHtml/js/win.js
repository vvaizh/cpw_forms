app = {};

$.ajaxTransport
(
	'+*',
	function (options, originalOptions, jqXHR)
	{
		var res =
		{
			send: function (headers, completeCallback)
			{
				DivineHelp
				(
					'ajax',
					options,
					function (res)
					{
						completeCallback(200, 'success', { text : res } );
					}
				);
			}
			, abort: function ()
			{
			}
		}
		return res;
	}
);

function ExceptionOnLoadCpwFormsExtension(extension_name, ex)
{
	var msg= 'При загрузке модуля "' + extension_name + '"';
	alert(msg + ":\r\n" + ex);
	DivineException(msg, ex);
}

function PrepareFormHtml(extension, pos, form)
{
	var html_txt = '<div class="form" key="' + extension.key + '.' + form.key + '" >';
	html_txt += '<div class="Pos">' + pos + '</div>';
	html_txt += '<div class="Title">' + form.Title + '</div>';
	html_txt += '&nbsp;(';
	html_txt += form.FileFormat.Description;
	html_txt += '&nbsp;-&nbsp;*.';
	html_txt += form.FileFormat.FileExtension;
	html_txt += ')';
	html_txt += '</div>';
	return html_txt;
}

function PrepareExtensionHtml(extension)
{
	var html_txt = '<div class="extension">';
	html_txt += '<div class="Title">' + extension.Title + '</div>&nbsp;';
	html_txt += '<div class="key">' + extension.key + '&nbsp;</div>';
	/*if (CpwFormsExtensionFileName)
		html_txt += '&nbsp;<div class="FileName">' + CpwFormsExtensionFileName + '</div>';*/
	if (extension.forms)
	{
		html_txt += '<div class="forms">';
		var pos = 0;
		for (var key in extension.forms)
		{
			html_txt += PrepareFormHtml(extension, pos, extension.forms[key]);
			pos++;
		}
		html_txt += '</div>';
	}
	html_txt += '</div>';
	return html_txt;
}

CpwFormsExtensionsConstructors = {};
CpwFormsExtensionsInfo = [];
CpwFormsExtensionsProfileControllers = {};

function RegisterCpwFormsExtension(extension)
{
	CpwFormsExtensionsInfo.push(extension);

	if (!CpwFormsExtensionsConstructors)
		CpwFormsExtensionsConstructors = {};

	if (extension.forms)
	{
		var forms = [];
		for (var form_key in extension.forms)
		{
			var form = extension.forms[form_key];
			forms.push(form);
			CpwFormsExtensionsConstructors[extension.key + '.' + form.key] = form.CreateController;
			delete form.CreateController;
		}
		extension.forms = forms;
	}

	if (extension.profile)
	{
		CpwFormsExtensionsProfileControllers[extension.key] =
		{
			  Title: extension.Title
			, controllers: extension.profile
		};
		delete extension.profile;
	}

	DivineHelp('OnLoadExtension', extension, null);

	var div_log = $('div.start div.log');
	var html_txt = div_log.html();
	html_txt += PrepareExtensionHtml(extension)
	$('div.start div.log').html(html_txt);

	$('div.start div.log div.extension div.forms div.form').on('click', OnFormClick);
}

ProfileToEdit = null;
CpwProfileControllers = null;
function RegisterCpwProfileControllers(c)
{
	CpwProfileControllers = c;
	if (ProfileToEdit)
		EditProfileSync();
	DivineHelp('OnLoadProfileController', null, null);
}

function OnFormClick(e)
{
	var id_form = $(this).attr('key');
	DivineCreateNewForm(id_form);
	return false;
}

function cpw_from_desktop_CreateNewActiveForm(divine_func_name_on_ok)
{
	setTimeout(function ()
	{
		try
		{
			$('div.start').hide();
			$('#form-editor').show();
			var form_spec = app.current_form_spec;
			form_spec.CreateNew('form-editor');
			DivineHelp(divine_func_name_on_ok, null, null);
		}
		catch (ex)
		{
			FatalException('При создании новой формы', JSON.stringify(ex));
		}
	}, 10);
}

function cpw_from_desktop_GetActiveFormContent()
{
	try
	{
		var form_spec = app.current_form_spec;
		var validation_text = form_spec.Validate();
		if (null != validation_text && '' != validation_text)
		{
			alert(validation_text);
		}
		else
		{
			var form_content = form_spec.GetFormContent();
			if (null != form_content)
			{
				//StopView();
				return form_content;
			}
		}
	}
	catch (ex)
	{
		if (!ex.parseException)
		{
			FatalException('При попытке получить отредактированные данные формы', ex);
		}
		else
		{
			DivineException("Не удаётся получить данные в корректном формате", ex.readablemsg);
		}
		return null;
	}
}

function cpw_from_desktop_GetActiveFormDraftContent()
{
	try
	{
		var form_spec = app.current_form_spec;
		var validation_text = form_spec.Validate();
		if (null != validation_text && '' != validation_text)
		{
			alert(validation_text);
		}
		else
		{
			var GetFormContent = form_spec.GetFormContent;
			var CodecInfo = form_spec.UsedCodecInfo;
			while (CodecInfo)
			{
				GetFormContent = CodecInfo.previous.GetFormContent;
				CodecInfo = CodecInfo.previous.CodecInfo;
			}
			var form_content = GetFormContent.call(form_spec);
			if (null != form_content)
			{
				//StopView();
				return JSON.stringify(form_content);
			}
		}
	}
	catch (ex)
	{
		FatalException('При попытке получить отредактированные данные формы в виде черновика', ex);
		return null;
	}
}

function LoadDraftContentController(id_form, content_json)
{
	var form_spec_func = CpwFormsExtensionsConstructors[id_form];
	var form_spec = form_spec_func();
	app.current_form_spec = form_spec;
	var SetFormContent = form_spec.SetFormContent;
	var CodecInfo = form_spec.UsedCodecInfo;
	while (CodecInfo)
	{
		SetFormContent = CodecInfo.previous.SetFormContent;
		CodecInfo = CodecInfo.previous.CodecInfo;
	}
	SetFormContent.call(form_spec, JSON.parse(content_json));
}

function cpw_from_desktop_TryToLoadFormContent(id_form, content)
{
	var form_spec_func = CpwFormsExtensionsConstructors[id_form];
	var form_spec = form_spec_func();
	try
	{
		var set_form_content_res = form_spec.SetFormContent(content);
		return set_form_content_res;
	}
	catch (ex)
	{
		if (ex.parseException && ex.readablemsg)
		{
			return 'Ошибка формата формы: ' + ex.readablemsg;
		}
		else
		{
			return 'Исключительная ситуация:\r\n' + JSON.stringify(ex);
		}
	}
}

function PrepareExceptionToReturnToDesktop(ex)
{
	if (ex.parseexception)
	{
		return { exception_text: ex.readablemsg };
	}
	else
	{
		return { exception_text: JSON.stringify(ex) };
	}
}

function cpw_from_desktop_SetActiveFormContent(id_form, content)
{
	try
	{
		var form_spec_func = CpwFormsExtensionsConstructors[id_form];
		var form_spec = form_spec_func();
		form_spec.SetFormContent(content);
		app.current_form_spec = form_spec;
		return true;
	}
	catch (ex)
	{
		return PrepareExceptionToReturnToDesktop(ex);
	}
}

function cpw_from_desktop_GetActiveFormXamlView()
{
	try
	{
		var form_spec= app.current_form_spec;
		return form_spec.BuildXamlView();
	}
	catch (ex)
	{
		return PrepareExceptionToReturnToDesktop(ex);
	}
}

function cpw_from_desktop_SetActiveFormDraftContent(id_form, content_json)
{
	try
	{
		$('#form-editor').show();
		$('div.start').hide();
		LoadDraftContentController(id_form, content_json);
	}
	catch (ex)
	{
		FatalException('При открытии черновика', ex);
	}
}

function cpw_from_desktop_SetActiveFormSpec(id_form, profile)
{
	try
	{
		$('#form-editor').show();
		$('div.start').hide();
		var form_spec_func = CpwFormsExtensionsConstructors[id_form];
		var form_spec = form_spec_func();
		app.current_form_spec = form_spec;
		form_spec.UseProfile(JSON.parse(profile));
	}
	catch (ex)
	{
		FatalException('При открытии черновика', ex);
	}
}

function cpw_from_desktop_EditActiveForm(divine_func_name_on_ok)
{
	try
	{
		$('#form-editor').show();
		$('div.start').hide();
		var form_spec = app.current_form_spec;
		form_spec.Edit('form-editor');
		DivineHelp(divine_func_name_on_ok, null, null)
	}
	catch (ex)
	{
		FatalException('При открытии редактора', ex);
	}
}

function ViewFormDraft(id_form,content_json)
{
	setTimeout(function ()
	{
		try
		{
			$('#form-editor').show();
			$('div.start').hide();
			LoadDraftContentController(id_form, content_json);
			form_spec.Edit('form-editor');
		}
		catch (ex)
		{
			FatalException('При открытии черновика', ex);
		}
	}, 10);
}

function FatalException(text, ex)
{
	FatalError(text + ":\r\n\r\n" + ex);
}

function DivineException(text, ex)
{
	DivineError(text + ":\r\n\r\n" + ex);
}

function FatalError(text)
{
	DivineHelp('FatalError', text, null)
}

function DivineError(text)
{
	DivineHelp('Error', text, null)
}

function DivineCreateNewForm(id_form)
{
	DivineHelp('CreateNewForm', id_form, null);
}

function DivineHelp(func_name,args,callback_func)
{
	CpwFormsAltarContent =
	{
		func_name: func_name
		, arguments: args
	};
	AfflatusCallbackFunction = callback_func;
	document.getElementById('divine_help').click();
}

function cpw_from_desktop_SetDesktopFunctionResult(txt)
{
	AfflatusCallbackFunction(txt);
}

CpwFormsAltarContent= null;
function cpw_from_desktop_GetDesktopFunctionAndParameters()
{
	return JSON.stringify(CpwFormsAltarContent);
}

function cpw_from_desktop_ViewActiveFormProfiledData()
{
	$('#form-editor').hide();
	ShowProfiled();
}

function ShowProfiled()
{
	var html_ForProfiledData = app.current_form_spec.BuildHtmlViewForProfiledData();
	$('#form-attrs').html(html_ForProfiledData);
	$('#form-attrs').show();
}

function cpw_from_desktop_StopViewActiveFormProfiledData()
{
	$('#form-attrs').hide();
	$('#form-editor').show();
}

function cpw_from_desktop_ActiveFormUseProfile(sprofile)
{
	var profile = JSON.parse(sprofile);
	app.current_form_spec.UseProfile(profile);
	ShowProfiled();
}

function cpw_from_desktop_EditProfile(sprofile)
{
    ProfileToEdit = JSON.parse(sprofile);
    alert(ProfileToEdit.Abonent.Name);
	$('div.start').hide();
	var form_spec_func = CpwProfileControllers.Full;
	var form_spec = form_spec_func(CpwFormsExtensionsProfileControllers);
	app.current_form_spec = form_spec;
	form_spec.SetFormContent(ProfileToEdit);
	form_spec.Edit('form-editor');
}

function cpw_from_desktop_GetEditedProfile()
{
	var form_spec = app.current_form_spec;
	var validation_text = form_spec.Validate();
	if (null != validation_text && '' != validation_text)
	{
		alert(validation_text);
		return null;
	}
	return JSON.stringify(form_spec.GetFormContent());
}

function cpw_from_desktop_StopActiveForm()
{
	var div_editor = $('#form-editor');
	div_editor.html('');
	div_editor.hide();
	$('div.start').show();
	delete app.current_form_spec;
	DivineHelp('OnCurrentFormStopped', null, null);
}