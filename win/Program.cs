﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace Forms
{
    static class Program
    {
        static ILog log = LogManager.GetLogger(typeof(Program));

        const string with_local_html_option_name = "--debug-local";
        const string with_profile_option_name = "--profile";

        [STAThread]
        static void Main(string[] args)
        {
            string ExeFileName = Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                ConfigureLog4net();
                log.InfoFormat("started {0}", ExeFileName);
                StartApplication(args);
            }
            finally
            {
                log.InfoFormat("exit {0}", ExeFileName);
            }
        }

        static void StartApplication(string[] args)
        {
            try
            {
                SetGlobalExceptionHandlers();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
#if DEBUG
                if (UseLocalHtml(args))
                    cpw.Forms.PathHelper.DebugLocalMode= true;
#endif
                Form frm= UseProfile(args)
                    ? (Form)cpw.Forms.ProfileBrowser.Instance
                    : (Form)new cpw.Forms.FormBrowser();
                Application.Run(frm);
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                MessageBox.Show(null, "Необработанная ошибка", ex.Message);
            }
        }

        static string FileNameToOpen(string[] args)
        {
            return args.FirstOrDefault(s => s != with_local_html_option_name && s != with_profile_option_name);
        }

        static bool UseLocalHtml(string[] args)
        {
            return args.Contains(with_local_html_option_name);
        }

        static bool UseProfile(string[] args)
        {
            return args.Contains(with_profile_option_name);
        }

        static void ConfigureLog4net()
        {
            log4net.Config.XmlConfigurator.Configure();
            Hierarchy repository = (Hierarchy)log4net.LogManager.GetRepository();
            if (repository.Root.Appenders.Count > 0)
            {
                FileAppender fileAppender = repository.Root.Appenders[0] as FileAppender;
                if (null != fileAppender)
                {
                    if (!Path.GetFullPath(fileAppender.File).StartsWith(Path.GetFullPath(cpw.Forms.PathHelper.AppDataPath)))
                    {
                        string logFileName = cpw.Forms.PathHelper.AppDataPath;
                        if (!Directory.Exists(logFileName))
                            Directory.CreateDirectory(logFileName);
                        logFileName = Path.Combine(logFileName, Path.GetFileName(fileAppender.File));
                        fileAppender.File = logFileName;
                        fileAppender.ActivateOptions();
                    }
                }
            }
        }

        static void SetGlobalExceptionHandlers()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(OnThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
        }

        static void OnThreadException(object sender, System.Threading.ThreadExceptionEventArgs t)
        {
            try
            {
                Exception e= t.Exception;
                ProcessException(e);
                Exception ie= e.InnerException as ArgumentException;
                if (null != ie && ie.Message.Contains("maxJsonLength"))
                {
                    MessageBox.Show(null, "Размер ваших данных превысил допустимый размер для данной программы!", e.Message);
                }
                else
                {
                    MessageBox.Show(null, "Необработанная ошибка в параллельных потоках управления", e.Message);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ProcessException(ex);
                    MessageBox.Show(null,"Фатальная ошибка",ex.Message);
                }
                finally
                {
                    Application.Exit();
                }
            }
        }

        static void OnUnhandledException(object sender, System.UnhandledExceptionEventArgs t)
        {
            try
            {
                Exception e= (Exception)t.ExceptionObject;
                ProcessException(e);
                MessageBox.Show(null,"Необработанная ошибка в основном потоке управления",e.Message);
            }
            catch (Exception ex)
            {
                try
                {
                    ProcessException(ex);
                    MessageBox.Show(null,"Фатальная ошибка",ex.Message);
                }
                finally
                {
                    Application.Exit();
                }
            }
        }

        static void ProcessException(Exception e)
        {
            log.Error(e.Message);
            log.Error(e.StackTrace);
            if (e.InnerException!=null)
            {
                log.Error(e.InnerException.Message);
                log.Error(e.InnerException.StackTrace);
            }
        }
    }
}
