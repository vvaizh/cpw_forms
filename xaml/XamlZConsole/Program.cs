﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Windows.Markup;

namespace XamlZConsole
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string cmd= args[0];
            switch (cmd)
            {
                case "open_xaml_xps": OpenXamlXps(args[1]); break;
                case "open_xaml_flow_xps": OpenXamlFlowXps(args[1], args[2]); break;
                case "fix_xaml": FixXaml(args[1]); break;
                case "fix_xaml_tpl": FixXamlTpl(args[1], args[2]); break;
                case "fix_xaml_open_xps": FixXamlOpenXps(args[1]); break;
                case "fix_xaml_tpl_open_xps": FixXamlTplOpenXps(args[1],args[2]); break;
                case "fix_xaml_flow_tpl_open_xps": FixXamlFlowTplOpenXps(args[1], args[2]); break;
                case "combine_xaml_open_xps": CombineXaml(args); break;
            }
        }

        static void CombineXaml(string[] args)
        {
            string path_to_xaml_tpl = args[1];
            string path_to_xps = Path.Combine(Path.GetDirectoryName(path_to_xaml_tpl), Path.GetFileNameWithoutExtension(path_to_xaml_tpl) + ".xps");
            string[] xaml_files = new string[args.Length - 3];
            Array.Copy(args, 3, xaml_files, 0, args.Length - 3);
            using (TextWriter writer = new StreamWriter(path_to_xaml_tpl, false, Encoding.UTF8))
            {
                ReadAndWritePagesContent(writer, args[2], xaml_files);
            }
        }

        static void ReadAndWritePagesContent(TextWriter writer, string xaml_file_first, string[] xaml_files)
        {
            using (TextReader reader = new StreamReader(xaml_file_first, Encoding.UTF8))
            {
                ReadAndWritePagesContent(writer, reader, xaml_files);
            }
        }

        static void ReadAndWritePagesContent(TextWriter writer, TextReader reader, string[] xaml_files)
        {
            for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
            {
                writer.WriteLine(line);
                if (line.Contains(MarkersXpsPageContent.page_content_marker_end))
                {
                    ReadAndWriteNextPagesContent(writer, xaml_files);
                }
            }
        }

        static void ReadAndWriteNextPagesContent(TextWriter writer, string[] xaml_files)
        {
            for (int i = 0; i < xaml_files.Length; i++)
            {
                using (TextReader reader = new StreamReader(xaml_files[i], Encoding.UTF8))
                {
                    ReadAndWritePageContent(writer, reader);
                }
            }
        }

        static void ReadAndWritePageContent(TextWriter writer, TextReader reader)
        {
            bool writePageContentToFile = false;
            for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
            {
                if (line.Contains(MarkersXpsPageContent.page_content_marker_start))
                {
                    writePageContentToFile = true;
                    writer.WriteLine(line);
                }
                else if (line.Contains(MarkersXpsPageContent.page_content_marker_end))
                {
                    writer.WriteLine(line);
                    writePageContentToFile = false;
                }
                else if (writePageContentToFile)
                {
                    writer.WriteLine(line);
                }
            }
        }

        static void FixXamlFlowTplOpenXps(string path_to_xaml, string path_to_xaml_tpl)
        {
            string path_to_xps = Path.Combine(Path.GetDirectoryName(path_to_xaml), Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xps");
            string path_to_fixed = Path.Combine(Path.GetDirectoryName(path_to_xaml), Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xaml");
            FixXaml(path_to_xaml, path_to_fixed, MarkersToOpenXps.Instance);
            FixXaml(path_to_fixed, path_to_xaml_tpl, MarkersToOpenXpsTpl.Instance);
            OpenXamlFlowXps(path_to_fixed, path_to_xps);
        }

        static void FixXamlTplOpenXps(string path_to_xaml, string path_to_xaml_tpl)
        {
            string path_to_xps= Path.Combine(Path.GetDirectoryName(path_to_xaml),Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xps");
            string path_to_fixed= Path.Combine(Path.GetDirectoryName(path_to_xaml),Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xaml");
            FixXaml(path_to_xaml,path_to_fixed, MarkersToOpenXps.Instance);
            FixXaml(path_to_fixed,path_to_xaml_tpl, MarkersToOpenXpsTpl.Instance);
            OpenXamlXps(path_to_fixed,path_to_xps);
        }

        static void FixXamlOpenXps(string path_to_xaml)
        {
            string path_to_fixed= Path.Combine(Path.GetDirectoryName(path_to_xaml),Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xaml");
            string path_to_xps= Path.Combine(Path.GetDirectoryName(path_to_xaml),Path.GetFileNameWithoutExtension(path_to_xaml) + ".fixed.xps");
            FixXaml(path_to_xaml,path_to_fixed, MarkersToOpenXps.Instance);
            OpenXamlXps(path_to_fixed,path_to_xps);
        }

        static void OpenXamlFlowXps(string path_to_xaml, string path_to_xps)
        {
            TestXamlFlowToXps(path_to_xaml, path_to_xps);

            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = path_to_xps;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
        }

        static void OpenXamlXps(string path_to_xaml, string path_to_xps)
        {
            TestXamlToXps(path_to_xaml,path_to_xps);

            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName= path_to_xps;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
        }

        static void OpenXamlXps(string path_to_xaml)
        {
            string path_to_xps= Path.Combine(Path.GetDirectoryName(path_to_xaml),Path.GetFileNameWithoutExtension(path_to_xaml) + ".xps");
            OpenXamlXps(path_to_xaml,path_to_xps);
        }

        static void XamlFlowStreamToXps(Stream srcXamlStream, string destXpsFile)
        {
            File.Delete(destXpsFile);
            using (XpsDocument document = new XpsDocument(destXpsFile, FileAccess.ReadWrite))
            {
                try
                {
                    using (XpsPackagingPolicy packagePolicy = new XpsPackagingPolicy(document))
                    using (XpsSerializationManager serializationMgr = new XpsSerializationManager(packagePolicy, false))
                    {
                        object parsedDocObject = XamlReader.Load(srcXamlStream);
                        System.Windows.Documents.DocumentPaginator paginator = ((System.Windows.Documents.IDocumentPaginatorSource)parsedDocObject).DocumentPaginator;
                        serializationMgr.SaveAsXaml(paginator);
                    }
                }
                finally
                {
                    document.Close();
                }
            }
        }

        static void XamlStreamToXps(Stream srcXamlStream, string destXpsFile)
        {
            File.Delete(destXpsFile);
            using (XpsDocument document = new XpsDocument(destXpsFile, FileAccess.ReadWrite))
            {
                try
                {
                    using (XpsPackagingPolicy packagePolicy = new XpsPackagingPolicy(document))
                    using (XpsSerializationManager serializationMgr = new XpsSerializationManager(packagePolicy, false))
                    {
                        object parsedDocObject = XamlReader.Load(srcXamlStream);
                        serializationMgr.SaveAsXaml(parsedDocObject);
                    }
                }
                finally
                {
                    document.Close();
                }
            }
        }

        static void TestXamlFlowToXps(string xaml_file_path, string xps_file_path)
        {
            using (FileStream stream = new FileStream(xaml_file_path, FileMode.Open))
            {
                XamlFlowStreamToXps(stream, xps_file_path);
            }
        }

        static void TestXamlToXps(string xaml_file_path, string xps_file_path)
        {
            using (FileStream stream = new FileStream(xaml_file_path, FileMode.Open))
            {
                XamlStreamToXps(stream,xps_file_path);
            }
        }

        static void FixXaml(string file_path_in)
        {
            string file_path_out= Path.Combine(Path.GetDirectoryName(file_path_in),Path.GetFileNameWithoutExtension(file_path_in) + ".fixed.xaml");
            FixXaml(file_path_in,file_path_out, MarkersToOpenXps.Instance);
        }

        static void FixXamlTpl(string file_path_in, string file_path_out)
        {
            FixXaml(file_path_in, file_path_out, MarkersToOpenXpsTpl.Instance);
        }

        internal interface IMarkers
        {
            string remove_marker_start { get; }
            string remove_marker_end { get; }

            string append_marker_start { get; }
            string append_marker_end { get; }

            string append_trace_marker_start { get; }
            string append_trace_marker_end { get; }
        }

        internal class MarkersToOpenXps : IMarkers
        {
            public string remove_marker_start { get { return "<!-- $$$$ remove {"; } }
            public string remove_marker_end { get { return "<!-- $$$$ remove }"; } }

            public string append_marker_start { get { return "<!-- $$$$ append {"; } }
            public string append_marker_end { get { return "$$$$ append } -->"; } }

            public string append_trace_marker_start { get { return "<!-- $$$$ append { -->"; } }
            public string append_trace_marker_end { get { return "<!-- $$$$ append } -->"; } }

            public static readonly IMarkers Instance= new MarkersToOpenXps();
        }

        internal class MarkersToOpenXpsTpl : IMarkers
        {
            public string remove_marker_start { get { return "<!-- $$$$ remove_tpl {"; } }
            public string remove_marker_end { get { return "<!-- $$$$ remove_tpl }"; } }

            public string append_marker_start { get { return "<!-- $$$$ append_tpl {"; } }
            public string append_marker_end { get { return "$$$$ append_tpl } -->"; } }

            public string append_trace_marker_start { get { return "<!-- $$$$ append_tpl { -->"; } }
            public string append_trace_marker_end { get { return "<!-- $$$$ append_tpl } -->"; } }

            public static readonly IMarkers Instance= new MarkersToOpenXpsTpl();
        }

        internal class MarkersXpsPageContent
        {
            public static string page_content_marker_start { get { return "<PageContent>"; } }
            public static string page_content_marker_end { get { return "</PageContent>"; } }
        }

        static void FixXaml(string file_path_in, string file_path_out, IMarkers markers)
        {
            using (TextReader reader = new StreamReader(file_path_in, Encoding.UTF8))
            using (TextWriter writer= new StreamWriter(file_path_out, false, Encoding.UTF8))
            {
                bool skip= false;
                for (string line = reader.ReadLine(); null != line; line= reader.ReadLine())
                {
                    if (line.StartsWith(markers.remove_marker_start))
                    {
                        skip= true;
                    }
                    else if (line.StartsWith(markers.remove_marker_end))
                    {
                        skip= false;
                    }
                    else if (line.StartsWith(markers.append_marker_start))
                    {
                        line= markers.append_trace_marker_start;
                    }
                    else if (line.EndsWith(markers.append_marker_end))
                    {
                        line= markers.append_trace_marker_end;
                    }
                    else if (!skip)
                        writer.WriteLine(line);
                }
            }
        }
    }
}
