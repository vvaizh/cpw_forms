function OnStartPage()
{
	if (confirm('Выйти из режима редактирования?')) {
		if (location.search)
		{
			window.location = 'index.html';
		}
		else
		{
			$('#cpw-form-editor').html('');
			$('#cpw-form-container').hide();
			$('body>div.body>div.container>div.load-form').hide();
			$('#cpw-start-page').show();
		}
	}
}

function GetProfile () {
	var cookie_name = 'cpw-forms-profile';
	var profile = $.cookie(cookie_name);
	profile = (!profile) ? {} : JSON.parse(profile);
	if (profile.parts && profile.parts.length > 0) {
		for (var i = 0; i < profile.parts.length; i++) {
			var part_profile = $.cookie(cookie_name + '_' + profile.parts[i]);
				part_profile = ('' == part_profile) ? {} : JSON.parse(part_profile);
			profile[profile.parts[i]] = part_profile;
		};
	}
	return profile;
}

function StartUsingForm(id_form)
{
	var form_spec_func = CpwFormsExtensionsConstructors[id_form];
	var form_spec = form_spec_func();
	window.current_form_spec = form_spec;
	for (var ie = 0; ie < CpwFormsExtensionsInfo.length; ie++)
	{
		var extension = CpwFormsExtensionsInfo[ie];
		$('#cpw-form-container div.ExtensionTitle').text(extension.Title);
	}
	$('#cpw-start-page').hide();
	var profile = GetProfile();
	form_spec.UseProfile(profile);
	$('#cpw-form-container').show();
	return form_spec;
}

function OnExtensionFormClick()
{
	var keyForm = $(this).parents('div.Form').children('div.Key').text();
	var keyExtension = $(this).parents('div.extension').children('div.Header').children('div.Key').text();
	var id_form = keyExtension + '.' + keyForm;
	var form_spec= StartUsingForm(id_form);
	form_spec.CreateNew('cpw-form-editor');
	$('#cpw-form-attrs').html(form_spec.BuildHtmlViewForProfiledData());
}

function UpdateFormByProfile()
{
	var profile = GetProfile();
	var form_spec = window.current_form_spec;
	form_spec.UseProfile(profile);
	$('#cpw-form-attrs').html(form_spec.BuildHtmlViewForProfiledData());
}

function OpenXamlView()
{
	var form_spec = window.current_form_spec;
	form_spec.GetDraftFormContent();
	var xaml_content = form_spec.BuildXamlView();

	$('#cpw-form-open-xaml textarea').text(xaml_content);
	$('#cpw-form-open-xaml').submit();
}

function cpw_forms_web_Save()
{
	var form_spec = window.current_form_spec;
	var validation_text = form_spec.Validate();
	if (null != validation_text && '' != validation_text)
	{
		alert(validation_text);
	} 
	else 
	{
		var content= '';
		try
		{
			content= form_spec.GetFormContent();
		}
		catch (ex)
		{
			if (!ex.parseException)
			{
				throw ex;
			}
			else
			{
				alert("Ошибка чтения документа (неверный формат):\r\n" + ex.readablemsg);
				return;
			}
		}

		$('#cpw-form-save textarea').text(content);
		$('#cpw-form-save').submit();
	}
}

function cpw_forms_web_SaveDraft()
{
	var form_spec = window.current_form_spec;

	var content = form_spec.GetDraftFormContent();
	$('#cpw-form-save-draft textarea').text(JSON.stringify(content));
	$('#cpw-form-save-draft').submit();
}

function OnLoadForm()
{
	$('body>div.body>div.container>div.load-form').show();
	$('#cpw-start-page').hide();
	var type_select = $("body>div.body>div.container>div.load-form select");
	type_select.empty();
	type_select.append('<option></option>');
	for (var iextension = 0; iextension < CpwFormsExtensionsInfo.length; iextension++)
	{
		var extension = CpwFormsExtensionsInfo[iextension];
		type_select.append('<option disabled="disabled">' + extension.Title + '</option>');
		for (var iform = 0; iform < extension.forms.length; iform++)
		{
			var form = extension.forms[iform];
			var id_form = extension.key + '.' + form.key;
			var txtFileFormat = '*.' + form.FileFormat.FileExtension + '\t– ' + form.FileFormat.Description;
			type_select.append('<option value="' + id_form + '">' + txtFileFormat + '</option>');
		}
	}
	type_select.select2({placeholder: "Выберите тип формы для выбранного файла",allowclear:true});
}

cpw_forms_page_params = {};

function cpw_forms_OnContentReceived(data, textStatus, jqXHR)
{
	var id_form = cpw_forms_page_params.id_form;
	var form_spec = StartUsingForm(id_form);
	try
	{
		form_spec.SetFormContent(data);
	}
	catch (ex)
	{
		if (!ex.parseException)
		{
			throw ex;
		}
		else
		{
			alert("Ошибка чтения документа (неверный формат):\r\n" + ex.readablemsg);
			OnStartPage();
			return;
		}
	}
	form_spec.Edit('cpw-form-editor');
	$('#cpw-form-attrs').html(form_spec.BuildHtmlViewForProfiledData());
}

function cpw_forms_OnContentError(jqXHR, textStatus, errorThrown)
{
	alert('can not get content..');
}

function cpw_forms_OnAllExtensionsLoaded()
{
	if (location.search)
	{
		var parts = location.search.substring(1).split('&');

		for (var i = 0; i < parts.length; i++)
		{
			var nv = parts[i].split('=');
			if (!nv[0]) continue;
			cpw_forms_page_params[nv[0]] = nv[1] || true;
		}
	}

	if (cpw_forms_page_params.action && 'open' == cpw_forms_page_params.action)
	{
		$.ajax({
			url: 'index.php?action=content',
			dataType: 'text',
			success: function (data, textStatus, jqXHR) { cpw_forms_OnContentReceived(data, textStatus, jqXHR); },
			error: function (jqXHR, textStatus, errorThrown) { cpw_forms_OnContentError(jqXHR, textStatus, errorThrown); }
		});
	}
}