<?
mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

function BaseUrl()
{
	$result = '';
	$default_port = 80;

	if (!isset($_SERVER['HTTPS']) || !($_SERVER['HTTPS']=='on'))
	{
		$result .= 'http://';
	}
	else
	{
		$result .= 'https://';
		$default_port = 443;
	}
	$result .= $_SERVER['SERVER_NAME'];

	if ($_SERVER['SERVER_PORT'] != $default_port)
		$result .= ':'.$_SERVER['SERVER_PORT'];

	$uri_parts= split('\\?', $_SERVER['REQUEST_URI']);
	$uri_without_params= $uri_parts[0];
	$fname= 'index.php';
	$uri_without_fname= $uri_parts[0];
	$result .= substr($uri_without_params,0,strlen($uri_without_params)-strlen($fname));
	return $result;
}

if (!isset($_GET['action']))
{
	echo 'параметр действия неопределён<br/>';
	exit;
}
else if ('xaml'==$_GET['action'])
{
	if (isset($_POST['xamlcontent']))
	{
		session_start();
		$_SESSION['xamlcontent']= stripslashes($_POST['xamlcontent']);
		header('Location: '.BaseUrl().'index.php?action=xaml&id_session=' . session_id());
	}
	else if (isset($_GET['id_session']))
	{
		session_id($_GET['id_session']);
		session_start();
		header("Content-Type: application/xaml+xml");
		print $_SESSION['xamlcontent'];
	}
	exit;
}
else if ('save'==$_GET['action'])
{
	header("Content-Type: application/xml");
	header('Content-Disposition: attachment; filename="form.xml"');
	print stripslashes($_POST['content']);
	exit;
}
else if ('save_draft'==$_GET['action'])
{
	header("Content-Type: text/plain");
	header('Content-Disposition: attachment; filename="form_draft.txt"');
	print stripslashes($_POST['content']);
	exit;
}
else if('open'==$_GET['action'])
{
	try
	{
		if (!isset($_FILES['content']))
			throw new RuntimeException('Skipped content.');

		$file_param= $_FILES['content'];
		if (!isset($file_param['error']) || is_array($file_param['error']))
			throw new RuntimeException('Invalid parameters.');

		switch ($file_param['error'])
		{
			case UPLOAD_ERR_OK:        break;
			case UPLOAD_ERR_NO_FILE:   throw new RuntimeException('No file sent.');
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE: throw new RuntimeException('Exceeded filesize limit.');
			default:                   throw new RuntimeException('Unknown errors.');
		}

		if ($file_param['size'] > 1000000)
			throw new RuntimeException('Exceeded filesize limit.');

		session_start();
		$_SESSION['content']= file_get_contents($file_param['tmp_name']);
		header('Location: '.BaseUrl().'index.html?action=open&id_form='.$_POST['id_form']);
		exit;
	}
	catch (RuntimeException $ex)
	{
		echo $ex->getMessage();
	}
}
else if('content'==$_GET['action'])
{
	session_start();
	echo $_SESSION['content'];
	exit;
}
else
{
	echo 'какой то неизвестный параметр';
	echo BaseUrl();
}
?>
